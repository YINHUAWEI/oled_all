#if 0
#ifndef _HIREG_H_
#define _HIREG_H_

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */



void *memmap_reg(unsigned int phy_addr, unsigned int size);

unsigned int set_reg(const unsigned int addr, const unsigned int value);
unsigned int get_reg(const unsigned int addr);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif //_HIREG_H_
#endif
