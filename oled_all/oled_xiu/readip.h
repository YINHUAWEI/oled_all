#ifndef _READIP_H_
#define _READIP_H_

#include <sys/socket.h>  
#include <netinet/in.h>  
#include <arpa/inet.h>  
#include <sys/ioctl.h>  
#include <net/if.h>  
#include <stdio.h>  
#include <string>  
#include <iostream>  
#include <cstring>

#include "oled_type.h"

using namespace std;

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern ifreq ifs[16];

extern VOID     print_hw_addr(int fd, const char* if_name, char* macAddr);
extern INT32    peek_interfaces(int fd);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif
