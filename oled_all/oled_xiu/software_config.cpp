#include "software_config.h"
char netfilepath_[36];
char filepath_[36];

SoftwareConfig::SoftwareConfig()
{
    if( !MakeDir(DATA_FILE) ){
        sprintf(netfilepath_, "./%s", NET_CONFIG_FILE);
        sprintf(filepath_, "./%s", OLED_CONFIG_FILE);
        }else{
        sprintf(netfilepath_, "%s/%s", DATA_FILE, NET_CONFIG_FILE);
        sprintf(filepath_, "%s/%s", DATA_FILE, OLED_CONFIG_FILE);
        }

    // 初始化 向量类型softwareConfig,定义其长度为kSoftWareConfigIDMax，内容都先置为空
    configvalue_.assign(kSoftWareConfigIDMax, "");
    //network






//    if(1)
//    {}
//    configvalue_[kEth0Ip]                   = "0.0.0.0";
//    configvalue_[kEth0Mask]                 = "0.0.0.0";
//    configvalue_[kEth0Gateway]              = "0.0.0.0";
//    configvalue_[kEth0Dns]                  = "0.0.0.0";
//    configvalue_[kEth0Mac]                  = "00:00:00:00:00:00";
    //




    configvalue_[Oled_Port]                  = "16124";
    configvalue_[Oled_Port_udp]               = "16124";
    //色彩增强
    configvalue_[Oled_a_kind_start]                   = "1";
  //  configvalue_[Oled_a_big_start]                     = "0";
    configvalue_[Oled_a_small_i_start]                 = "0";
    configvalue_[Oled_a_small_n_start]                 = "0";
    configvalue_[Oled_a_small_i_buf]                     = "0.0.0.0";
    configvalue_[Oled_a_small_n_buf]                     = "WELCOME";
  // configvalue_ [Oled_a_small_china_start]              ="0";
    configvalue_[Oled_a_cpu]                         = "1";
    configvalue_[Oled_a_vi]                          = "1";
    configvalue_[Oled_a_ai]                          = "1";
    configvalue_[Oled_a_net]                         = "1";
    configvalue_[Oled_a_venc]                        = "1";
    configvalue_[Oled_a_start]                       = "1";
    configvalue_[Oled_a_sleep_tim]                   = "3";
    configvalue_[Oled_a_all_ip]                      = "1";
    configvalue_[OLed_a_gpio_j]                        ="0";
    configvalue_[Oled_model]                         = "0";
    configvalue_[Oledsml_SDA_DIR]                    = "0";
    configvalue_[OLed_sml_SCL_DIR]                   = "0";
    configvalue_[OLed_sml_SCLMUX]                    = "0";
    configvalue_[OLed_sml_SCLMUX_G]                  = "0";
    configvalue_[OLed_sml_SDAMUX]                    = "0x0";
    configvalue_[OLed_sml_SDAMUX_G]                  = "0x0";
    configvalue_[OLed_sml_SDA]                       = "0x0";
    configvalue_[OLed_sml_SCL]                       = "0x0";
    configvalue_[OLed_sml_SDA_H]                     = "0x0";
    configvalue_[OLed_sml_SDA_L]                     = "0x0";
    configvalue_[OLed_sml_SCL_H]                     = "0x0";
    configvalue_[OLed_sml_SCL_L]                     = "0x0";
    configvalue_[OLed_sml_SCL_DIR_W]                 = "0x0";
    configvalue_[OLed_sml_SDA_DIR_W]                 = "0x0";
    configvalue_[OLed_sml_VCCMUX]                    = "0x0";
    configvalue_[OLed_sml_GNDMUX]                    = "0x0";
    configvalue_[OLed_sml_VCC]                       = "0x0";
    configvalue_[OLed_sml_GND]                       = "0x0";
    configvalue_[OLed_sml_VCC_H]                     = "0x0";
    configvalue_[OLed_sml_VCC_L]                     = "0x0";
    configvalue_[OLed_sml_GND_H]                     = "0x0";
    configvalue_[OLed_sml_GND_L]                     = "0x0";
    configvalue_[OLed_ba_CS]                         = "0x0";
    configvalue_[OLed_ba_DC]                         = "0x0";
    configvalue_[OLed_ba_RES]                        = "0x0";
    configvalue_[OLed_ba_CS_mux_W]                        = "0x0";
    configvalue_[OLed_ba_DC_mux_W]                        = "0x0";
    configvalue_[OLed_ba_RES_mux_W]                        = "0x0";
    configvalue_[OLed_ba_CS_MUX]                     = "0x0";
    configvalue_[OLed_ba_DC_MUX]                     = "0x0";
    configvalue_[OLed_ba_RES_MUX]                    = "0x0";
    configvalue_[ OLed_ba_CS_H]                       = "0x0";
    configvalue_[ OLed_ba_DC_H]                        = "0x0";
    configvalue_[ OLed_ba_RES_H]                       = "0x0";
    configvalue_[ OLed_ba_CS_L]                        = "0x0";
    configvalue_[ OLed_ba_DC_L]                        = "0x0";
    configvalue_[ OLed_ba_RES_L]                       = "0x0";
    configvalue_[ OLed_CS_DIR]                        = "0x0";
    configvalue_[ OLed_DC_DIR]                        = "0x0";
    configvalue_[ OLed_CS_DIR_W]                        = "0x0";
    configvalue_[ OLed_DC_DIR_W]                        = "0x0";
#if 1


#endif



    snprintf(paramsnamelist[Oled_Port],                  PARA_NAME_LEN, "%s=", EnumtoStr(Oled_Port));
    snprintf(paramsnamelist[Oled_Port_udp],                   PARA_NAME_LEN, "%s=", EnumtoStr(Oled_Port_udp));
    snprintf(paramsnamelist[Oled_a_kind_start],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_kind_start));
  //  snprintf(paramsnamelist[Oled_a_big_start],                PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_big_start));
    snprintf(paramsnamelist[Oled_a_small_i_start],            PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_small_i_start));
    snprintf(paramsnamelist[Oled_a_small_n_start],            PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_small_n_start));
    snprintf(paramsnamelist[Oled_a_small_i_buf],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_small_i_buf));
    snprintf(paramsnamelist[Oled_a_small_n_buf],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_small_n_buf));
  // snprintf(paramsnamelist[Oled_a_small_china_start],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_small_china_start));
    snprintf(paramsnamelist[Oled_a_cpu],            PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_cpu));
    snprintf(paramsnamelist[Oled_a_vi],            PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_vi));
    snprintf(paramsnamelist[Oled_a_ai],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_ai));
    snprintf(paramsnamelist[Oled_a_net],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_net));
    snprintf(paramsnamelist[Oled_a_venc],            PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_venc));
    snprintf(paramsnamelist[Oled_a_start],            PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_start));
    snprintf(paramsnamelist[Oled_a_sleep_tim],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_sleep_tim));
    snprintf(paramsnamelist[Oled_a_all_ip],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_all_ip));
    snprintf(paramsnamelist[OLed_a_gpio_j],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_gpio_j));
    snprintf(paramsnamelist[Oled_model],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_model));
  //gpio
    snprintf(paramsnamelist[Oledsml_SDA_DIR],              PARA_NAME_LEN, "%s=", EnumtoStr(Oledsml_SDA_DIR));
    snprintf(paramsnamelist[OLed_sml_SCL_DIR],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_DIR));
    snprintf(paramsnamelist[OLed_sml_SCLMUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCLMUX));
    snprintf(paramsnamelist[OLed_sml_SCLMUX_G],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCLMUX_G));
    snprintf(paramsnamelist[OLed_sml_SDAMUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDAMUX));
    snprintf(paramsnamelist[OLed_sml_SDAMUX_G],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDAMUX_G));
    snprintf(paramsnamelist[OLed_sml_SDA],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA));
    snprintf(paramsnamelist[OLed_sml_SCL],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL));
    snprintf(paramsnamelist[OLed_sml_SDA_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_H));
    snprintf(paramsnamelist[OLed_sml_SDA_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_L));
    snprintf(paramsnamelist[OLed_sml_SCL_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_H));
    snprintf(paramsnamelist[OLed_sml_SCL_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_L));
    snprintf(paramsnamelist[OLed_sml_SCL_DIR_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_DIR_W));
    snprintf(paramsnamelist[OLed_sml_SDA_DIR_W],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_DIR_W));
    snprintf(paramsnamelist[OLed_sml_VCCMUX],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCCMUX));
    snprintf(paramsnamelist[OLed_sml_GNDMUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GNDMUX));
    snprintf(paramsnamelist[OLed_sml_VCC],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC));
    snprintf(paramsnamelist[OLed_sml_GND],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GND));
    snprintf(paramsnamelist[OLed_sml_VCC_H],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC_H));
     snprintf(paramsnamelist[OLed_sml_VCC_L],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC_L));
     snprintf(paramsnamelist[OLed_sml_VCC_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC_L));
     snprintf(paramsnamelist[OLed_sml_GND_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GND_H));
     snprintf(paramsnamelist[OLed_sml_GND_L],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GND_L));


     //big

     snprintf(paramsnamelist[OLed_ba_CS],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS));
     snprintf(paramsnamelist[OLed_ba_DC],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC));
     snprintf(paramsnamelist[OLed_ba_RES],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES));
     snprintf(paramsnamelist[OLed_ba_CS_mux_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_mux_W));
     snprintf(paramsnamelist[OLed_ba_DC_mux_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_mux_W));
     snprintf(paramsnamelist[OLed_ba_RES_mux_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_mux_W));
     snprintf(paramsnamelist[OLed_ba_CS_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_MUX));
     snprintf(paramsnamelist[OLed_ba_DC_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_MUX));
     snprintf(paramsnamelist[OLed_ba_RES_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_MUX));
      snprintf(paramsnamelist[OLed_ba_CS_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_H));
      snprintf(paramsnamelist[OLed_ba_DC_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_H));
      snprintf(paramsnamelist[OLed_ba_RES_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_H));
      snprintf(paramsnamelist[OLed_ba_CS_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_L));
      snprintf(paramsnamelist[OLed_ba_DC_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_L));
      snprintf(paramsnamelist[OLed_ba_RES_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_L));
      snprintf(paramsnamelist[OLed_CS_DIR],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_CS_DIR));
      snprintf(paramsnamelist[OLed_DC_DIR],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_DC_DIR));
      snprintf(paramsnamelist[OLed_CS_DIR_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_CS_DIR_W));
      snprintf(paramsnamelist[OLed_DC_DIR_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_DC_DIR_W));


}

SoftwareConfig::~SoftwareConfig()
{
    COMMON_PRT("SoftwareConfig Normal Exit!\n");
}
  int kEth0Mac = 0;
bool SoftwareConfig::ReadConfig()
{
    COMMON_PRT("read_config:%s and %s\n", netfilepath_, filepath_);

//    read_ini rnet( netfilepath_ );
//    for(uint i = 0; i <= kEth0Mac; i++){
//        rnet.find_value( paramsnamelist[i],   configvalue_[i] );
//    }

    read_ini ri( filepath_ );
    for(uint i = 1; i < kSoftWareConfigIDMax; i++){
        ri.find_value( paramsnamelist[i],   configvalue_[i] );
    }

    return true;
}
#if 1
 #endif
bool SoftwareConfig::SaveConfig()
{
    FILE *f = NULL;
    f = fopen( filepath_, "wb" );
    if(f)
    {
        for(uint i = kEth0Mac+1; i < kSoftWareConfigIDMax; i++){
            fprintf(f, "%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
        }
        fflush(f);
        fclose(f);
        f = NULL;
        COMMON_PRT("save config succeed");
        return true;

    }

    COMMON_PRT("save config failed");
    return false;
}

void SoftwareConfig::PrintConfig()
{
    for(uint i = 0; i < kSoftWareConfigIDMax; i++){
        printf("%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
    }
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, string value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = value;
    else
        return false;

    return true;
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, int value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = int2str(value);
    else
        return false;

    return true;
}

string SoftwareConfig::GetConfig(const SoftWareConfigID kId)
{
    if(kId < kSoftWareConfigIDMax)
        return configvalue_[kId];
    else
        return "error";
}

bool SoftwareConfig::LoadConfigToGlobal(bool bPrintThem)
{
    if(bPrintThem){

    }

    return true;
}


