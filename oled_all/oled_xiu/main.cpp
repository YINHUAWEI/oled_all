

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>
#include <deque>
#include <signal.h>
#include <sys/epoll.h>
#include "./udpsocket/udpsocket.h"
#include "software_config.h"
//#include "himpp_master.h"
//#include "http_handler.h"
//#include "software_config.h"
//#include "commandmap.h"
#include "singleton.h"
//#include "common.h"
#include "readip.h"
#include "oled.h"
#include "readsys.h"

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus    */

#define MAXDATA   1024
#define DEMAXDATALAY_TIME      10
#define OLED_CHAR_NUM   22
#define BIG_IP          1
#define HDPATH      "/version/hardware_info"

#define MAXEPOLLSIZE    (128)
#define MAXDATA         1024
#define BUFF_SIZE (1024*1024)
#define MESSAGE_BUF_MAX (5*1024*1024)
#define CMD_KILL  "[KILL]"
#define CMD_RESET "[RESET]"
#define CMD_HELP  "[HELP]"
#define CMD_REMSGQUEUE  "[REMSGQUEUE]"



using namespace std;

int oled_ump_i = 0;

bool oled_ump_n  = false;
bool oled_ump_n_data= false;
char oled_ump_i_data[56];
bool oled_big_start = false;
bool oled_small_start = false;
bool init_sys_big = true;
bool init_sys_small = true;
bool  thread_big_a_loop = true;
bool thread_sml_a_loop = true;
char  * oled_n_start; // malloc(sizeof(int ) * 6);
char *  oled_i_start = NULL;
char oled_data[512]  ;
int oled_sleep_time = 3;
char oled_data_ump[1024];
bool  send_ump_start = false;
bool big_a_exit = true;
bool small_a_exit = true;
char ump_ip[20] = {0};
bool  thread_oled_ump = true;
bool ump_send_me = false;
bool oled_smala_china = false;

bool oled_a_cup = true;
bool oled_a_ai = true;
bool oled_a_venc = true ;
bool oled_a_ethspeed = true;
bool oled_a_time = true;
bool oled_a_vi = true;


bool oled_all_ip = true;
int oled_jpio_j = 0;
uint ump_port = 0;
  UDPSocket *udpsocket_ump = NULL;
U32   g_hexip[3];
U32   g_mac[3][20];
U8    g_ip[3][20];
//int g_sock =0;
INT32 g_ethnum = 0;
extern U8 chinese_num[14][16];
extern int display_length;
char loip[]="127.0.0.1";
bool main_exit_while = true;
bool readHdVersion(char *hd_version);

inline  VOID showIp( U8 offset, U16 i, U8 flag = '0' );
inline  VOID showMac( U8 offset, U16 i );
BOOL    getIp();
VOID    initSys();
VOID   initSys_big();

void init_argumnt(int oled_moudle);
VOID     print_hw_addr(int fd, const char* if_name, char* macAddr);
INT32    peek_interfaces(int fd);
void oledsml_set_gpio(int oled_who);

bool readHdVersion(char *hd_version)
{
    FILE *hdFp = NULL;
    int rdLen = 0;
    char buff[256] = {0};

    char *st = NULL, *end = NULL;

    if(NULL == (hdFp = fopen(HDPATH,"r"))) {
        printf("open   hardware_info error\n");
        return false;
    }
    memset(buff,0,256);
    memset(hd_version,0,32);

    rdLen = fread(buff,1,256,hdFp);
    if(rdLen > 0) {
        printf("read hardware_info:%s\n",buff);
        st = strstr(buff,"\"board\": \"");
        st += strlen("\"board\": \"");
        end = strchr(st,'\"');
        strncpy(hd_version,st,end-st);
        printf("get hdVersion:%s\n",hd_version);
    }
    else{
        fclose(hdFp);
        return false;
    }
    fclose(hdFp);
    return true;
}

#define NODE_MODEL  "input_4hdmi"
#if 0


#endif





bool l_recvrunning = true;


ifreq ifs[16] = {0};

VOID print_hw_addr(INT32 fd, const char* if_name, char* macAddr) {
    ifreq req = {0};
    strcpy(req.ifr_name, if_name);
    if(-1 == ioctl(fd, SIOCGIFFLAGS, &req)) {
        perror("Failed   IOCTL SIOCGIFFLAGS.");
        return;
    }
    if(req.ifr_flags & IFF_LOOPBACK) {
        //cout << "Is LOOPBACK." << endl;
        return;
    }

    if(-1 == ioctl(fd, SIOCGIFHWADDR, &req)) {
        perror("Failed IOCTL SIOCGIFHWADDR.");
        return;
    }

    unsigned char* puc = (unsigned char*)req.ifr_hwaddr.sa_data;
    sprintf(macAddr, "%02x%02x%02x%02x%02x%02x", puc[0], puc[1], puc[2], puc[3], puc[4], puc[5]);
    //sprintf(macAddr,   "%02x:%02x:%02x:%02x:%02x:%02x", puc[0], puc[1], puc[2], puc[3], puc[4], puc[5]);
}

INT32 peek_interfaces(INT32 fd) {
    memset(ifs, 0, sizeof(ifs));
    ifconf conf = {sizeof(ifs)};
    conf.ifc_req = ifs;
    if(-1 == ioctl(fd, SIOCGIFCONF, &conf)) {
        perror("Failed IOCTL SIOCGIFCONF.");
        return -1;
    }
    if(conf.ifc_len >= sizeof(ifs)) {
        perror("Buffer too small for IOCTL SIOCGIFCONF.");
        return -1;
    }

    return (conf.ifc_len / sizeof(ifreq));
}







#if 1


void * send_ump_Thread()
{
    int send_ump_len;

//         if(ump_send_me == false)
//          { break;
//          }
     if(send_ump_start == true)
     {
        // send_ump_len = udpsocket->SendTo(content[0].c_str(),(uint)str2int(content[1]), oled_data_ump, strlen(oled_data_ump));

        // printf("umpstart-------------\n");
        // init_argumnt();
 #if 0
         if (1){
          char oled_File_buf[1024] = {0};
          int  oled_File = open("/data/oled.ini",O_RDONLY);
          if(oled_File == NULL)
          {
              printf("error  open have`nt /data/oled.ini  \n");

          }
          else{

              bzero(oled_File_buf,sizeof(oled_File_buf));
              //read
              int ret_read = read(oled_File,oled_File_buf,sizeof(oled_File_buf));
              if(ret_read < 0)
              {
                  perror("fail to read");
                  //return -1;
              }
              else if(ret_read == 0) //end_of_file
              {
                  printf("end\n");
                 // break;
              }

              printf("read_buf=%s\n",oled_File_buf);


          }

#endif

          FILE *fp1 = NULL;

          char oled_File_buf[1024];
          char oled_File_buf_cpy[1024];
          int ret;
          fp1 = fopen("/data/oled.ini","r");
          if(fp1 == NULL)
          {
              perror("fail to fopen1");

          }



          //do something

              //Çå¿ÕÊý×é
          //	bzero(buf,sizeof(buf));
             int file_buf_len = 0;
            int cpy_buf = 0;
           int i = 0;
            for( i = 0; i < 15; i++)
            {
            memset(oled_File_buf,0,sizeof(oled_File_buf));
              if(NULL == fgets(oled_File_buf,sizeof(oled_File_buf),fp1))
              {
                  if(feof(fp1))
                  {
                      printf("open feof\n");
                  }
                  else
                  {
                      fprintf(stderr,"fail to fgets!\n");

                  }
              }

                       file_buf_len = strlen(oled_File_buf);
                     // sprintf(&oled_File_buf_cpy[file_buf_len],"%s" ,oled_File_buf);

                     strcpy(&oled_File_buf_cpy[cpy_buf],oled_File_buf);
                       //oled_File_buf_cpy  = oled_File_buf ;
                cpy_buf = cpy_buf + file_buf_len;
                   // oled_File_buf_cpy + file_buf_len = oled_File_buf;

              }
        printf("file_buf_len =%d open buf =%s  oled_fbu_vpy=%s\n",file_buf_len,oled_File_buf,oled_File_buf_cpy);








          sprintf(oled_data_ump,"[loed]\n");
          //strcpy(&oled_data_ump[7],oled_File_buf);
           strcpy(&oled_data_ump[7],oled_File_buf_cpy);
  }



         printf("send_mup-start\n");
         send_ump_len =  udpsocket_ump->SendTo(ump_ip,ump_port,oled_data_ump,strlen(oled_data_ump));
         send_ump_start = false;











}



void *  oled_big_a_loop(void * arg)
{

      printf(" ----oled_big_loop- \n");
        int star_flg = 1;
         int sync_count = 0;
         int str_len = 0;
          FILE * fTickFile = NULL;
    if(1){
          string timeres,airate,resolution,vencres,ethspeed,cpures;
 auto oledmaster = Singleton<OLEDMaster>::getInstance();
   auto oledmaster_ba = Singleton<OLEDBaMaster>::getInstance();

   // fclose(fTickFile);
while(thread_big_a_loop){
    if((who_led == 1) ||(who_led == 3) )
    {
        if (( oled_big_start == true)){//&& ( small_a_exit == true)){

            big_a_exit = false;
            if( init_sys_big == true)
            {


                // oled_buf_init(who_led);
                printf("**************tucjk=%d\n",fTickFile);
               oledmaster_ba-> initSys_big();
               oledmaster_ba->oled_open_all(fTickFile);
                init_sys_big = false;
                getIp();
                if(oled_all_ip == true ){
                    //ShowOLEDFill( 0, 0, 256, 39, 0x00 );
    #if BIG_IP
                  //  printf("+++++++++++if++ oled ip\n");
                   oledmaster_ba-> ShowOLEDString_big( 1, 0, g_ip[1], 15 );//第一个参数控制左右移，第二个参数控制上下
    #else
                    oledmaster-> ShowOLEDSmallString( 1, 0, g_ip[0], strlen((S8*)g_ip[0]) );
    #endif
               // sleep(oled_sleep_time);

                }



            }



#if 1
            if(fTickFile != NULL){
                //是否锁定，每秒钟读取一下Tick的文件
                int sync = 0;
                char f_data[100];
                memset(f_data, 0, sizeof(f_data));
                fseek(fTickFile,0,SEEK_SET);
                if(fgets(f_data,100,fTickFile)){
                    sync = 0;
                    if(f_data[0] == '-'){
                        for(int i = 1; i < min(100,(int)strlen(f_data)); i ++){
                            if(f_data[i] == 0)
                                break;
                            sync *= 10;
                            sync -= f_data[i] - '0';
                        }
                    }
                    else{
                        for(int i = 0; i < strlen(f_data); i ++){
                            if(f_data[i] == 0)
                                break;
                            sync *= 10;
                            sync += f_data[i] - '0';
                        }
                    }
                    if(sync_count == 11)
                      oledmaster->  ShowOLEDSmallString( 240, 0, (U8*)"Lk", 2 );
                    else
                      oledmaster->  ShowOLEDSmallString( 240, 0, (U8*)"Un", 2 );

                    if(sync >= -5000 && sync <= 5000){
                        //锁定了
                        printf("Time Tick Locked.\n");
                        if(sync_count < 10)
                            sync_count ++;
                        else if(sync_count == 10){
                            //记录本地Tick和全局Tick的差值
                            //gbLock = true;
                            //glLocalTickDelta = (int)time_tick.ko_date.frame_num - (int)kodata.frame_num;
                            //printf("Local Time Tick Offset: %d\n", glLocalTickDelta);
                            //printf("lock\n");
                            //ShowOLEDSmallString( 240, 0, (U8*)"Lk", 2 );
                            sync_count = 11;
                        }
                    }
                    else if(sync <= -50000 || sync >= 50000){
                        //失锁
                        printf("Time Tick Unlocked! Offset: %d\n", sync);
                        if(sync_count < 11 || sync <= -100000 || sync >= 100000){
                            //gbLock = false;
                            //  printf("unlock\n");
                            //ShowOLEDSmallString( 240, 0, (U8*)"Un", 2 );
                            sync_count = 0;
                        }
                    }
                    else{
                        printf("Time Tick Keep. Offset: %d\n", sync);
                    }
                }
            }


#endif

            if(oled_ump_i  == 0 ){
        printf("oled_ump_i-----------------------oled_ump_i-\n");

               if(strcmp(((S8*)g_ip[0]),loip))
               {

                   printf("have nt \n ");


               }
              else{
                   printf("ip_-------------i\n");
                oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );

              oledmaster_ba-> ShowOLEDBigString( 2, 45,  g_ip[0],  strlen((S8*)g_ip[0]) );

              }



            }
             else  if(oled_ump_i == 2)
               {
        //         sleep(oled_sleep_time);
                   sprintf( ((S8*)g_ip[0]),"%s",oled_ump_i_data);
                   oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
                    printf("oled_ump_i_data=%s  u_ip=%s\n",oled_ump_i_data,((S8*)g_ip[0]));
                 oledmaster_ba-> ShowOLEDBigString( 2, 45,  g_ip[0],  strlen((S8*)g_ip[0]) );
                 sleep(oled_sleep_time);

               }



            if(oled_ump_n == true)
            {

        //            oled_n_start = oled_data;
        //            devname = oled_n_start + 9 ;
        string devname;
                    if(oled_ump_n_data == true)
                    {
                        oled_n_start = oled_data;
                        devname = oled_n_start + 9 ;
                    }
                    else
                    {
                        oled_n_start = oled_data;
                        devname = oled_n_start;
                    }
                    //   oled_n_start +
                oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
                //devname = oled_data ;

                printf("--- -daa=%s  -=%s\n", oled_data, oled_n_start + 9);
              //  printf("**************************%s   ---%d\n",(U8*)devname.c_str(), devname.length());
               oledmaster_ba-> ShowOLEDBigString( 2, 45, (U8*)devname.c_str(), devname.length() );
                sleep(oled_sleep_time);

            }


            if(oled_a_cup == true)
            {
                string data_cpu ,data_jiao;
              data_cpu = CheckCpuFile_big();
             int len = data_cpu.length();
                data_cpu[len -1]   = ' ';
              //   sprintf(data_cpu,"CPU:34.13");

             cpures =  data_cpu;
            if(cpures.length() > 0){
                printf("CheckCpuFile_big+++++++++++++++++cpu=%s-%d\n", (U8*)cpures.c_str(), cpures.length());
           oledmaster_ba->ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
             oledmaster_ba-> ShowOLEDBigString( 2, 45, (U8*)cpures.c_str(), cpures.length() );
                sleep(oled_sleep_time);
            }
            }
            /*netres = CheckNetFile();
                ShowOLEDFill( 0, 45, 256, 23, 0x00 );
                ShowOLEDSmallString( 2, 45, (U8*)netres.c_str(), netres.length() );
                sleep(3);*/
            if(oled_a_time == true)
            {

                timeres = CheckTimeFile_big();
             str_len = timeres.length();
             timeres[str_len -1] = ' ';


             if(timeres.length() > 0){
                printf("CheckCpuFile_big+++++++++++++++++timefile \n");
               oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
               oledmaster_ba-> ShowOLEDBigString( 2, 45, (U8*)timeres.c_str(), timeres.length() );
                sleep(oled_sleep_time);
             }
            }
            if(oled_a_ai == true)
            {
            airate = CheckAiFile_big();
            str_len = airate.length();
            airate[str_len -1] = ' ';
            if(airate.length() > 0){
                printf("CheckCpuFile_big+++++++++++++++++AI \n");
               oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
               oledmaster_ba-> ShowOLEDBigString( 2, 45, (U8*)airate.c_str(), airate.length() );
                sleep(oled_sleep_time);
            }
            }
            if(oled_a_vi == true)
            {
            resolution  = CheckViFile_big();

            str_len = resolution.length();
            resolution[str_len -1] = ' ';

            oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
               printf("CheckCpuFile_big+++++++++++++++++vi=%s   %d\n" ,(U8*)resolution.c_str(), resolution.length());
            oledmaster_ba-> ShowOLEDBigString( 2, 45, (U8*)resolution.c_str(), resolution.length() );
            sleep(oled_sleep_time);
            }
            if(oled_a_venc == true)
            {
            vencres  = CheckVencFile_big();

            str_len = vencres.length();
            vencres[str_len -1] = ' ';

            if(vencres.length() > 0){
                printf("CheckCpuFile_big+++++++++++++++++venc \n");
             oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
            oledmaster_ba->ShowOLEDBigString( 2, 45, (U8*)vencres.c_str(), vencres.length() );
                sleep(oled_sleep_time);
            }
            }
            if(oled_a_ethspeed == true)
            {
            ethspeed  = CheckEthspeedFile_big();
            str_len = ethspeed.length();
            ethspeed[str_len -1] = ' ';
            if(ethspeed.length() > 0){
                printf("CheckCpuFile_big+++++++++ ++++++++speed \n");
            oledmaster_ba-> ShowOLEDFill_big( 0, 45, 240, 23, 0x00 );
             oledmaster_ba-> ShowOLEDBigString( 2, 45, (U8*)ethspeed.c_str(), ethspeed.length() );
                sleep(oled_sleep_time);
            }
            }

        }
        else
        {
           // printf("a bif exit\n");
            big_a_exit = true;
            // usleep(2000);
        }
    }
    else
    {
        break;
    }
}

     if(fTickFile != NULL)
     {
     fclose(fTickFile);
     }

    }



  big_a_exit = true;

    printf("  oled_big_a_loopaa\n");

   return 0 ;
}
void *  oled_small_a_loop(void * arg)
{


    U8	offset = 0;
    string vdecres;
    string devname;
    vector<string> vores;
    vector<string> monitorres;
    string cpures;
    string netres;
     int nFileLen = 0;
     int timer = 0;
     int sync_count = 0;
  FILE* fp = NULL;
  auto oledmaster = Singleton<OLEDMaster>::getInstance();

#if 1
 while(thread_sml_a_loop == true){
  if ((oled_small_start == true) && (big_a_exit == true)){
        small_a_exit = false;
        printf("smallexit = %d  big-exit=%d\n",small_a_exit,big_a_exit);

        if(init_sys_small == true)
        {
            printf(" init_sys_small  start\n");
            oledmaster->initSys();
            oledmaster-> ShowOLEDClear(0, 8);

            getIp();
           if(oled_all_ip == true ){
           oledmaster-> ShowOLEDClear( 2, 4 );


          oledmaster->ShowOLEDString( 1, 2, g_ip[1], strlen((S8*)g_ip[1]) );//第一个参数控制左右移，第二个参数控制上下
           }
            int i, j;
          #if 0
             fp = fopen("chinese.txt","r");//从txt文件内读取字模
            if(fp != NULL){
            while(!feof(fp))
            {
                for(i=0; i<14; i++){

                    for(j=0; j<16; j++)
                   { fscanf(fp, "%x", &chinese_num[i][j]);

                   printf("----china=%x    s=\n",chinese_num[i][j]);
                       }
                }
                break;
            }

            fseek(fp,0,SEEK_END); //定位到文件末
            nFileLen = ftell(fp); //文件长度
            }
#endif
            init_sys_small = false;
//            initSys();
//            ShowOLEDClear(0, 8);
        }

        printf("i ma is small a timr=%d   ai-strt%d\n",oled_sleep_time,oled_a_ai);


 // getIp();
    //printf("%s\n", g_ip[0]);
    if(oled_all_ip == true ){

        printf("aa ip not\n");



    }
    else
        oledmaster->ShowOLEDClear( 3, 4 );

    if(oled_ump_i  == 0 ){
printf("oled_ump_i-----------------------oled_ump_i-\n");

       if(strcmp(((S8*)g_ip[0]),loip))
       {

           printf("have nt \n ");


       }
      else{
           printf("ip_-------------i\n");
      oledmaster->  ShowOLEDClear( 0, 2 );
#if BIG_IP
       oledmaster-> ShowOLEDString( 2, 0, g_ip[1], strlen((S8*)g_ip[1]) );
        }
#else
        ShowOLEDSmallString( 1, 0, g_ip[1], strlen((S8*)g_ip[1]) );
#endif




    }
     else  if(oled_ump_i == 2)
       {
//         sleep(oled_sleep_time);
           sprintf( ((S8*)g_ip[0]),"%s",oled_ump_i_data);
           oledmaster->  ShowOLEDClear( 0, 2 );
         printf(" oeldn = %s   gip=%s\n",oled_ump_i_data,((S8*)g_ip[0]));
           oledmaster-> ShowOLEDString( 2, 0, g_ip[0], strlen((S8*)g_ip[0]) );

       }

    //显示可执行文件后参数

    if(oled_ump_n == true)
    {
 sleep(oled_sleep_time);
//            oled_n_start = oled_data;
//            devname = oled_n_start + 9 ;

            if(oled_ump_n_data == true)
            {
                oled_n_start = oled_data;
                devname = oled_n_start + 9 ;
            }
            else
            {
                oled_n_start = oled_data;
                devname = oled_n_start;
            }
            //   oled_n_start +
       oledmaster-> ShowOLEDClear( 0, 2 );
        //devname = oled_data ;

      //  printf("--- -daa=%s  -=%s\n", oled_data, oled_n_start + 9);
      //  printf("**************************%s   ---%d\n",(U8*)devname.c_str(), devname.length());
        oledmaster->ShowOLEDString( 2, 0, (U8*)devname.c_str(), devname.length() );


    }

     if( oled_a_cup == true)
     {
          printf("oled_aal_cpu\n");
          sleep(oled_sleep_time);
          oledmaster-> ShowOLEDClear( 0, 2 );
            //cpures  = CheckCpuFile_big();
      string cpu_cpy = CheckCpuFile_big();
      char cpu_cpy_buf[24];
      strncpy(cpu_cpy_buf,(char *)cpu_cpy.c_str(),10);

       cpures = cpu_cpy_buf;

           printf("data   cpu -small=%slen=%d\n",(U8 *)cpures.c_str(),cpures.length());
            oledmaster->  ShowOLEDString( 2, 0, (U8*)cpures.c_str(), cpures.length() );
             oledmaster->  ShowOLEDString( 2, 0, (U8*)cpures.c_str(), cpures.length() );

          // oledmaster->ShowOLEDSmallString
            // ShowOLEDBigString(2,0,(U8*)cpures.c_str(), cpures.length());

     }

        if (oled_a_ai == true)
      {  sleep(oled_sleep_time);
           oledmaster-> ShowOLEDClear( 0, 2 );


         string  airate = CheckAiFile_big();
         printf(" --------aioled_sleep_time=%d\n",oled_sleep_time);
         //printf("**************************%s   ---%d\n",(U8*)devname.c_str(), devname.length());
        oledmaster-> ShowOLEDString( 2, 0, (U8*)airate.c_str(), airate.length() );

     }

    #if 1
        if( oled_a_venc == true )
        { //sleep(1);
            string vencres  = CheckVencFile_big();
            oledmaster->  ShowOLEDClear( 0, 2 );
            oledmaster->  ShowOLEDString( 2, 0, (U8*)vencres.c_str(), vencres.length() );
    printf(" ------venc--oled_sleep_time=%d\n",oled_sleep_time);

        }

        if (oled_a_ethspeed == true)

        {    printf(" --eth------aioled_sleep_time=%d\n",oled_sleep_time);

              sleep(oled_sleep_time);
            string ethspeed  = CheckEthspeedFile_big();
            oledmaster-> ShowOLEDClear( 0, 2 );
            oledmaster-> ShowOLEDString( 2, 0, (U8*)ethspeed.c_str(), ethspeed.length() );

        }
        if  ( oled_a_time == true)
        {
             sleep(oled_sleep_time);
            string timeres = CheckTimeFile_big();
            oledmaster->  ShowOLEDClear( 0, 2 );
            oledmaster->  ShowOLEDString( 2, 0, (U8*)timeres.c_str(), timeres.length() );

        }
        if(  oled_a_vi == true)
        {
            sleep(oled_sleep_time);
            string resolution  = CheckViFile_big();
            oledmaster->  ShowOLEDClear( 0, 2 );
            oledmaster-> ShowOLEDString( 2, 0, (U8*)resolution.c_str(), resolution.length() );

        }

#endif

    //汉字显示：最多显示7个字
//    if(oled_smala_china == true){
//        ShowOLEDClear( 0, 4 );
//        ShowOLEDChinese( 2, 0, 7);
//    }
   // sleep(2);

    //显示第二个IP
    //g_ip[1] = {"9"};




    }
  else
  {
      small_a_exit = true;

  }

}
#endif

//small_a_exit = true;

printf("small_exit");


    return 0;

}

void * oled_ump(void *arg)
{
    int  len  = 0;
  int i = 0;
    uint port = *(uint*)arg;
    char data[1024] = "";
    vector<string> content;
  string devname;
   char name_oled[100] = {0};

char  * oled_n_buf = NULL;
char oled_i_buf[20] ;
char * ump_send_buf = NULL ;
//char  recv_data_buf[20];
char data_recv = {0};
 int kind_who = 0;
 char recv_data_all[1024] = {0};
auto thisini = Singleton<SoftwareConfig>::getInstance();

     int send_ump_len ;
    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false); //   venc  not userget  vencchn 12   because   cat /pore/umap/venc
    int b = 0;
 //JsonParse* jsonparse = new JsonParse();
    while(thread_oled_ump == true){
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){

            if((strlen(data) > 35) )
            {
                strcpy(recv_data_all,data);
                printf("all recv=%s\n",recv_data_all);
                if(strstr(recv_data_all,"kind") )
                {

//                    ump_send_buf   =  recv_data_all +4 ;
//                    printf("-----------a=%s   c=%c\n",ump_send_buf,ump_send_buf[0]);
//                    if(strstr(ump_send_buf,"ba=1"))
//                    {
//                            printf("ump_send_buf---- \n");
//                    }
//                    else if(strstr(ump_send_buf,"ba=0"))
//                    {


//                    }

                    if(strstr((recv_data_all),"kind=0"))
                    {
                        printf("kindab == 0\n");
                      //  init_sys_big = true;
                        oled_big_start = true;
                        oled_small_start = false;

                          kind_who = 0;

                          thisini->SetConfig(SoftwareConfig::Oled_a_kind_start,int2str(kind_who));


                    }
                    else if(strstr((recv_data_all),"kind=1"))
                    {
                        printf("kindsa = 1\n");
                       // init_sys_small = true ;
                        oled_small_start = true;
                        oled_big_start = false;

                        kind_who = 1;

                        thisini->SetConfig(SoftwareConfig::Oled_a_kind_start,int2str(kind_who));
                    }
                    else if(strstr((recv_data_all),"kind=2"))
                    {
                       printf("close all \n");

                       oled_small_start = false;
                       oled_big_start = false;
                       kind_who = 2;

                       thisini->SetConfig(SoftwareConfig::Oled_a_kind_start,int2str(kind_who));
                     }


                      thisini->SaveConfig();




                }
                for (i = 0;i  < strlen(recv_data_all); i++)
                {


                    if(recv_data_all[i] == '|')
                    {

                          ump_send_buf = recv_data_all + i +1;
                       // memccpy(ump_send_buf,recv_data_all,'|',40);
//                          send_len = strlen(ump_send_buf);

                         printf("\n1-----------=%s   c=%c\n",ump_send_buf,ump_send_buf[0]);



                            if(strstr((ump_send_buf),"lenan=0"))
                            {
                                printf("data_smaln = 0\n" );
                                oled_ump_n = false;
                                thisini->SetConfig(SoftwareConfig::Oled_a_small_n_start,  int2str(oled_ump_n));

                                 thisini->SaveConfig();
                            }
                            else if(strstr((ump_send_buf),"lenan=1"))
                            {
                                oled_ump_n = true;
                                //oled_ump_i = true;
                                thisini->SetConfig(SoftwareConfig::Oled_a_small_n_start,  int2str(oled_ump_n));

                                 thisini->SaveConfig();
                                printf("data-smaln=1\n");
                            }

                            else if ( (strstr(ump_send_buf, "ipan"))     ){

                                data_recv = ump_send_buf[5];
                                printf("smalsend me -----\n");
                                if(data_recv == '1')
                                {
                                    printf("data_recv =%c\n",data_recv);
                                   // oled_ump_n = false;
                                    oled_ump_i = 1;

                                }
                                else if(data_recv == '2')
                                 {
                                    //oled_ump_n = ;
                                    oled_ump_i = 2;
                                    printf("data-cer=0\n");

                                 }
                                else
                                {
                                      oled_ump_i = 0;
                                }


                                //thisini->SetConfig(Config::Oled_0_small_n_start,  int2str(oled_ump_i));
                                thisini->SetConfig(SoftwareConfig::Oled_a_small_i_start, int2str(oled_ump_i));
                                  thisini->SaveConfig();
                            }


                            else if ( (strstr(ump_send_buf, "ipbufan=")) ){

                                printf("aaaaaaa\n");
                               // int i = 0;
                               // sleep( 4);
                                char p_oled[56];
                            //   char  p_oled[1024] = ump_send_buf;
                        memccpy(p_oled,ump_send_buf,'|',15);

                              char * ip_oled_1 = p_oled + 8 ;

                         //  char my_ip[20] = "128.0.129.111";

                           printf("---------ifall-=%s---------ip=%s\n",p_oled,ip_oled_1);
                           sprintf(oled_ump_i_data, "%s", ip_oled_1);
                        //  oled_i_buf = ip_oled_1;


                           // thisini->SetConfig(SoftwareConfig::Oled_a_small_i_start, int2str(oled_ump_i));
                         //   thisini->SetConfig(SoftwareConfig::Oled_a_small_n_start,  int2str(oled_ump_n));
                            thisini->SetConfig(SoftwareConfig::Oled_a_small_i_buf, (oled_ump_i_data ));
                            thisini->SaveConfig();

                           // send_ump_start = true;


                            }


                        else if(strstr(ump_send_buf,"lenanbuf="))
                        {
                            printf("snalnbuf\n");
                          //  strcpy (oled_data,ump_send_buf);
                          if( oled_big_start == true)
                         memccpy(oled_data,ump_send_buf,'|',28);
                         else if (oled_small_start == true)
                           memccpy(oled_data,ump_send_buf,'|',15);
                              //   printf("oled_data=%s",oled_data + 9);

                          int len_lenan= strlen(oled_data);
                          oled_data[len_lenan -1] = ' ';

                            oled_n_buf = oled_data;
                    //oled_data  = oled_n_buf ;
                                oled_ump_n_data = true;
                           printf("//////// n:%s\n",oled_n_buf + 9) ;
                             thisini->SetConfig(SoftwareConfig::Oled_a_small_n_buf, (oled_n_buf + 9));

                            thisini->SaveConfig();







                        }
                          else if(strstr(ump_send_buf,"cpua"))

                        {
                                data_recv = ump_send_buf[5];
                                if(data_recv == '1')
                                oled_a_cup = true;
                                else
                                oled_a_cup = false;

                                thisini->SetConfig(SoftwareConfig::Oled_a_cpu, int2str(data_recv));
                                  thisini->SaveConfig();


                        }
                            else if(strstr(ump_send_buf,"via"))

                          {
                                data_recv = ump_send_buf[4];
                                if(data_recv == '1')
                                    oled_a_vi = true;
                                    else
                                    oled_a_vi = false;

                                thisini->SetConfig(SoftwareConfig::Oled_a_vi, int2str(data_recv));
                                  thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"aia"))

                          {
                                data_recv = ump_send_buf[4];
                                if(data_recv == '1')
                                    oled_a_ai = true;
                                else
                                    oled_a_ai = false;

                                thisini->SetConfig(SoftwareConfig::Oled_a_ai, int2str(data_recv));
                                  thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"neta"))

                          {
                                data_recv = ump_send_buf[5];
                                if(data_recv == '1')
                                   oled_a_ethspeed = true;
                                else
                                    oled_a_ethspeed  = false;

                                thisini->SetConfig(SoftwareConfig::Oled_a_net, int2str(data_recv));
                                  thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"stara"))
                           {
                                data_recv = ump_send_buf[6];

                                if(data_recv == '1')
                                    oled_a_time = true;
                                else
                                    oled_a_time =false;

                                thisini->SetConfig(SoftwareConfig::Oled_a_start, int2str(data_recv));
                                  thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"ven"))

                          {
                                data_recv = ump_send_buf[4];
                                if(data_recv == '1')
                                    oled_a_venc = true;
                                else
                                    oled_a_venc = false;

                                thisini->SetConfig(SoftwareConfig::Oled_a_venc, int2str(data_recv));
                                  thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"alltim"))

                          {
                                char tim_c = ump_send_buf[7];



                                  oled_sleep_time = (tim_c - 48);

                                  if(oled_sleep_time < 2)
                                  {
                                  oled_sleep_time = 2;
                                  }

                                   printf("time =%d\n",oled_sleep_time);

                                   thisini->SetConfig(SoftwareConfig::Oled_a_sleep_tim, int2str(oled_sleep_time));
                                     thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"allip"))

                          {
                                  data_recv = ump_send_buf[6];
                                 printf("data wa allip -------------\n");
                                  if( data_recv == '0')
                                   {
                                       printf("data  allip --------------= 0\n");
                                       oled_all_ip = false;
                                   }else if( data_recv == '1')
                                   {
                                       oled_all_ip =  true;
                                   }

                                   thisini->SetConfig(SoftwareConfig::Oled_a_all_ip, int2str(data_recv));
                                     thisini->SaveConfig();


                          }
                            else if(strstr(ump_send_buf,"jointj"))

                          {

                                data_recv = ump_send_buf[7];
                                //int shiyan =100;
                                //thisini->SetConfig(Config::Oled_shiyan, int2str( shiyan));
                               //  thisini->SetConfig(SoftwareConfig:: oled_jpio_j);
                                thisini->SetConfig(SoftwareConfig::OLed_a_gpio_j, int2str( data_recv));
                                thisini->SaveConfig();

                          }
                    }


                }


            }

           // }
    #if 1


             else if ( (strstr(data, "jointj")) &&(strlen(data) < 9 ))
             {
                data_recv = data[7];
                //int shiyan =100;
                //thisini->SetConfig(Config::Oled_shiyan, int2str( shiyan));
               //  thisini->SetConfig(SoftwareConfig:: oled_jpio_j);
                thisini->SetConfig(SoftwareConfig::OLed_a_gpio_j, int2str( data_recv));
                thisini->SaveConfig();
//Oled_shiyan
             }
             else if ( (strstr(data, "ven")) &&(strlen(data) < 9 )){
            data_recv = data[4];
            if(data_recv == '1')
                oled_a_venc = true;
            else
                oled_a_venc = false;

            thisini->SetConfig(SoftwareConfig::Oled_a_venc, int2str(data_recv));
              thisini->SaveConfig();
}
              else if ( (strstr(data, "stara")) &&(strlen(data) < 9 )){
                data_recv = data[6];

                if(data_recv == '1')
                    oled_a_time = true;
                else
                    oled_a_time =false;

                thisini->SetConfig(SoftwareConfig::Oled_a_start, int2str(data_recv));
                  thisini->SaveConfig();


            }
              else if ( (strstr(data, "neta")) &&(strlen(data) < 9 )){
                data_recv = data[5];
                if(data_recv == '1')
                   oled_a_ethspeed = true;
                else
                    oled_a_ethspeed  = false;

                thisini->SetConfig(SoftwareConfig::Oled_a_net, int2str(data_recv));
                  thisini->SaveConfig();


            }
              else if ( (strstr(data, "aia")) &&(strlen(data) < 9 )){
                    data_recv = data[4];
                    if(data_recv == '1')
                        oled_a_ai = true;
                    else
                        oled_a_ai = false;

                    thisini->SetConfig(SoftwareConfig::Oled_a_ai, int2str(data_recv));
                      thisini->SaveConfig();

                }
              else if ( (strstr(data, "via")) &&(strlen(data) < 9 )){
                    data_recv = data[4];
                    if(data_recv == '1')
                        oled_a_vi = true;
                        else
                        oled_a_vi = false;

                    thisini->SetConfig(SoftwareConfig::Oled_a_vi, int2str(data_recv));
                      thisini->SaveConfig();

                    }
             else if ( (strstr(data, "cpua")) &&(strlen(data) < 9 )){
                   data_recv = data[5];
                   if(data_recv == '1')
                   oled_a_cup = true;
                   else
                   oled_a_cup = false;

                   thisini->SetConfig(SoftwareConfig::Oled_a_cpu, int2str(data_recv));
                     thisini->SaveConfig();
               }
            else if ( (strstr(data, "allip")) &&(strlen(data) < 9 )){
                data_recv = data[6];
                if(data[6] == '0')
                   {
                       printf("data=akkip = 0\n");
                       oled_all_ip = false;
                   }else if(data[6] == '1')
                   {
                       oled_all_ip =  true;
                   }

                   thisini->SetConfig(SoftwareConfig::Oled_a_all_ip, int2str(data_recv));
                     thisini->SaveConfig();
            }


            else if ( (strstr(data, "alltim")) &&(strlen(data) < 10 )){



                   char tim_c = data[7];



                     oled_sleep_time = (tim_c - 48);

                     if(oled_sleep_time < 2)
                     {
                         oled_sleep_time = 2;
                     }

                      printf("time =%d\n",oled_sleep_time);



                      thisini->SetConfig(SoftwareConfig::Oled_a_sleep_tim, int2str(oled_sleep_time));
                        thisini->SaveConfig();

            }

            else if ( (strstr(data, "lenan=")) && (strlen(data) < 7) ){


                if(strstr((data),"smlan=0"))
                {
                    printf("data_smaln = 0\n" );
                    oled_ump_n = true;
                }
                else if(strstr((data),"smlan=1"))
                {
                    oled_ump_n = false;
                    //oled_ump_i = true;
                    printf("data-smaln=1\n");
                }


                thisini->SetConfig(SoftwareConfig::Oled_a_small_n_start,  int2str(oled_ump_n));
              //  thisini->SetConfig(Config::Oled_0_small_i_start, int2str(oled_ump_i));
                  thisini->SaveConfig();
            }
            else if ( (strstr(data, "ipan")) && (strlen(data) < 17) ){

                data_recv = data[5];
                printf("smalsend me -----\n");
                if(data_recv == '1')
                {
                    printf("data_recv =%c\n",data_recv);
                   // oled_ump_n = false;
                    oled_ump_i = 1;

                }
                else if(data_recv == '2')
                 {
                    //oled_ump_n = ;
                    oled_ump_i = 2;
                    printf("data-cer=0\n");

                 }
                else
                {
                      oled_ump_i = 0;
                }


                //thisini->SetConfig(Config::Oled_0_small_n_start,  int2str(oled_ump_i));
                thisini->SetConfig(SoftwareConfig::Oled_a_small_i_start, int2str(oled_ump_i));
                  thisini->SaveConfig();
            }
            else if ( (strstr(data, "lenanbuf=")) && (strlen(data) < 25) ){




                 strcpy (oled_data,data);
                oled_ump_n_data = true;


                 oled_n_buf = oled_data;


                 printf("**************** n:%s\n",oled_n_buf+9 ) ;
                  thisini->SetConfig(SoftwareConfig::Oled_a_small_n_buf, ( oled_n_buf +9 ));

                 thisini->SaveConfig();


            }
            else if ( (strstr(data, "ipbufan")) && (strlen(data) < 27) ){

               // int i = 0;
               // sleep( 4);
               char * p_oled = data;

              char * ip_oled_1 = p_oled + 8 ;

         //  char my_ip[20] = "128.0.129.111";
           printf("ip=%s\n",ip_oled_1);
           sprintf(oled_ump_i_data, "%s", ip_oled_1);
        //  oled_i_buf = ip_oled_1;

        printf("oled_ump_i_data=%s\n",oled_ump_i_data);
           // thisin i->SetConfig(SoftwareConfig::Oled_a_small_i_start, int2str(oled_ump_i));
         //   thisini->SetConfig(SoftwareConfig::Oled_a_small_n_start,  int2str(oled_ump_n));
            thisini->SetConfig(SoftwareConfig::Oled_a_small_i_buf, (oled_ump_i_data ));
            thisini->SaveConfig();

           // send_ump_start = true;


            }
             else if((strstr(data,"kind")) && (strlen(data) < 10))
            {

              // data_recv = data[4];

               printf("dad_recv==%s\n",data_recv);

               if(strstr((data),"kind=0"))
               {
                   printf("kind == 0\n");
                   init_sys_big = true;
                   oled_big_start = true;
                   oled_small_start = false;

                     kind_who = 0;

                     thisini->SetConfig(SoftwareConfig::Oled_a_kind_start,int2str(kind_who));


               }
               else if(strstr((data),"kind=1"))
               { oled_big_start = false;
                   printf("kindsa = 1\n");

                   sleep(1);
                   init_sys_small = true ;
                   oled_small_start = true;

//                   if(big_a_exit == true)
//                   {
//                   int big_creat1 = 0;
//                   pthread_t oled_tid_small = CreateThread(oled_small_a_loop, 0, SCHED_FIFO, true, &big_creat1 );
//                   if (oled_tid_small == 0){
//                       COMMON_PRT("pthread_create() failed: snapServerProc\n");
//                   }
//                   }
                   kind_who = 1;

                   thisini->SetConfig(SoftwareConfig::Oled_a_kind_start,int2str(kind_who));
               }
               else if(strstr((data),"kind=2"))
               {
                  printf("close all \n");

                  oled_small_start = false;
                  oled_big_start = false;
                  kind_who = 2;

                  thisini->SetConfig(SoftwareConfig::Oled_a_kind_start,int2str(kind_who));
                }


                 thisini->SaveConfig();

               //  send_ump_start = true;


            }
            else if((strstr(data,"replyoled")) && (strlen("replyoled")) )
            {

                    ump_send_me = true;

                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));
                    COMMON_PRT("--2--ip: %s, port: %s----0\n", content[0].c_str(), content[1].c_str());

            //   printf("%d\n",(uint)str2int(content[1]));

              send_ump_len = udpsocket->SendTo(content[0].c_str(),(uint)str2int(content[1]), oled_data_ump, strlen(oled_data_ump));
            sprintf(ump_ip,"%s",content[0].c_str());
           //sprintf( &ump_port,"%d",content[1].c_str());

            ump_port =  (uint)str2int(content[1]);
            udpsocket_ump = udpsocket;

          printf("umo_ip =%s um_port=%d\n",ump_ip,ump_port);
          send_ump_start = true;

          send_ump_Thread();

                   // jsonparse->LoadJson(msgdata.mtext.msg);
//                    jsonparse->LoadJson(data);
//                    if(jsonparse->CheckJsonFormat()){
//                        jsonparse->IterationParseStart();

//                    }
//                    else{
//                    printf("-------oeld_ump have data\n");
//                    }

            }
            else
                printf("ump send me have`nt error ?? \n");
       #endif
        }





    }

    delete udpsocket;

    return 0;
}


void init_argumnt(int oled_moudle)
{
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    auto oledmaster = Singleton<OLEDMaster>::getInstance();
     auto oledmaster_ba = Singleton<OLEDBaMaster>::getInstance();
    int kind_oled  = (int)str2int(thisini->GetConfig(SoftwareConfig::Oled_a_kind_start));
    char oled_n_buf[56];

     oled_ump_i = (int)str2int(thisini->GetConfig(SoftwareConfig::Oled_a_small_i_start));
     oled_ump_n = (bool)str2int(thisini->GetConfig(SoftwareConfig::Oled_a_small_n_start));
     sprintf(oled_ump_i_data,"%s",thisini->GetConfig(SoftwareConfig::Oled_a_small_i_buf).c_str());

      sprintf((S8 *)g_ip[1],"%s",thisini->GetConfig(SoftwareConfig::Oled_a_small_i_buf).c_str());
      sprintf(oled_n_buf,"%s",thisini->GetConfig(SoftwareConfig::Oled_a_small_n_buf).c_str());
    int i = 0;
    if((strlen(oled_n_buf) > 15) &&(kind_oled == 1))
    {


        strncpy(oled_data,oled_n_buf,15);


    }
    else if((strlen(oled_n_buf) > 28) &&(kind_oled == 0))
    {
        strncpy(oled_data,oled_n_buf,28);

     //   printf(" big oled_nlen   oled_data=%s\n",oled_data);
    }
    else
    {
        sprintf(oled_data,"%s",oled_n_buf);
    }


      oled_a_cup  = (bool)str2int(thisini->GetConfig(SoftwareConfig::Oled_a_cpu));
      oled_a_ai  = (bool)str2int(thisini->GetConfig(SoftwareConfig::Oled_a_ai));
       oled_a_vi = (bool)str2int(thisini->GetConfig(SoftwareConfig::Oled_a_vi));
       oled_a_ethspeed  = (bool)str2int(thisini->GetConfig(SoftwareConfig:: Oled_a_net));
      oled_a_venc  = (bool)str2int(thisini->GetConfig(SoftwareConfig:: Oled_a_venc));
      oled_a_time  = (bool)str2int(thisini->GetConfig(SoftwareConfig:: Oled_a_start));
      oled_sleep_time  = (int)str2int(thisini->GetConfig(SoftwareConfig:: Oled_a_sleep_tim));
       oled_all_ip  = (bool)str2int(thisini->GetConfig(SoftwareConfig:: Oled_a_all_ip));
       oled_jpio_j  = (int)str2int(thisini->GetConfig(SoftwareConfig:: OLed_a_gpio_j));

     //  char shiyanmy[15];
     //     sprintf( shiyanmy   , "%s",thisini->GetConfig(Config::OLed_sml_SCL_DIR).c_str());
     // printf("______________________=%s\n",shiyanmy);


       int oled_who = oled_moudle;
#if 1


#endif



      // oled_data
      printf("time sleep >>>>>>>>>>>>>>>>>>>>> %d\n",oled_sleep_time);
       if(kind_oled == 0)
      {
         oled_big_start = true;
         oled_small_start = false;

     }else if(kind_oled == 1)
     {
         oled_big_start = false;
         oled_small_start = true;

     }
       else
       {
           oled_big_start = false;
           oled_small_start = false;
       }
  //snprintf(buf_send, MAXDATA - 1, "{\n\"node_model\":\"%s\",\n\"node_board\":\"%s\",\n\"hw_name\":\"%s\",\n\"tick_module\":\"slave\",\n\"node_ip\":\"%s\",\n\"node_mac\":\"%s\",\n\"node_name\":\"%s\",\n\"version\":%s,\n\"uuid\":\"%s\"\n}", NODE_MODEL, node_board.c_str(), hw_name.c_str(), localip, macaddr, name.c_str(), ver.c_str(), uuid.c_str());


   #if 0
       if (1){
        char oled_File_buf[1024] = {0};
        int  oled_File = open("/data/oled.ini",O_RDONLY);
        if(oled_File == NULL)
        {
            printf("error  open have`nt /data/oled.ini  \n");

        }
        else{

            bzero(oled_File_buf,sizeof(oled_File_buf));
            //read
            int ret_read = read(oled_File,oled_File_buf,sizeof(oled_File_buf));
            if(ret_read < 0)
            {
                perror("fail to read");
                //return -1;
            }
            else if(ret_read == 0) //end_of_file
            {
                printf("end\n");
               // break;
            }
        //   printf("read_buf=%s\n",oled_File_buf);


        }

        sprintf(oled_data_ump,"[loed]\n");
        strcpy(&oled_data_ump[7],oled_File_buf);
}

  //  sprintf(oled_data_ump,"{\n\[oled]\n\"Oled_a_big_start=%d\",\n\"Oled_a_small_start=%d\",\n\"Oled_a_small_i_start=%d\",\n\"Oled_a_small_n_start=%d\",\n\"Oled_a_small_i_buf=%s\",\n\"Oled_a_small_n_buf=%s\",\n\"Oled_a_small_china_start=%d\"\n}",oled_big_start,
           // oled_small_start,oled_ump_i,oled_ump_n,(S8*)g_ip[1],oled_data,oled_smala_china);

     //  sprintf(oled_data_ump,"%s","oled_bigstart);
   //  printf("oled_big_start=%d  i--g_ip=%s  oled_ump_n=%d oled_data =%s *********\n",oled_big_start,(S8*)g_ip[1],oled_ump_n,oled_data);

//    printf("all=%s",oled_data_ump);
     //oled_dat_ump
#endif
     // return 0;
}
void HandleSig(int signo)
{
 auto oledmaster = Singleton<OLEDMaster>::getInstance();
   thread_big_a_loop = false;
   thread_sml_a_loop = false;
   oled_big_start = false;
   oled_small_start = false;
   thread_oled_ump = false;
   oledmaster->IICStop( );


   oledmaster->WriteByteOLED( 0xAE, 0x00 );
    sleep(1);
    main_exit_while =false;
    printf("exit\n");
}
#endif
INT32 main(int argc, char * argv[]){

    signal(SIGINT, HandleSig); //NOTE: 检测Ctrl+C按键
    signal(SIGTERM, HandleSig); //NOTE: 检测killall 命令
    signal(SIGHUP, HandleSig); //NOTE: 关闭终端
    signal(SIGSEGV, HandleSig); //NOTE: 检测 Segmentation fault!

    auto thisini = Singleton<SoftwareConfig>::getInstance();
    thisini->ReadConfig();

    thisini->PrintConfig();

   thisini->SaveConfig();
#if 1
   // auto oled_ini = Singleton<OLEDMaster>::getInstance();

    //oled_ini->OLED_lei();


    U8	offset = 0;
    string vdecres;
    string devname;
    vector<string> vores;
    vector<string> monitorres;
    string cpures;
    string netres;
    display_length = 130;




    char version[32] ={0};
    memset(version,0,sizeof(version));
    readHdVersion(version);
    int reg_value = 0;
    // int who_led = -1;

    if(!strncmp(version,HI3531D_4MIX_INPUT,strlen(HI3531D_4MIX_INPUT))) {
        printf("this chip is 4hdmi input\n");
        who_led = 0;
    }
    else if(!strncmp(version,HI3531D_4MIX_INPUT_MIX,strlen(HI3531D_4MIX_INPUT_MIX))) {
        printf("this chip is 4mix input\n");
        who_led = 0;
    }
    else if(!strncmp(version,HI3531D_NJ_JIU_6PIN,strlen(HI3531D_NJ_JIU_6PIN))) {
        printf("this chip is  input  nanjing\n");
        who_led = 1;
    }

    else if(!strncmp(version,HI3531D_GZ_JIU_6PIN,strlen(HI3531D_GZ_JIU_6PIN))) {
        printf("this chip is 4mix GAUNGZHOU\n");
        who_led = 1;
    }
    else if(!strncmp(version,HI3531D_GUANGZHOU_VDEC,strlen(HI3531D_GUANGZHOU_VDEC))) {
        printf("this chip is 4mix HI3531D_GUANGZHOU_VDEC\n");
        who_led = 2;
    }
    else if(!strncmp(version,HI3531D_DOUBLEDECK_HDMI,strlen(HI3531D_DOUBLEDECK_HDMI))) {
        printf("this chip is  input  HI3531D_DOUBLEDECK_HDMI\n");
        who_led = 3;
    }
    else if(!strncmp(version,HI3531D_4HDMI,strlen(HI3531D_4HDMI))) {
        printf("this chip is 4mix HI3531D_4HDMI\n");
        who_led = 4;
    }
    else if(!strncmp(version,HI3521D_SDI,strlen(HI3521D_SDI))) {
        printf("this chip is  input  nanjing\n");
        who_led = 5;
    }
    else if(!strncmp(version,HI3521D_2HDMI,strlen(HI3521D_2HDMI))) {
        printf("this chip is  SHNJ23015d_200_V1   \n");
        who_led = 6;
    }
    else if(!strncmp(version,HI3521D_LOOP_V2,strlen(HI3521D_LOOP_V2))) {
        printf("this chip is  input  SHNJ23015d_100_V2\n");
        who_led = 7;
    }
    else if(!strncmp(version,HI3521D_LOOP_V1,strlen(HI3521D_LOOP_V1))) {
        printf("this chip is  input  SHNJ23015d_100_V1 \n");
        who_led = 8;
    }


    else
    {
        printf(" error have`nt led  ?????  \n");
    }







    printf("*******************%d\n",who_led);
    init_argumnt(who_led);
    auto oledmaster = Singleton<OLEDMaster>::getInstance();
    auto oledmaster_ba = Singleton<OLEDBaMaster>::getInstance();
    oledmaster->OLED_init_gpio(who_led);
    oledmaster_ba->oled_ba_init(who_led);


    if(oled_jpio_j != 0)
    {
        if(oled_small_start == true)
        {
            oledmaster->oled_gpio_sml();
        }

        else  if((oled_big_start == true))
        {
            oledmaster_ba->oled_gpio_ba();
        }


    }



    int oled_port = 16124;
    pthread_t oled_tid = CreateThread(oled_ump, 0, SCHED_FIFO, true, &oled_port);
    if (oled_tid == 0){
        COMMON_PRT("pthread_create() failed: snapServerProc\n");
    }
    if (oled_big_start == true)
    {
        printf(" CreateThread(oled_big_a_loo\n");
    int big_creat = 0;
    pthread_t oled_tid_big = CreateThread(oled_big_a_loop, 0, SCHED_FIFO, true, &big_creat );
    if (oled_tid_big == 0){
        COMMON_PRT("pthread_create() faile d: snapServerProc\n");
    }
    }
     if(oled_small_start == true)
     {
         printf("CreateThread(oled_small_a_loop\n");
    int big_creat1 = 0;
    pthread_t oled_tid_small = CreateThread(oled_small_a_loop, 0, SCHED_FIFO, true, &big_creat1 );
    if (oled_tid_small == 0){
        COMMON_PRT("pthread_create() failed: snapServerProc\n");
    }
     }









    while(1)
    {
        if(main_exit_while == false)
        {
            printf("exit main\n");
            return 0;
        }
        else
        { sleep(1);
        }
    }




   return 1;
#endif
}


//字符串分割函数




VOID ipToNumber( string str, INT32 eth )
{
    string  strtmp = str;
    INT8    buf[4][10];
    INT8    ipBuf[50] = "";

    sprintf((S8*)g_ip[eth], "%s", str.c_str());

    memset(buf,  0, sizeof(buf));
    memset(&g_hexip[eth], 0, sizeof(g_hexip[eth]));

    for (INT32 i = 0; i < 4; i++){
	sprintf(buf[i], "%02x", atoi(strtmp.c_str())); //printf("buf: %x\n", buf[i]);

	if ( strtmp.find(".") != string::npos )
	    strtmp = strtmp.c_str() + strtmp.find(".") + 1;
    }

    sprintf(ipBuf, "0x%s%s%s%s", buf[0], buf[1], buf[2], buf[3]);
    StrToNumber(ipBuf , &g_hexip[eth]);

    return ;
}

VOID macToNumber( INT8 macTmp[], INT32 eth )
{
    INT8 macBuf[6]  = "";
    U16  len        = strlen(macTmp);

    memset(&g_mac[eth], 0, sizeof(g_mac[eth]));

    for ( U16 i = 0; i < len; i++ ){
	memset(macBuf, '\0', sizeof(macBuf));
	sprintf(macBuf, "0x%c", macTmp[i]);
    StrToNumber(macBuf , &g_mac[eth][i]); //printf("g_mac[%d][%d]:  %x, %d\n", eth, i, g_mac[eth][i], g_mac[eth][i]);
    }

    return ;
}

BOOL getIp()
{
    INT8 macTmp[20] = "";
    INT32 j = 0;
    g_ethnum = peek_interfaces(g_sock);

    printf("ip--------------=%d\n",g_ethnum);
    for(INT32 i = 0; i < g_ethnum; ++i) {
	memset(macTmp, 0, sizeof(macTmp));

	sockaddr_in* sai = (sockaddr_in*)&ifs[i].ifr_addr;
    if(strcmp(inet_ntoa(sai->sin_addr),loip))
    {
        ipToNumber(inet_ntoa(sai->sin_addr), i);
     printf("i=%d--ip-%s\n",i,((S8*)g_ip[i]));
        print_hw_addr(g_sock, ifs[j].ifr_name, macTmp);
	    macToNumber(macTmp, j);
	    j=j+1;
    }
      printf("for=%d--ip-%s\n",i,((S8*)g_ip[i]));
    }

    return 1;
}






#if 0

#endif

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */





