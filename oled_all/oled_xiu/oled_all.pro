
OBJECTS_DIR += ./objs
TARGET = oled

#INCLUDEPATH += /home/hwyin/work/l_led/OLED-small_oled/oled_small_hi3531d_5_19_ok/include
              # /home/hwyin/work/xjh_4in/hi3531d_4in_venc-master/hi3531d_venc/common


SOURCES += \
	main.cpp \
    common.cpp \
    udpsocket/udpsocket.cpp \
    software_config.cpp \
    oled.cpp \
    readsys.cpp \
    memmap.cpp \
    hi_reg/hi_reg.c


HEADERS += \
	oled.h \
    common.h \
    udpsocket/networkinfo.h \
    udpsocket/udpsocket.h \
    software_config.h \
    singleton.h \
    readini.h \
    memmap.h \
    hi_type.h \
    oled_type.h \
    hi_reg/hi_reg.h \
    version.h \
    boost/lexical_cast.hpp


