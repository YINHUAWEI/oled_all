#ifndef __OLED_H__
#define __OLED_H__


//#include <stdio.h>
//#include <string.h>
//#include <unistd.h>


//#include "alphabet.h"
#include "memmap.h"

#define GET_0Byte_32(ul) ((U32)ul & 0x000000FF)
#define GET_1Byte_32(ul) ((U32)ul & 0x0000FF00) >> 8
#define GET_2Byte_32(ul) ((U32)ul & 0x00FF0000) >> 16
#define GET_3Byte_32(ul) ((U32)ul & 0xFF000000) >> 24

#define GET_HI8_16(us)  ((U16)us >> 8)
#define GET_LO8_16(us)  ((U16)us & 0x00FF)

#define SET_HI8_16(us, v) (((U16)us & 0x00FF) | (v << 8))
#define SET_LO8_16(us, v) (((U16)us & 0xFF00) | (v))

/**/
#define GET_HI16_32(dw)  ((dw) >> 16)
#define GET_LO16_32(dw)  ((dw) & 0x0000FFFF)

#define SET_HI16_32(dw, v) ((dw & 0x0000FFFF) | (v << 16))
#define SET_LO16_32(dw, v) ((dw & 0xFFFF0000) | (v))

#define GET_LO32_64(dw)  ((dw) & 0x00000000FFFFFFFF)

/*����16λ����, ��nbitΪv*/
/*15...3 2 1 0*/
#define SET_BIT0_16(us, v)   ((us & 0xFFFE) | v)
#define SET_BIT1_16(us, v)   ((us & 0xFFFD) | (v << 1))
#define SET_BIT2_16(us, v)   ((us & 0xFFFB) | (v << 2))
#define SET_BIT3_16(us, v)   ((us & 0xFFF7) | (v << 3))

#define SET_BIT4_16(us, v)   ((us & 0xFFEF) | (v << 4))
#define SET_BIT5_16(us, v)   ((us & 0xFFDF) | (v << 5))
#define SET_BIT6_16(us, v)   ((us & 0xFFBF) | (v << 6))
#define SET_BIT7_16(us, v)   ((us & 0xFF7F) | (v << 7))

#define SET_BIT8_16(us, v)   ((us & 0xFEFF) | (v << 8))
#define SET_BIT9_16(us, v)   ((us & 0xFDFF) | (v << 9))
#define SET_BIT10_16(us, v)  ((us & 0xFBFF) | (v << 10))
#define SET_BIT11_16(us, v)  ((us & 0xF7FF) | (v << 11))

#define SET_BIT12_16(us, v)  ((us & 0xEFFF) | (v << 12))
#define SET_BIT13_16(us, v)  ((us & 0xDFFF) | (v << 13))
#define SET_BIT14_16(us, v)  ((us & 0xBFFF) | (v << 14))
#define SET_BIT15_16(us, v)  ((us & 0x7FFF) | (v << 15))
#define CHAR_SIZE   9

/*######################################## 板子型号 ########################################*/
//#define HI3521D_LOOP_V1		//3521d 硬环出V1（绿色）
//#define HI3521D_LOOP_V2		//3521d 硬环出V2（蓝色）
//#define HI3521D_2HDMI			//3521D 双hdmi输入
//#define HI3521D_SDI			//3521D sdi环出
//#define HI3531D_4HDMI			//3531D 4hdmi老板子
//#define HI3531D_DOUBLEDECK_HDMI       //3531D 双层hdmi
//#define HI3531D_GUANGZHOU_VDEC        //3531D 广州解码300R
//#define HI3531D_4MIX_INPUT            //3531D 4混合输入，4hdmi新板子
//#define HI3531D_NJ_JIU_6PIN           //3531D 南京6PIN旧板子


#define HI3521D_LOOP_V1               "SHNJ23015d_100_V1"                       //3521d 硬环出V1（绿色）          8
#define HI3521D_LOOP_V2               "SHNJ23015d_100_V2"                       //3521d 硬环出V2（蓝色）          7
#define HI3521D_2HDMI                 "SHNJ23015d_200_V1"                          //3521D 双hdmi输入           6
#define HI3521D_SDI                   "SHNJ23015d_400_V1"                      //3521D sdi环出                  5
#define HI3531D_4HDMI                                "0"                        //3531D 4hdmi老板子             4
#define HI3531D_DOUBLEDECK_HDMI       "SHNJ2301fd_110_V1"                        //3531D 双层hdmi               3
#define HI3531D_GUANGZHOU_VDEC        "SHGZ2301fd_100_V1"                      //3531D 广州解码300R              2
#define HI3531D_4MIX_INPUT            "SHNJ2301fd_300_V1"                       //3531D 4混合输入，4hdmi新板子    0
#define HI3531D_4MIX_INPUT_MIX        "SHNJ2301fd_400_V1"                                              //      0
#define HI3531D_NJ_JIU_6PIN           "SHNJ2301fd_100_V2"                      //3531D 南京6PIN旧板子            1
#define HI3531D_GZ_JIU_6PIN           "SHGZ2301fd_300_V1"                       // guang zhou                  1




#define MAX_led      9
typedef enum slSL_OLEDSHOW 
{
    SL_O_NUM   =  0,
    SL_O_CHAR,
    SL_O_SYM,

    SL_O_STATE_BUFF,
} SL_OLEDSHOW;

#ifndef O_NUM
#define O_NUM   (SL_OLEDSHOW)0
#endif

#ifndef O_CHAR
#define O_CHAR  (SL_OLEDSHOW)1
#endif

#ifndef O_SYM
#define O_SYM   (SL_OLEDSHOW)2
#endif

#if 0




#endif




//int Max_char = ('~' - ' ' + 1);

extern const U16  oled_num[10][7];
extern const U16  oled_char[26][9];
extern U16 const small_oled_char['~' - ' ' + 1][5];



extern  const	UINT8	small_oled_char_big['~' - ' ' + 1][ 4 * 13 ];

extern  const   UINT8   oled_num1[12][ 8 * 27 ];
extern UINT8 oled_logo[128*64];




#if 1
class OLEDMaster
{
public:
    OLEDMaster();
    ~OLEDMaster();

    void OLED_lei();
    VOID   initSys( );

    void OLED_init_gpio(int oled_moudle);
    VOID  InitOLED();
    VOID	    ShowOLEDClear(U8 start, U8 end );
    VOID	WriteByteOLED( U8 data, U8 addr );
    VOID	IICStart( );
    VOID	 IICStop( );
    U8	  DelayACK( );
    VOID	   IICWriteByte( U8 data );
    VOID	ShowOLEDString( U8 x, U8 y, U8* str, U8 len );
    VOID     ShowOLEDError( U8 y );
    // VOID	    ShowOLEDClear(U8 start, U8 end );
    VOID    ShowOLEDSmallString( U8 x, U8 y, U8* str, U8 len );
    VOID     ShowOLEDChinese( U8 x, U8 y, U8 number );
    VOID	SetOLEDAddr( U8 x, U8 y );
    VOID  	 SetSmallOLEDAddr( U8 x, U8 y );
    void  oled_gpio_sml( );


public:

    bool    OLED_buf_my;
private:


     char   shiyan[10];
     int shiyan1;
    #if 1
       INT8     SDA_DIR[15];
       INT8     SCL_DIR[15];
       INT8     SCLMUX[15];
       INT8     SCLMUX_G[10];
       INT8     SDAMUX[15];
       INT8     SDAMUX_G[10];
       INT8     SDA [15];
       INT8     SCL [15];
       INT8     SDA_H [10];
       INT8     SDA_L [10];
       INT8     SCL_H [10];
       INT8     SCL_L [10];
       INT8     SCL_DIR_W[10];
       INT8     SDA_DIR_W[10];
       INT8     VCCMUX[15];
       INT8     GNDMUX[15];
       INT8     VCC [15];
       INT8     GND [15];
       INT8     VCC_H[10];
       INT8     VCC_L[10];
       INT8     GND_H[10];
       INT8     GND_L[10];

#endif


};

#endif





class  OLEDBaMaster{
public:
    OLEDBaMaster();
    ~OLEDBaMaster();
     void   oled_ba_init(int who_oled_big );
     void   initSys_big();
     int    oled_open_all(FILE * log_oled  );
     VOID 	ShowOLEDString_big( UINT8 x, UINT8 y, UINT8* str, UINT8 len );
     VOID	ShowOLEDFill_big( UINT8 x, UINT8 y, UINT16 w, UINT8 h, UINT8 data );
     VOID 	 ShowOLEDBigString( UINT8 x, UINT8 y, UINT8* str, UINT8 len );
     VOID   Show_Logo( );
     VOID	SetOLEDAddr_big( UINT8 x, UINT8 y );
     VOID 	SendByte( UINT8 data );
     VOID    WriteCmdOLED( UINT8 cmd );
     VOID    InitOLED_big();
     VOID	    ShowOLEDFillPic_big( UINT8 x, UINT8 y, UINT16 w, UINT8 h, UINT8* data );
     void    oled_gpio_ba();

public:

    bool    OLEDBa ;
private:
      INT8    CSMUX[15]; //GPIO MUX 3_5
      INT8    DCMUX[15]; //GPIO MUX 4_0
      INT8    RESMUX[15]; //GPIO MUX 4_1
      INT8    SDINMUX[15]; //GPIO MUX 4_2
      INT8    SCLKMUX[15]; //GPIO MUX 4_3

      INT8    CSMUX_G[10]; //GPIO 3_5
      INT8    DCMUX_G[10]; //GPIO 4_0
      INT8    RESMUX_G[10]; //GPIO 4_1
      INT8    SDINMUX_G[10]; //GPIO 4_2
      INT8    SCLKMUX_G[10]; //GPIO 4_3

      INT8     CS_DIR[15]; //GPIO3_DIR
      INT8     DC_DIR[15]; //GPIO4_DIR

      INT8    CS[15]; //GPIO_DATA 3_5
      INT8    DC[15]; //GPIO_DATA 4_0
      INT8    RES[15]; //GPIO_DATA 4_1
      INT8    SDIN[15]; //GPIO_DATA 4_2
     INT8    SCLK[15]; //GPIO_DATA 4_3

     INT8    CS_H[10];       //GPIO3_5: SET 1
     INT8    CS_L[10];       //GPIO3_5: SET 0
     INT8    DC_H[10];       //GPIO4_0: SET 1
     INT8    DC_L[10];       //GPIO4_0: SET 0
     INT8    RES_H[10];       //GPIO4_1: SET 1
     INT8    RES_L[10];       //GPIO4_1: SET 1
     INT8    SDIN_H[10];       //GPIO4_2: SET 1
     INT8    SDIN_L[10];       //GPIO4_2: SET 0
     INT8    SCLK_H[10];       //GPIO4_3: SET 1
     INT8    SCLK_L[10];       //GPIO4_3: SET 1

     INT8    CS_DIR_W[10];//GPIO_DIR 3_5 set out
     INT8    DC_DIR_W[10];//GPIO_DIR 4_0 4_1 4_2 4_3 set out


};








extern int g_sock;
extern U16	 oled_buf[160];
extern int  who_led;
extern U8	ShowOLEDIP( U8 x, U8 y, U32 ip );
extern U8   ShowOLEDMAC( U8 x, U8 y, U32 mac[] );
extern U8	ShowOLEDNum( U8 x, U8 y, U8 num );
extern U8	ShowOLEDChar( U8 x, U8 y, U8 num );

extern void oled_buf_init(int who_oled_big);

extern VOID	Delay( );

extern VOID InitOLED_big();
extern VOID	ShowOLED( U8 x );

extern VOID	FillOLEDBuffer( U8 x, U8 y, U8 num, SL_OLEDSHOW flag );






extern VOID	SendByte( UINT8 data );
extern VOID WriteCmdOLED( UINT8 cmd );
VOID Show_Logo( );
int oled_open_all(FILE * log_oled);
VOID	ShowOLEDFill( UINT8 x, UINT8 y, UINT16 w, UINT8 h, UINT8 data );
VOID	ShowOLEDString_big( UINT8 x, UINT8 y, UINT8* str, UINT8 len );
VOID 	ShowOLEDBigString( UINT8 x, UINT8 y, UINT8* str, UINT8 len );
VOID	SetOLEDAddr_big( UINT8 x, UINT8 y );



#endif
