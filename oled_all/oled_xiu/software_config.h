#ifndef Software_Config_H
#define Software_Config_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>

#include <readini.h>
#include "global.h"
#include "version.h"

using namespace std;
#define ETH0_NAME "eth0"
#define ETH1_NAME "eth1"
#define DNS_NAME  "/etc/resolv.conf"
#define DATA_FILE "/data"
#define NET_CONFIG_FILE "net.ini"
#define OLED_CONFIG_FILE "oled.ini"
#define NODE_NAME_FILE  "in_node_name"
extern char filepath_[36];
extern char netfilepath_[36];

#define PARA_NAME_LEN 56
#define EnumtoStr(val) #val //把枚举变量的名称. 返回类型为const char[]

class SoftwareConfig
{
public:
    typedef enum {

      //  [oled],
        Oled_Port_udp,
        Oled_Port,
       // Oled_a_small_start,
        Oled_a_kind_start,
      //  Oled_a_big_start ,
        Oled_a_small_i_start,
        Oled_a_small_n_start,
        Oled_a_small_i_buf,
        Oled_a_small_n_buf,
        Oled_a_cpu ,
        Oled_a_vi ,
        Oled_a_ai ,
        Oled_a_net,
        Oled_a_venc,
        Oled_a_start,
        Oled_a_sleep_tim,
        Oled_a_all_ip,
        OLed_a_gpio_j,
        Oled_model,
        Oledsml_SDA_DIR,
        OLed_sml_SCL_DIR,
        OLed_sml_SCLMUX,
        OLed_sml_SCLMUX_G,
        OLed_sml_SDAMUX,
        OLed_sml_SDAMUX_G,
        OLed_sml_SDA,
        OLed_sml_SCL,
        OLed_sml_SDA_H,
        OLed_sml_SDA_L,
        OLed_sml_SCL_H,
        OLed_sml_SCL_L,
        OLed_sml_SCL_DIR_W,
        OLed_sml_SDA_DIR_W,
        OLed_sml_VCCMUX,
        OLed_sml_GNDMUX,
        OLed_sml_VCC,
        OLed_sml_GND,
        OLed_sml_VCC_H,
        OLed_sml_VCC_L,
        OLed_sml_GND_H,
        OLed_sml_GND_L,
        OLed_ba_CS,
        OLed_ba_DC,
        OLed_ba_RES,
        OLed_ba_CS_mux_W,
        OLed_ba_DC_mux_W,
        OLed_ba_RES_mux_W,
        OLed_ba_CS_MUX,
        OLed_ba_DC_MUX,
        OLed_ba_RES_MUX,
        OLed_ba_CS_H,
        OLed_ba_DC_H,
        OLed_ba_RES_H,
        OLed_ba_CS_L,
        OLed_ba_DC_L,
        OLed_ba_RES_L,
        OLed_CS_DIR,
        OLed_DC_DIR,
        OLed_CS_DIR_W,
        OLed_DC_DIR_W,
        //Oled_a_small_china_start,
        kSoftWareConfigIDMax,

#if 0
#endif


    }SoftWareConfigID;

    SoftwareConfig();
    ~SoftwareConfig();

    bool ReadConfig();
    bool SaveConfig();
    bool SaveNetConfig();
    void PrintConfig();
    bool LoadConfigToGlobal(bool bPrintThem = false);
    bool SetConfig(const SoftWareConfigID kId, string value);
    bool SetConfig(const SoftWareConfigID kId, int value);
    string GetConfig(const SoftWareConfigID kId);
    void do_set_net(const char *kIp, const char *kMask, const char *kGateway, const char *kDns, const char* kEthName );
    void do_set_mac(const char* kMac, const char* kEthName );
    void SetNetwork();


private:
    vector<string> configvalue_;
    //const char *const filepath_ = "./hi3531d_venc.ini"; //const data,const pointer
    char paramsnamelist[kSoftWareConfigIDMax][PARA_NAME_LEN]; //存放配置文件的前缀名称
};

#endif // Software_Config_H
