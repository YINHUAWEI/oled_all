#ifndef COMMANDMAP_H
#define COMMANDMAP_H

#include <map>
#include <string>
#include <sstream>

#include "cJSON.h"  //cJSON库，标准C编写，函数多以“cJSON_”开头
#include "himpp_master.h"
#include "software_config.h"
#include "udpsocket/udpsocket.h"

using namespace std;

#define MEDIA_CONFIG_FILE "media.json"
#define	MAX_WINDOW_NUM	16
#define THREAD_UDP_PORT     8008
#define LOOP_IP "127.0.0.1"
#define CODE_SUCCEED    (0)
#define CODE_SYS_ERROR  (1000)
#define CODE_VI_ERROR   (2000)
#define CODE_VENC_ERROR (3000)
#define CODE_VO_ERROR   (4000)
#define CODE_VDEC_ERROR (5000)
#define CODE_VPSS_ERROR (6000)



#define CODE_SUCCEED    "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":0}"
#define CODE_SYS_ERR    "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":1%05d}"
#define CODE_VI_ERR     "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":2%05d}"
#define CODE_VENC_ERR   "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":3%05d}"
#define CODE_VO_ERR     "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":4%05d}"
#define CODE_VDEC_ERR   "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":5%05d}"
#define CODE_VPSS_ERR   "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":6%05d}"
#define CODE_SNAP_ERR   "{\"Type\":\"ACK\",\"Function\":\"%s\",\"Value\":7%05d}"

#define SET_SIGNALS_CMD "setSignalList"
#define SET_WINDOWS_CMD "setWindowList"
#define CLOSE_WINDOWS_CMD "closeWindowList"

#define TidCnt 32

// 定义一个函数指针，并且定义一个map类型
class CommandMap;
class JsonCreater;
//class MediaJsonSave;
typedef bool (CommandMap::*pFun)(cJSON* value);
typedef map<string, pFun> CmdHandlerMap;



class CommandMap
{
public:
    CommandMap();
    ~CommandMap();

    bool LoadInitialMediaConfig();
    void SaveCommand();
    string GetConfigPath();

    // 主函数
    bool ProcessCmd(string key, cJSON* value);

    //一级关键字(key)响应 - 对应主函数中的调用
    bool TypeHandler(cJSON *value);
    bool FuncHandler(cJSON *value);
    bool ValueHandler(cJSON* value);
    bool Oled_all(cJSON *value);

    //二级响应 - 响应"Function"关键字
    bool SetNetHandler(cJSON* value);
    bool SetVencHandler(cJSON* value);
    bool SetAudioHandler(cJSON* value);
    bool SetMulticastHandler(cJSON* value);
    bool SetBasicConfigHandler(cJSON* value);
    bool SetViCropHandler(cJSON* value);
    bool SetOsdHandler(cJSON* value);
    bool SetSnapHandler(cJSON* value);
    bool SetSnapMulticastHandler(cJSON *value);
    bool GetMsgHandler(cJSON* value);
    bool GetLogHandler(cJSON* value);
    bool DeleteConfigHandler(cJSON *value);
    bool SetAuMulticastHandler(cJSON *value);
    bool GetShellHandler(cJSON *value);
    bool RemoveFileHandler(cJSON *value);
    bool SetTickMasterHandler(cJSON *value);

private:
    bool SendFileMsg(cJSON* value, const char* filepath, const char *key);
    bool LoadEchoUdp();

public:
    CmdHandlerMap pfuncmdmap_;
    CmdHandlerMap mediafuncmdmap_;
    bool b_save_;

private:
    string cmdtype_;
    string cmdfunc_;
    UDPSocket *echoudp_ = NULL;
    pthread_t  snap_tid[4];

    bool LetMeTellYou(const char *msg);
    void ClearCmdTypeAndFunc();
    void ACK(const char *code, const char *msg);
    void ACK(const char *code, int errcode, const char *msg);
};


typedef bool (CommandMap::*PF)(string key, cJSON* value);

class JsonParse
{
public:
    JsonParse()
    {
    }
    JsonParse(const char *kp_ch)
    {
        json_root_ = cJSON_Parse(kp_ch);
    }
    ~JsonParse()
    {
        cJSON_Delete(json_root_);
    }

    // NOTE: Paired with ReleaseJson()
    void LoadJson(const char *kp_ch)
    {
        json_root_ = cJSON_Parse(kp_ch);
    }

    //NOTE: Paired with LoadJson()
    void ReleaseJson()
    {
        if(json_root_ != NULL){
            cJSON_Delete(json_root_);
            json_root_ = NULL;
        }
    }

    bool CheckJsonFormat()
    {
        if (json_root_ == NULL)
        {
            const char *error_ptr = cJSON_GetErrorPtr();
            if (error_ptr != NULL)
                COMMON_PRT("Error before: %s", error_ptr);

            return false;
        }
        else
            return true;
    }

/*
 * 功能： 置顶 SET_SIGNALS_CMD 模块
 * 输入： 无
 * 返回： bool
 * 日期： 2019.3.29
 * 作者： zh.sun
 */
    bool StickJson()
    {
        int i = 0;
        cJSON *element;
        cJSON_ArrayForEach(element, json_root_){
            if(element->type == cJSON_Object){
                cJSON *jRet = cJSON_GetObjectItemCaseSensitive(element, "Function");
                if(jRet && cJSON_IsString(jRet) &&
                   !strcmp(jRet->valuestring, SET_SIGNALS_CMD)){
                    if(i != 0){
                        COMMON_PRT("stick SignalList Module");
                        jRet = cJSON_DetachItemFromArray(json_root_, i);

                        // 把SET_SIGNALS_CMD模块置顶
                        cJSON_InsertItemInArray(json_root_, 0, jRet);

                        return true;
                    }
                    else
                        break;
                }
            }
            i++;
        }
        //log_d("root: %s", cJSON_Print(json_root_));

        return false;
    }

    void IterationParseStart()
    {
        ParsingJson(json_root_);
    }

private:
    void ParsingJson(cJSON *p_item)
    {
#if 1
        cJSON *element;
        cJSON_ArrayForEach(element, p_item){
            if(element->string != NULL){ // 不能给string类型赋NULL初值
                if ( !Singleton<CommandMap>::getInstance()->ProcessCmd(element->string, element) )
                    COMMON_PRT("First-level keyword - \"%s\" not found", element->string);
            }
            else
                ParsingJson(element);
        }
#else
        bool b_iskey = false;
        cJSON *element;
        cJSON_ArrayForEach(element, p_item){
            if(cJSON_Object == element->type ||
               cJSON_Array  == element->type){
                if(element->string != NULL){ // 不能给string类型赋NULL初值
                    log_v("key: %s - %s%d%s", element->string,
                          (cJSON_Object == element->type) ? "{" : "[",
                          cJSON_GetArraySize(element),
                          (cJSON_Object == element->type) ? "}" : "]");

                    b_iskey = Singleton<CommandMap>::getInstance()->ProcessCmd(element->string, element);
                }

                //如果已经获取到指令关键字，并且已经执行对应的处理函数成功，则不再对该关键字的内层进行递归
                if(!b_iskey)
                    ParsingJson(element); //如果对应键的值仍为cJSON_Object就递归调用ParsingJson
            }
            else{ //NOTE:用于识别"Function"，"Type"，"Params"等关键字
                log_v("key: %s\t-\tvalue: %s", element->string, cJSON_Print(element));
                if(element->string != NULL) // 不能给string类型赋NULL初值
                    Singleton<CommandMap>::getInstance()->ProcessCmd(element->string, element);
            }
        }
#endif
    }

private:
    cJSON *json_root_ = NULL;
};

#endif // COMMANDMAP_H
