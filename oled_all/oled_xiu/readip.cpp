#include "readip.h"
//#include "memmap.h"

ifreq ifs[16] = {0};

VOID print_hw_addr(INT32 fd, const char* if_name, char* macAddr) {  
    ifreq req = {0};
    strcpy(req.ifr_name, if_name);  
    if(-1 == ioctl(fd, SIOCGIFFLAGS, &req)) {  
        perror("Failed IOCTL SIOCGIFFLAGS.");  
        return;  
    }  
    if(req.ifr_flags & IFF_LOOPBACK) {  
        //cout << "Is LOOPBACK." << endl;  
        return;  
    }  
  
    if(-1 == ioctl(fd, SIOCGIFHWADDR, &req)) {  
        perror("Failed IOCTL SIOCGIFHWADDR.");  
        return;  
    }

    unsigned char* puc = (unsigned char*)req.ifr_hwaddr.sa_data;
    sprintf(macAddr, "%02x%02x%02x%02x%02x%02x", puc[0], puc[1], puc[2], puc[3], puc[4], puc[5]);
    //sprintf(macAddr, "%02x:%02x:%02x:%02x:%02x:%02x", puc[0], puc[1], puc[2], puc[3], puc[4], puc[5]);
}

INT32 peek_interfaces(INT32 fd) {  
    memset(ifs, 0, sizeof(ifs));
    ifconf conf = {sizeof(ifs)};  
    conf.ifc_req = ifs;  
    if(-1 == ioctl(fd, SIOCGIFCONF, &conf)) {  
        perror("Failed IOCTL SIOCGIFCONF.");  
        return -1;  
    }  
    if(conf.ifc_len >= sizeof(ifs)) {  
        perror("Buffer too small for IOCTL SIOCGIFCONF.");  
        return -1;  
    }  
      
    return (conf.ifc_len / sizeof(ifreq));
}
