#include "memmap.h"

TMMAP_Node_t * pTMMAPNode = NULL;

#define LOCALFUNC    static
#define EXTERNFUNC   extern

LOCALFUNC HI_RET atoul(IN CHAR *str,OUT U32 * pulValue);
LOCALFUNC HI_RET atoulx(IN CHAR *str,OUT U32 * pulValue);

/*****************************************************************************
 Prototype    : StrToNumber
 Description  : 10/16 进制字符串转换为无符号数字。
 Input  args  : IN CHAR *str 
                   10进制字符串, 不接受符号
                   16进制字符串, 不包括前缀0x. 如ABCDE
                            
 Output args  : U32* pulValue, 转换后的数字
 Return value : HI_RET  HI_SUCCESS 转换成功
                        HI_FAILURE 转换失败
 Calls        : isdigit
                
 Called  By   : 
                
 History        : 
 1.Date         : 2005年7月10日
   Author       : t41030
   Modification : Created function
*****************************************************************************/

HI_RET StrToNumber(IN CHAR *str , OUT U32 * pulValue)
{
    /*判断是否16进制的字符串*/
    if ( *str == '0' && (*(str+1) == 'x' || *(str+1) == 'X') )
    {
        if (*(str+2) == '\0')
        {
            return HI_FAILURE;
        }
        else
        {
            return atoulx(str+2,pulValue);
        }
    }
    else
    {
        return atoul(str,pulValue);
    }
}

/*****************************************************************************
 Prototype    : atoul
 Description  : 10进制字符串转换为无符号数字。
 Input  args  : IN CHAR *str 10进制字符串
                不接受符号
 Output args  : U32* pulValue, 转换后的数字
 Return value : HI_RET  HI_SUCCESS 转换成功
                        HI_FAILURE 转换失败
 Calls        : isdigit
                
 Called  By   : 
                
 History        : 
 1.Date         : 2005年7月10日
   Author       : t41030
   Modification : Created function
*****************************************************************************/
HI_RET atoul(IN CHAR *str,OUT U32 * pulValue)
{
    U32 ulResult=0;

    while (*str)
    {
        if (isdigit((int)*str))
        {
            /*最大支持到0xFFFFFFFF(4294967295), 
               X * 10 + (*str)-48 <= 4294967295
               所以， X = 429496729 */
            if ((ulResult<429496729) || ((ulResult==429496729) && (*str<'6')))
            {
                ulResult = ulResult*10 + (*str)-48;
            }
            else
            {
                *pulValue = ulResult;
                return HI_FAILURE;
            }
        }
        else
        {
            *pulValue=ulResult;
            return HI_FAILURE;
        }
        str++;
    }
    *pulValue=ulResult;
    return HI_SUCCESS;
}

/*****************************************************************************
 Prototype    : atoulx
 Description  : 16进制字符串转换为无符号数字。输入的16进制字符串不包括前缀0x
 Input  args  : IN CHAR *str 16进制字符串, 不包括前缀0x. 如ABCDE
 Output args  : U32* pulValue, 转换后的数字
 Return value : HI_RET  HI_SUCCESS 转换成功
                        HI_FAILURE 转换失败
 Calls        : toupper
                isdigit
                
 Called  By   : 
                
 History        : 
 1.Date         : 2005年7月10日
   Author       : t41030
   Modification : Created function
*****************************************************************************/
#define ASC2NUM(ch) (ch - '0')
#define HEXASC2NUM(ch) (ch - 'A' + 10)

HI_RET  atoulx(IN CHAR *str,OUT U32 * pulValue)
{
    U32   ulResult=0;
    UCHAR ch;

    while (*str)
    {
        ch=toupper(*str);
        if (isdigit(ch) || ((ch >= 'A') && (ch <= 'F' )))
        {
            if (ulResult < 0x10000000)
            {
                ulResult = (ulResult << 4) + ((ch<='9')?(ASC2NUM(ch)):(HEXASC2NUM(ch)));
            }
            else
            {
                *pulValue=ulResult;
                return HI_FAILURE;
            }
        }
        else
        {
            *pulValue=ulResult;
            return HI_FAILURE;
        }
        str++;
    }
    
    *pulValue=ulResult;
    return HI_SUCCESS;
}
#if 1
VOID * memmap(unsigned int phy_addr, unsigned int size)
{
    static int fd = -1;
    static const char dev[]="/dev/mem";
	
    unsigned int phy_addr_in_page;
	unsigned int page_diff;
	unsigned int size_in_page;

	TMMAP_Node_t * pTmp;
	TMMAP_Node_t * pNew;
	
	VOID *addr=NULL;

	if(size == 0)
	{
		printf("memmap():size can't be zero!\n");
		return NULL;
	}

	/* check if the physical memory space have been mmaped */
	pTmp = pTMMAPNode;
	while(pTmp != NULL)
	{
		if( (phy_addr >= pTmp->Start_P) && 
			( (phy_addr + size) <= (pTmp->Start_P + pTmp->length) ) )
		{
            pTmp->refcount++;   /* referrence count increase by 1  */
			return (VOID *)(pTmp->Start_V + phy_addr - pTmp->Start_P);
		}

		pTmp = pTmp->next;
	}

	/* not mmaped yet */
	if(fd < 0)
	{
		/* dev not opened yet, so open it */
		fd = open (dev, O_RDWR | O_SYNC);
		if (fd < 0)
		{
			printf("memmap():open %s error!\n", dev);
			return NULL;
		}
	}

	/* addr align in page_size(4K) */
	phy_addr_in_page = phy_addr & PAGE_SIZE_MASK;
	page_diff = phy_addr - phy_addr_in_page;

	/* size in page_size */
	size_in_page =((size + page_diff - 1) & PAGE_SIZE_MASK) + PAGE_SIZE;

	addr = mmap ((VOID *)0, size_in_page, PROT_READ|PROT_WRITE, MAP_SHARED, fd, phy_addr_in_page);
	if (addr == MAP_FAILED)
	{
		printf("memmap():mmap @ 0x%x error!\n", phy_addr_in_page);
		return NULL;
	}

	/* add this mmap to MMAP Node */
	pNew = (TMMAP_Node_t *)malloc(sizeof(TMMAP_Node_t));
    if(NULL == pNew)
    {
        printf("memmap():malloc new node failed!\n");
        return NULL;
    }
	pNew->Start_P = phy_addr_in_page;
	pNew->Start_V = (unsigned int)addr;
	pNew->length = size_in_page;
    pNew->refcount = 1;
	pNew->next = NULL;
	
	if(pTMMAPNode == NULL)
	{
		pTMMAPNode = pNew;
	}
	else
	{
		pTmp = pTMMAPNode;
		while(pTmp->next != NULL)
		{
			pTmp = pTmp->next;
		}

		pTmp->next = pNew;
	}

	return (VOID *)((unsigned int)addr+page_diff);
}

VOID* himm(char* addr, char* bit)
{
    U32 ulOld, ulNew;
    U32 ulAddr = 0;
    VOID *pMem = NULL;

    if(bit == NULL){
	if( StrToNumber(addr, &ulAddr) == HI_SUCCESS){
	    pMem = memmap(ulAddr, DEFAULT_MD_LEN);
	    //printf("%s: 0x%08lX\n", addr, *(U32*)pMem);
	    return pMem;
	}
	else{
	    printf("ERROR:Please input addr like 0x12345678\n");
	}
    }
    else if( StrToNumber(addr, &ulAddr) == HI_SUCCESS &&
             StrToNumber(bit, &ulNew) == HI_SUCCESS){
	pMem = memmap(ulAddr, DEFAULT_MD_LEN);
	ulOld = *(U32*)pMem;
	*(U32*)pMem = ulNew;
	//printf("%s: 0x%08lX --> 0x%08lX \n", addr, ulOld, ulNew);
	return pMem;
    }
    else{
	printf("ERROR:xxx\n");
    }

    return NULL;
}
#endif

unsigned int set_reg(const unsigned int addr, const unsigned int value)
{

    void *pMem = NULL;

    pMem = memmap_reg(addr, DEFAULT_MD_LEN);
    *(unsigned int*)pMem = value;

    return *(unsigned int*)pMem;
}

unsigned int get_reg(const unsigned int addr)
{
    return *(unsigned int*)memmap_reg(addr, DEFAULT_MD_LEN);
}

void *memmap_reg(unsigned int phy_addr, unsigned int size)
{
    static int fd = -1;
    static const char dev[]="/dev/mem";

    unsigned int phy_addr_in_page;
    unsigned int page_diff;
    unsigned int size_in_page;

    TMMAP_Node_t * pTmp;
    TMMAP_Node_t * pNew;

    void *addr=NULL;

    if(size == 0)
    {
        printf("memmap():size can't be zero!\n");
        return NULL;
    }

    /* check if the physical memory space have been mmaped */
    pTmp = pTMMAPNode;
    while(pTmp != NULL)
    {
        if( (phy_addr >= pTmp->Start_P) &&
            ( (phy_addr + size) <= (pTmp->Start_P + pTmp->length) ) )
        {
            pTmp->refcount++;   /* referrence count increase by 1  */
            return (void *)(pTmp->Start_V + phy_addr - pTmp->Start_P);
        }

        pTmp = pTmp->next;
    }

    /* not mmaped yet */
    if(fd < 0)
    {
        /* dev not opened yet, so open it */
        fd = open (dev, O_RDWR | O_SYNC);
        if (fd < 0)
        {
            printf("memmap():open %s error!\n", dev);
            return NULL;
        }
    }

    /* addr align in page_size(4K) */
    phy_addr_in_page = phy_addr & PAGE_SIZE_MASK;
    page_diff = phy_addr - phy_addr_in_page;

    /* size in page_size */
    size_in_page =((size + page_diff - 1) & PAGE_SIZE_MASK) + PAGE_SIZE;

    addr = mmap ((void *)0, size_in_page, PROT_READ|PROT_WRITE, MAP_SHARED, fd, phy_addr_in_page);
    if (addr == MAP_FAILED)
    {
        printf("memmap():mmap @ 0x%x error!\n", phy_addr_in_page);
        return NULL;
    }

    /* add this mmap to MMAP Node */
    pNew = (TMMAP_Node_t *)malloc(sizeof(TMMAP_Node_t));
    if(NULL == pNew)
    {
        printf("memmap():malloc new node failed!\n");
        return NULL;
    }
    pNew->Start_P = phy_addr_in_page;
    pNew->Start_V = (unsigned int)addr;
    pNew->length = size_in_page;
    pNew->refcount = 1;
    pNew->next = NULL;

    if(pTMMAPNode == NULL)
    {
        pTMMAPNode = pNew;
    }
    else
    {
        pTmp = pTMMAPNode;
        while(pTmp->next != NULL)
        {
            pTmp = pTmp->next;
        }

        pTmp->next = pNew;
    }

    return (void *)((unsigned int)addr+page_diff);
}




