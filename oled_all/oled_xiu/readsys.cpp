#include "readsys.h"

int g_vi_cnt = 0; //用于判断有无输入信号

//string 转 int
int stoi(string str)  
{  
    int result; 
    istringstream is(str);
    is >> result;
    return result;
}

//string 转float 
float stof(string str)  
{  
    float result;
    istringstream is(str);
    is >> result;
    return result;
}

//int 转 string
string itos(int i)  
{  
    ostringstream os;  
    os<<i;  
    string result;  
    istringstream is(os.str());  
    is>>result;  
    return result;  
  
}

//float 转 string
string ftos(float f)  
{  
    ostringstream os;
    os<<f;
    string result;
    istringstream is(os.str()); 
    is>>result; 
    return result;
} 
//*****************

string CheckTimeFile_big()
{
    FILE *fp = NULL;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出

    string time = "time";
    string result;

    if(NULL == (fp = fopen("/tmp/config.txt","r"))){
    printf("Open config.txt failed!\n");
    return result;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);

    pos = 0;
    if((pos = data.find(time)) != string::npos){
        data = data.c_str() + pos + time.length() + 1;
        time  = data.substr(0, data.find(" ")-1);
        result.append("TIME:");
        result += time;
        break;
    }

    }
    //result.append("TIME:");
    //result += time;

    fclose(fp);
    //printf("TIME---%s\n", result.c_str());
    return result;
}

string CheckCpuFile_big()
{
    FILE *fp = NULL;
    int cnt = 0;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出
    string cpu = "cpu";
    string mem = "memory";
    string result;

    if(NULL == (fp = fopen("/tmp/config.txt","r"))){
    printf("Open config.txt failed!\n");
    return result;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);

    pos = 0;
    if((pos = data.find(cpu)) != string::npos){
        data = data.c_str() + pos + cpu.length() + 1;
        cpu  = data.substr(0, data.find(" ")-1);

    }else if((pos = data.find(mem)) != string::npos){
        data = data.c_str() + pos + mem.length() + 1;
        mem  = data.substr(0, data.find(" ")-1);
        break;
    }
    }

    if(cpu.length() > 3){
    result.append("CPU:");
    result += cpu;
    }
    if(mem.length() > 6){
    result.append("MEM:");
    result += mem;
    }
    fclose(fp);
    //printf("CPU---%s\n", result.c_str());

    return result;
}

string CheckNetFile_big()
{
    FILE *fp = NULL;
    int cnt = 0;
    int pos = 0;
    int recvbytesusing = 0;
    int sendbytesusing = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出
    string recvbytes;
    string sendbytes;
    string ethkey = "eth0:";
    string result;

    for (int t = 0; t < 2; t++){
    if(NULL == (fp = popen("cat /proc/net/dev","r"))){
        printf("Open %s failed!\n", PROC_NET_PATH);
        return ethkey;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
        string data(buff);

        pos = 0;
        cnt = 0;
        if((pos = data.find(ethkey)) != string::npos){
        for (int i = 0; i < strlen(buff); i++){
            if(buff[i] != ' '){
            cnt++;

            //查找NET RECV使用率
            if(cnt == 2){ //取第2个数字
                recvbytes = data.substr(i, data.find(" ", i) - i);
                i += recvbytes.length() - 1;
            }
            //查找NET SEND使用率
            else if(cnt == 10){ //取第10个数字
                sendbytes = data.substr(i, data.find(" ", i) - i);
                break;
            }
            else{
                sendbytes = data.substr(i, data.find(" ", i) - i);
                i += sendbytes.length() - 1;
            }
            }
        }
        break;
        }
    }

    if (t == 0){
        recvbytesusing = atoi(recvbytes.c_str());
        sendbytesusing = atoi(sendbytes.c_str());
        printf("recvbytes %d, recvbytesusing %d\n", recvbytes.c_str(), recvbytesusing);
        printf("sendbytes %d, sendbytesusing %d\n", sendbytes.c_str(), sendbytesusing);
    }
    pclose(fp);
    //printf("recvbytes %d, recvbytesusing %d\n", recvbytes.c_str(), recvbytesusing);
    //printf("sendbytes %d, sendbytesusing %d\n", sendbytes.c_str(), sendbytesusing);

    usleep(1000000);
    }

    float netuserate = (((stoi(recvbytes) - recvbytesusing) + (stoi(sendbytes) - sendbytesusing) * 1.0) / ( 1000 * 1000)) / 1000 * 100; //带宽利用率=流率/带宽,这里流率包括输入流率和输出流率之和
    printf("netuserate: %f\n", netuserate);

    result.append("NET:");
    result += ftos(netuserate);
    result.append("%");

    //printf("%s\n", result.c_str());

    return result;
}

string CheckVdecFile_big()
{
    FILE *fp = NULL;
    int cnt = 0;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出
    int    vdec_chn = 0;
    string vdec_w;
    string vdec_h;
    string vdec_type;
    string str_tmp;
    string vdecwhkey = "CHN ATTR & PARAMS";
    string result;

    if(NULL == (fp = popen("cat /proc/umap/vdec","r"))){
    printf("Open %s failed!\n", PROC_VDEC_PATH);
    return vdecwhkey;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);
    pos = 0;
    cnt = 0;

    if((pos = data.find(vdecwhkey)) != string::npos){ // 查询最大解码分辨率、解码类型、最大解码通道数
        fgets(buff, sizeof(buff), fp);
        fgets(buff, sizeof(buff), fp); // 连读两行
        data.assign(buff); //重新赋值

        for (int i = 0; i < strlen(buff); i++){
        if(buff[i] != ' '){
            cnt++;

            if(cnt == 2){ //取第2个数字
            vdec_type = data.substr(i, data.find(" ", i) - i);
            i += vdec_type.length() - 1;
            vdec_chn++;
            }
            else if(cnt == 4){ //取第4个数字
            vdec_w = data.substr(i, data.find(" ", i) - i);
            i += vdec_w.length() - 1;
            }
            else if(cnt == 5){ //取第5个数字
            vdec_h = data.substr(i, data.find(" ", i) - i);
            break;
            }
            else{
            str_tmp = data.substr(i, data.find(" ", i) - i);
            i += str_tmp.length() - 1;
            }
        }
        }

        // 计数一共有多少个H264/H265通道个数
        while (fgets(buff, sizeof(buff), fp) != NULL) {
        data.assign(buff); //重新赋值
        cnt = 0;

        for (int i = 0; i < strlen(buff); i++){
            if(buff[i] != ' '){
            cnt++;

            if(cnt == 2){ //取第2个数字
                str_tmp = data.substr(i, data.find(" ", i) - i);
                if(vdec_type == str_tmp){
                vdec_chn++;
                }
                break;
            }
            else{
                str_tmp = data.substr(i, data.find(" ", i) - i);
                i += str_tmp.length() - 1;
            }
            }
        }
        if(str_tmp == "VfmwID"){
            break;
        }
        }

        break;
    }
    }

    result.append("VDEC:");
    result += vdec_w;
    result.append("x");
    result += vdec_h;
    result.append("_");
    result += vdec_type;
    result.append("x");
    result += itos(vdec_chn);

    pclose(fp);
    //printf("%s\n", result.c_str());

    return result;
}

string CheckVoFile_big()
{
    FILE *fp = NULL;
    int cnt = 0;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出
    string vo_type;
    string vo_inf;
    string vo_lchs;
    string str_tmp;
    string vodevkey     = "DEV CONFIG";
    string vostatuskey  = "VIDEO LAYER STATUS 2";
    string result;

    if(NULL == (fp = popen("cat /proc/umap/vo","r"))){
    printf("Open %s failed!\n", PROC_VO_PATH);
    return vodevkey;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);
    pos = 0;
    cnt = 0;

    if((pos = data.find(vodevkey)) != string::npos){ //查询HDMI输出制式
        fgets(buff, sizeof(buff), fp);
        fgets(buff, sizeof(buff), fp); // 连读两行
        data.assign(buff); //重新赋值

        for (int i = 0; i < strlen(buff); i++){
        if(buff[i] != ' '){
            cnt++;

            if(cnt == 3){ //取第3个数字
            vo_type = data.substr(i, data.find(" ", i) - i);
            i += vo_type.length() - 1;
            }
            else if(cnt == 4){ //取第4个数字
            vo_inf = data.substr(i, data.find(" ", i) - i);
            break;
            }
            else{
            str_tmp = data.substr(i, data.find(" ", i) - i);
            i += str_tmp.length() - 1;
            }
        }
        }
    }
    else if((pos = data.find(vostatuskey)) != string::npos){ // 查询白平衡值
        fgets(buff, sizeof(buff), fp);
        fgets(buff, sizeof(buff), fp); // 连读两行
        data.assign(buff); //重新赋值

        for (int i = 0; i < strlen(buff); i++){
        if(buff[i] != ' '){
            cnt++;

            if(cnt == 5){ //取第5个数字
            str_tmp = data.substr(i, data.find(" ", i) - i);
            i += str_tmp.length() - 1;
            vo_lchs.append("LU:");
            vo_lchs += str_tmp;
            }
            else if(cnt == 6){ //取第6个数字
            str_tmp = data.substr(i, data.find(" ", i) - i);
            i += str_tmp.length() - 1;
            vo_lchs.append("CO:");
            vo_lchs += str_tmp;
            }
            else if(cnt == 7){ //取第7个数字
            str_tmp = data.substr(i, data.find(" ", i) - i);
            i += str_tmp.length() - 1;
            vo_lchs.append("HU:");
            vo_lchs += str_tmp;
            }
            else if(cnt == 8){ //取第8个数字
            str_tmp = data.substr(i, data.find("\n", i) - i);
            vo_lchs.append("SA:");
            vo_lchs += str_tmp;
            break;
            }
            else{
            str_tmp = data.substr(i, data.find(" ", i) - i);
            i += str_tmp.length() - 1;
            }
        }
        }

        break;
    }
    }

    result.append("VO:");
    result += vo_type;
    result.append("_");
    result += vo_inf;
    result.append(";");
    result += vo_lchs;

    pclose(fp);
    //printf("%s\n", result.c_str());

    return result;
}

string CheckMonitorFile_big()
{
    FILE *fp = NULL;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出
    string ao_samplerate;
    string ao_outmode;
    string multicast_type;
    string rtp_len;
    string multicast_size;
    string multicast_pack;
    string multicast_wait;
    string license;
    string authorization;
    string aosamplekey = "audio_sample_rate";
    string aooutmodekey = "audio_out_mode";
    string multicasttypekey = "multicast_type";
    string rtplenkey = "rtp_len";
    string multicastsizekey = "multicast_size";
    string multicastpackkey = "multicast_pack";
    string multicastwaitkey = "multicast_wait";
    string licensekey = "sys_license";
    string authorizationkey = "authorization";
    string result;

    if(NULL == (fp = popen("cat /home/monitor/monitor.ini","r"))){
    printf("Open %s failed!\n", PROC_AO_PATH);
    return aosamplekey;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);
    pos = 0;

    if((pos = data.find(aosamplekey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        ao_samplerate = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(aooutmodekey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        ao_outmode = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(multicasttypekey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        multicast_type = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(rtplenkey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        rtp_len = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(multicastsizekey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        multicast_size = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(multicastpackkey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        multicast_pack = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(multicastwaitkey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        multicast_wait = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(licensekey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        license  = data.substr(pos + 1, data.length() - pos - 2);
    }
    else if((pos = data.find(authorizationkey)) != string::npos){ //
        if((pos = data.find("=")) != string::npos)
        authorization = data.substr(pos + 1, data.length() - pos - 2);
    }

    }

    multicast_wait = itos(stoi(multicast_wait)/1000);
    multicast_wait.append("K");

    result.append("AO:");
    result += ao_outmode;
    result.append("_");
    result += ao_samplerate;
    result.append("Hz;");
    result += multicast_type;
    if(multicast_type == "RTP"){
    result.append(":");
    result += rtp_len;
    result.append("_");
    result += multicast_wait;
    }
    else if(multicast_type == "UDP"){
    result.append(":");
    result += multicast_size;
    result.append("x");
    result += multicast_pack;
    result.append("_");
    result += multicast_wait;
    }
    result.append(";LIC:");
    if(!license.length())
    result.append("N/A");
    else
    result += license.substr(0, 17);
    if(authorization != "0"){
    result.append(";Author:");
    result += authorization;
    }

    pclose(fp);
    //printf("%s\n", result.c_str());

    return result;
}

string CheckAiFile_big()
{
    FILE *fp = NULL;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出

    string ai_rate = "kAiSampleRate";
    string result;

    if(NULL == (fp = fopen("/tmp/config.txt","r"))){
    printf("Open config.txt failed!\n");
    return result;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);

    pos = 0;
    if((pos = data.find(ai_rate)) != string::npos){
        data = data.c_str() + pos + ai_rate.length() + 1;
        ai_rate  = data.substr(0, data.find(" ")-1);
        result.append("AI_SRATE:");
        result += ai_rate;
        break;

    }
    }
    //result.append("AI_SRATE:");
    //result += ai_rate;

    fclose(fp);
    //printf("AI---%s\n", result.c_str());
    return result;
}

string CheckVencFile_big()
{
    FILE *fp = NULL;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出

    string venc_type = "kChn0_M_VENC_Type";
    string result;

    if(NULL == (fp = fopen("/tmp/config.txt","r"))){
    printf("Open config.txt failed!\n");
    return result;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);

    pos = 0;
    if((pos = data.find(venc_type)) != string::npos){
        data = data.c_str() + pos + venc_type.length() + 1;
        venc_type  = data.substr(0, data.find(" ")-1);
        result.append("VENC_TYPE:");
        if(venc_type == "96\n")
        result.append("H264");
        else if(venc_type == "265\n")
        result.append("H265");
        else if(venc_type == "1002\n")
        result.append("MJPEG");
        break;

    }
    }

    /*result.append("VENC_TYPE:");
    if(venc_type == "96\n")
        result.append("H264");
    else if(venc_type == "265\n")
        result.append("H265");
    else if(venc_type == "1002\n")
        result.append("MJPEG");*/

    fclose(fp);
    //printf("VENC---%s\n", result.c_str());
    return result;
}

string CheckEthspeedFile_big()
{
    FILE *fp = NULL;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出

    string eth_speed = "eth_speed";
    string result;

    if(NULL == (fp = fopen("/tmp/config.txt","r"))){
    printf("Open config.txt failed!\n");
    return result;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
    string data(buff);

    pos = 0;
    if((pos = data.find(eth_speed)) != string::npos){
        data = data.c_str() + pos + eth_speed.length() + 1;
        eth_speed  = data.substr(0, data.find(" ")-1);
        result.append("ETH_SPEED:");
        result += eth_speed;
        break;

    }
    }
    //result.append("ETH_SPEED:");
    //result += eth_speed;

    fclose(fp);
    //printf("ETH---%s\n", result.c_str());
    return result;
}

string CheckViFile_big()
{
    FILE *fp = NULL;
    int vi_cnt = 0;
    int cnt = 0;
    int pos = 0;
    char buff[1024]  = ""; //设置一个合适的长度，以存储每一行输出
    string vi_w;
    string vi_h;
    string vi_frmrate;
    string vicapwhkey   = "VI PHYCHN ATTR";
    string vifrmratekey = "VI CHN STATUS";
    string vicntkey     = "VI PHYCHN STATUS 1";
    string result;

    if(NULL == (fp = popen("cat /proc/umap/vi","r"))){
        printf("Open %s failed!\n", PROC_VI_PATH);
        return vicntkey;
    }

    while (fgets(buff, sizeof(buff), fp) != NULL) {
        string data(buff);
        pos = 0;
        cnt = 0;

        if((pos = data.find(vicapwhkey)) != string::npos){ //查找输入分辨率。注意，当无输入信号时，输入分辨率默认为1920*1080@30
            fgets(buff, sizeof(buff), fp);
            fgets(buff, sizeof(buff), fp); // 连读两行
            data.assign(buff); //重新赋值

            for (int i = 0; i < strlen(buff); i++){
                if(buff[i] != ' '){
                    cnt++;

                    if(cnt == 4){ //取第4个数字
                        vi_w = data.substr(i, data.find(" ", i) - i);
                        i += vi_w.length() - 1;
                    }
                    else if(cnt == 5){ //取第4个数字
                        vi_h = data.substr(i, data.find(" ", i) - i);
                        break;
                    }
                    else{
                        vi_h = data.substr(i, data.find(" ", i) - i);
                        i += vi_h.length() - 1;
                    }
                }
            }

            //printf("w: %s, h: %s\n", vi_w.c_str(), vi_h.c_str());
            continue;
        }
        else if((pos = data.find(vicntkey)) != string::npos){ // 判断有无输入信号
            fgets(buff, sizeof(buff), fp);
            fgets(buff, sizeof(buff), fp); // 连读两行

            for (int i = 0; i < strlen(buff); i++){
                if(buff[i] != ' '){
                    cnt++;

                    if(cnt == 4){ //取第4个数字
                        vi_cnt = atoi(buff + i - 1);
                        break;
                    }
                    else{
                        vi_cnt = atoi(buff + i - 1);
                        if (vi_cnt >= 10 && vi_cnt <= 99)
                            i += 1;
                        else if((vi_cnt >= 100 && vi_cnt <= 999))
                            i += 2;
                        else if((vi_cnt >= 1000 && vi_cnt <= 9999))
                            i += 3;
                    }
                }
            }
            if(vi_cnt == g_vi_cnt){
                pclose(fp);
                result = "VI:No Video Input";
                printf("%s. IntCnt: %d\n", result.c_str(), g_vi_cnt);
                return result;
            }
            else{
                g_vi_cnt = vi_cnt;
                result.append("VI:");
                result += vi_w;
                result.append("x");
                result += vi_h;
                //printf("IntCnt: %d\n", g_vi_cnt);
            }

            continue;
        }
        if((pos = data.find(vifrmratekey)) != string::npos){ //输入帧率。注意当无信号输入时，帧率默认为30
            fgets(buff, sizeof(buff), fp);
            fgets(buff, sizeof(buff), fp); // 连读两行
            data.assign(buff); //重新赋值

            for (int i = 0; i < strlen(buff); i++){
                if(buff[i] != ' '){
                    cnt++;

                    if(cnt == 4){ //取第4个数字
                        vi_frmrate = data.substr(i, data.find(" ", i) - i);
                        i += vi_frmrate.length() - 1;

                        break;
                    }
                    else{
                        vi_frmrate = data.substr(i, data.find(" ", i) - i);
                        i += vi_frmrate.length() - 1;
                    }
                }
            }
            result.append("_");
            result += vi_frmrate;
            break;
        }
    }

    pclose(fp);
    printf("%s\n", result.c_str());

    return result;
}





//******************************







