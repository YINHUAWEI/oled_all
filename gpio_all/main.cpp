#include <stdio.h>
#include <hi_reg/hi_reg.h>
#include <software_config.h>
//#include <GPIO3531d.h>
//#include <GPIO3521d.h>
#include <hi_reg/hi_reg.h>
#include <common.h>
#include <gpio.h>


#define MAXDATA   1024
#define DEMAXDATALAY_TIME      10
#define OLED_CHAR_NUM   22
#define BIG_IP          1
#define HDPATH      "/version/hardware_info"

#define N 100


//int gpio_master = 0;
//int gpio_child = 0;
int who_device = -1;
void gpio_set_MUX();
int htoi_int(char a[]);

int stoi(string str)
{
    int result;
    istringstream is(str);
    is >> result;
    return result;
}



bool readHdVersion(char *hd_version)
{
    FILE *hdFp = NULL;
    int rdLen = 0;
    char buff[256] = {0};

    char *st = NULL, *end = NULL;

    if(NULL == (hdFp = fopen(HDPATH,"r"))) {
        printf("open   hardware_info error\n");
        return false;
    }
    memset(buff,0,256);
    memset(hd_version,0,32);

    rdLen = fread(buff,1,256,hdFp);
    if(rdLen > 0) {
       // printf("read hardware_info:%s\n",buff);
        st = strstr(buff,"\"board\": \"");
        st += strlen("\"board\": \"");
        end = strchr(st,'\"');
        strncpy(hd_version,st,end-st);
        //printf("get hdVersion:%s\n",hd_version);
    }
    else{
        fclose(hdFp);
        return false;
    }
    fclose(hdFp);
    return true;
}


//void gpio_set_MUX()
//{
//    int MUX_reg = g_muxctrl_reg[10 * 8][0];
//    gpio_master = 10;
//    gpio_child = 6;
//   // set_reg(g_muxctrl_reg[gpio_master *8 + gpio_child][0],g_muxctrl_reg[gpio_master * 8 +gpio_child][1]);
//   // set_reg()



//}

//long StrToNumber( char *str , long* pulValue)
//{
//    /*判断是否16进制的字符串*/
//    if ( *str == '0' && (*(str+1) == 'x' || *(str+1) == 'X') )
//    {
//        if (*(str+2) == '\0')
//        {
//            return -1;
//        }
//        else
//        {
//            return atoulx(str+2,pulValue);
//        }
//    }
//    else
//    {
//        return atoul(str,pulValue);
//    }
//}

int char_A_F(char ch)
{
        if(isdigit(ch))
                return ch - 48;
        if( ch < 'A' || (ch > 'F' && ch < 'a') || ch > 'z' )
                return -1;
        if(isalpha(ch))
                return isupper(ch) ? ch - 55 : ch - 87;

        return -1;
}

int char_zhuan_int(char *hex)
{
        int len;
        int num = 0;
        int temp;
        int bits;
        int i;


        len = strlen(hex);

        for (i=0, temp=0; i<len; i++, temp=0)
        {
                temp = char_A_F( *(hex + i) );
                bits = (len - i - 1) * 4;
                temp = temp << bits;


                num = num | temp;
        }

        return num;
}




#if 1
int htoi_int(char a[]){
    int i;
    int len,t,sum=0;
    len=strlen(a);
    for(i=0;i<len;i++){
        if(a[i]>='0'&&a[i]<='9'||a[i]>='a'&&a[i]<='f'||a[i]>='A'&&a[i]<='F'){
            if(a[i]>='0'&&a[i]<='9')
                t=a[i]-'0';
            else if(a[i]>='a'&&a[i]<='f')
                t=a[i]-'a'+10;//
            else
                t=a[i]-'A'+10;
            sum=sum*16+t;
        }
        else{
            printf("aa\n");
            break;
        }
    }
    return sum;

}

#endif



int main(int argc, char * argv[])
{
       // set_reg();
  //  auto thisini = Singleton<SoftwareConfig>::getInstance();
    auto  gpiomaster = Singleton<GPIOMaster>::getInstance();

    char version[32] ={0};
    memset(version,0,sizeof(version));
    readHdVersion(version);
    int reg_value = 0;
    // int who_device = -1;

   // thisini->ReadConfig();

    if(argc == 1)
    {
    printf("******************************Parameters that* readME ********************\n");

  //  printf("example: ./gpio 4_0         ,get reg data is addr\n");
    printf("example: ./gpio 4_0 in      ,hight \n");
    printf("example: ./gpio 4_0 out     ,low\n");
    printf("example: ./gpio 4_0 12      ,the '12' value that you put in, You can write what you want \n");
    printf("example: ./gpio 4_0 on      ,direction controller  input\n");
    printf("example: ./gpio 4_0 off     ,direction controller  output\n");
    printf("example: ./gpio 4_0 m  1    , 'm' is behalf of reuse(MUX) 复用 '1'  Set the value of the\n");
    printf("example: ./gpio 4_0 m       , 'm' is behalf of reuse(MUX)  \n");
    printf("example: ./gpio 4_0 get     ,Returns the value of the set address\n");
    printf("example: ./gpio 4_0 get on  , direction the value of the address\n");
    printf("example: ./gpio 4_0 get m   , (MUX)Returns the value of the set address\n");
    printf("**************************************************************************\n");
    }

    if(!strncmp(version,HI3531D_4MIX_INPUT,strlen(HI3531D_4MIX_INPUT))) {
        printf("this chip is 4hdmi input\n");
        who_device = 0;
    }
    else if(!strncmp(version,HI3531D_4MIX_INPUT_MIX,strlen(HI3531D_4MIX_INPUT_MIX))) {
        printf("this chip is 4mix input\n");
        who_device = 0;
    }
    else if(!strncmp(version,HI3531D_NJ_JIU_6PIN,strlen(HI3531D_NJ_JIU_6PIN))) {
        printf("this chip is  input  HI3531D_NJ_JIU_6PIN\n");
        who_device = 1;
    }
    else if(!strncmp(version,HI3531D_GZ_JIU_6PIN,strlen(HI3531D_GZ_JIU_6PIN))) {
        printf("this chip is 4mix GAUNGZHOU\n");
        who_device = 1;
    }
    else if(!strncmp(version,HI3531D_NJ_JIU_7PIN_3,strlen(HI3531D_NJ_JIU_7PIN_3))) {
        printf("this chip is 4mix GAUNGZHOU\n");
        who_device = 1;
    }
    else if(!strncmp(version,HI3531D_NJ_JIU_6PIN_1,strlen(HI3531D_NJ_JIU_6PIN_1))) {
        printf("this chip is 4mix GAUNGZHOU\n");
        who_device = 1;
    }
    else if(!strncmp(version,HI3531D_GUANGZHOU_VDEC,strlen(HI3531D_GUANGZHOU_VDEC))) {
        printf("this chip is 4mix HI3531D_GUANGZHOU_VDEC\n");
        who_device = 2;
    }
    else if(!strncmp(version,HI3531D_DOUBLEDECK_HDMI,strlen(HI3531D_DOUBLEDECK_HDMI))) {
        printf("this chip is  input  HI3531D_DOUBLEDECK_HDMI\n");
        who_device = 3;
    }
    else if(!strncmp(version,HI3531D_4HDMI,strlen(HI3531D_4HDMI))) {
        printf("this chip is 4mix HI3531D_4HDMI\n");
        who_device = 4;
    }
    else if(!strncmp(version,HI3521D_SDI,strlen(HI3521D_SDI))) {
        printf("this chip is  input  HI3521D_SDI\n");
        who_device = 5;
    }
    else if(!strncmp(version,HI3521D_2HDMI,strlen(HI3521D_2HDMI))) {
        printf("this chip is  SHNJ23015d_200_V1   \n");
        who_device = 6;
    }
    else if(!strncmp(version,HI3521D_LOOP_V2,strlen(HI3521D_LOOP_V2))) {
        printf("this chip is  input  SHNJ23015d_100_V2\n");
        who_device = 7;
    }
    else if(!strncmp(version,HI3521D_LOOP_V1,strlen(HI3521D_LOOP_V1))) {
        printf("this chip is  input  SHNJ23015d_100_V1 \n");
        who_device = 8;
    }
    else if(!strncmp(version,HI3520D_7000_v1,strlen(HI3520D_7000_v1))) {
        printf("this chip is  input  HI3520D_7000_v1\n");
        who_device = 9;
    }
    else if(!strncmp(version,HI3520D_7000_v1_D,strlen(HI3520D_7000_v1_D))) {
        printf("this chip is  input  HI3520D_7000_v1_D \n");
        who_device = 9;
    }
    else
    {
        who_device = -1;
        printf(" error have`nt led  is /version/hardware_info  \n");
        return 0;
    }




    char gpio_data_a[12];

    int gpio_mastr,gpio_chile;
     int len_gpio;
   //  printf("argc %d\n",argc);
    if(argc > 1)
   {

    sprintf(gpio_data_a,"%s",argv[1]);


 if(gpio_data_a[1] != '_')
 {

    // printf("--=%c\n",gpio_data_a[0]);
  //   if(gpio_data_a[2] != '_')

     if(gpio_data_a[3] < '0' || gpio_data_a[3] > '7')
     {
         printf("输入的值是否正确 oh!  NO \n");
         return -1;
     }
     gpio_chile = gpio_data_a[3] - 48;
     gpio_mastr = ((gpio_data_a[0] - 48) * 10) + (gpio_data_a[1] - 48);



 }
 else
 {
     if(gpio_data_a[2] < '0' || gpio_data_a[2] > '7')
       {
         printf("输入的值是否正确 oh!  NO \n");
         return -1;
       }
     gpio_mastr = ((gpio_data_a[0] - 48) )  ;
     gpio_chile = gpio_data_a[2] - 48;

 }

 if(gpio_chile < 0 || gpio_chile > 7 )
 {
     printf("hei you! what you doing ?");
     return -1;
 }
  if((who_device < 5) && ((gpio_mastr >= 25 ) || (gpio_mastr < 0 )))
     return -1;
 else if ((who_device >= 5) && ((gpio_mastr >= 15) || (gpio_mastr < 0)))
             return -1;

    if(argc == 2)
    printf("*********>>>>>>>>>you input gpio is  gpio%d_%d,<<<<<<<<********\n",gpio_mastr,gpio_chile);
    else if(argc >= 3)
     printf("*********>>>>>>>>>you input gpio is  gpio%d_%d,  %s <<<<<<<<********\n",gpio_mastr,gpio_chile,argv[2]);





 if(argc == 2)
 {
     int ret_main;
     int ret_get_addr;
     if(who_device < 5)
         ret_main =  gpiomaster->gpio_return_3531(gpio_mastr,gpio_chile,&ret_get_addr);
     else if(who_device >= 5)
         gpiomaster->gpio_return_3521(gpio_mastr,gpio_chile,&ret_get_addr);
        int retrun_reg;
     char buf[24] ;
     sprintf(buf,"%x",ret_get_addr);

     //retrun_reg = char_zhuan_int(buf);
   retrun_reg  = htoi_int(buf);
   int  h=(ret_get_addr&0xffff0000)>>8;
    int  s= ret_get_addr&0xffff;

     printf("**************************************************************************\n");
         printf("i am return is =%x   ,=%d  the gpio data=%x\n",s,s,ret_get_addr);
     printf("**************************************************************************\n");

       ;



      return  s;
     // get_reg()
     //system()
 }
 if(argc == 3)
 {
     if(strstr(argv[2],"in"))
     {
        // printf("________________---=%s\n",argv[2]);
         if(who_device < 5)
             gpiomaster->argc_gpio_3531d(gpio_mastr,gpio_chile,0);
         else if(who_device >= 5)
             gpiomaster->argc_gpio_3521d(gpio_mastr,gpio_chile,0);

     }
     else if(strstr(argv[2],"out")){


         if(who_device < 5)
             gpiomaster->argc_gpio_3531d(gpio_mastr,gpio_chile,1);
         else if(who_device >= 5)
             gpiomaster->argc_gpio_3521d(gpio_mastr,gpio_chile,1);

     }
     else  if(strstr(argv[2],"on"))
     {
         //char read_addr[24];

         if(who_device < 5)
             gpiomaster->gpio_can_read_3531d(gpio_mastr,gpio_chile,0,0);
         else if(who_device >=5 )
             gpiomaster->gpio_can_read_3521d(gpio_mastr,gpio_chile,0,0);
     }
     else if(strstr(argv[2],"off"))
     {
         if(who_device < 5)
             gpiomaster->gpio_can_read_3531d(gpio_mastr,gpio_chile,1,0);
         else if(who_device >=5 )
             gpiomaster->gpio_can_read_3521d(gpio_mastr,gpio_chile,1,0);
     }
     else if(strstr(argv[2],"m"))
     {
         if(who_device < 5)
             gpiomaster->gpio_MUX_3531d(gpio_mastr,gpio_chile,0,0);
         else if(who_device >=5 )
          {
             if(gpio_mastr == 0)
             {    return -1;
             }
             gpiomaster->gpio_MUX_3521d(gpio_mastr,gpio_chile,0,0) ;
            }
     }
     else if(strstr(argv[2],"getb")){


         int ret_main;
         int ret_get_addr;
         if(who_device < 5)
             ret_main =  gpiomaster->gpio_return_base_3531(gpio_mastr,gpio_chile,&ret_get_addr);
         else if(who_device >= 5)
             gpiomaster->gpio_return_base_3531(gpio_mastr,gpio_chile,&ret_get_addr);
            int retrun_reg;
         char buf[24] ;
         sprintf(buf,"%x",ret_get_addr);

         //retrun_reg = char_zhuan_int(buf);
       retrun_reg  = htoi_int(buf);
       int  h=(ret_get_addr&0xff0000)>>16;
        int  s= ret_get_addr&0xffff;

         printf("**************************************************************************\n");
             printf("i am return is =%x   ,=%d  the gpio data=%x\n",h,h,ret_get_addr);
         printf("**************************************************************************\n");

           return h;


     }
      else if(strstr(argv[2],"get"))
     {



         int ret_main;
         int ret_get_addr;
         if(who_device < 5)
             ret_main =  gpiomaster->gpio_return_3531(gpio_mastr,gpio_chile,&ret_get_addr);
         else if(who_device >= 5)
             ret_main = gpiomaster->gpio_return_3521(gpio_mastr,gpio_chile,&ret_get_addr);
      printf("**************************************************************************\n");
      printf(">> get  i am return is =%x     the gpio data=%x\n",ret_main,ret_get_addr);
      printf("**************************************************************************\n");

         return ret_main;




     }

     else
     {


         char read_addr[24];
         sprintf(read_addr,"%s",argv[2]);
//         int i = 0;
//         for(i = 0; i < strlen(read_addr); i++)
//         {
//             if((read_addr[i] < 0 ))
//                 return -1;
//         }

      //  printf(">>>>>i am is else\n");
         int ret_st_int= char_zhuan_int(read_addr);
         if(who_device < 5)
             gpiomaster->argc_gpio_3531d_wit(gpio_mastr,gpio_chile,1,ret_st_int);
         else if(who_device >= 5)
             gpiomaster->argc_gpio_3521d_wit(gpio_mastr,gpio_chile,1,ret_st_int);
     }
 }
 else if(argc == 4)
 {
     printf(" iam is argc==4\n");
     if(strstr(argv[2],"on"))
     {
         char read_addr[12];
         //  string read_addr_st = read_addr;
           int ret_st_int;//= stoi (read_addr_st);
            int the1 ;
        char * str_the = "12";

         sprintf( read_addr,"%s",argv[3]);
         the1 = atoi(str_the);





         ret_st_int = char_zhuan_int(read_addr);

         printf("stringlen_argv  >>>>>>>>>>>>>argv3=%s,=%x,ret_P %x \n", argv[3],ret_st_int,the1);
         if(who_device < 5)
             gpiomaster->gpio_can_read_3531d(gpio_mastr,gpio_chile,2,ret_st_int);
         else if(who_device >=5 )
             gpiomaster->gpio_can_read_3521d(gpio_mastr,gpio_chile,2,ret_st_int);

     }
     else if(strstr(argv[2],"off"))
     {
         char read_addr[24];
         sprintf(read_addr,"%s",argv[3]);


         int ret_st_int= char_zhuan_int(read_addr);

         if(who_device < 5)
             gpiomaster->gpio_can_read_3531d(gpio_mastr,gpio_chile,2,ret_st_int);
         else if(who_device >=5 )
             gpiomaster->gpio_can_read_3521d(gpio_mastr,gpio_chile,2,ret_st_int);
     }
     else if(strstr(argv[2],"m"))
     {
        // printf("ia am is  mmm\n");
         char read_addr[24];
         sprintf(read_addr,"%s",argv[3]);


         int ret_st_int= char_zhuan_int (read_addr);


         if(who_device < 5)
             gpiomaster->gpio_MUX_3531d(gpio_mastr,gpio_chile ,1,ret_st_int);
         else if(who_device >=5 )
            {
             if(gpio_mastr == 0)
                 return -1;
             gpiomaster->gpio_MUX_3521d(gpio_mastr, gpio_chile,1,ret_st_int);
            }
     }
     else if(strstr(argv[2],"get"))
     {
         int ret_get_addr;
         int ret_set_;
         if(strstr(argv[3],"on"))
         {
            if ( who_device < 5)
           ret_set_ = gpiomaster->argc_direction_3521d(gpio_mastr,gpio_chile,&ret_get_addr,0);
             else if(who_device >= 5)
          ret_set_ =  gpiomaster->argc_direction_3521d(gpio_mastr,gpio_chile,&ret_get_addr,1);
               printf("*******get On*>>>>>the addr is 0x%x   data=%x<<<<<<<<<********\n",ret_set_,ret_get_addr);
               return ret_set_;
         }



         if(strstr(argv[3],"m"))
         {
             if(who_device  < 5)
               ret_set_ = gpiomaster->argc_MUX_3531d(gpio_mastr,gpio_chile,&ret_get_addr);
             else if(who_device >= 5)
                ret_set_ = gpiomaster->argc_MUX_3521d(gpio_mastr,gpio_chile,&ret_get_addr);

                printf("***** MUX ***>>>>>the addr is 0x%x   data=%x<<<<<<<<<********\n",ret_set_,ret_get_addr);
                return ret_set_;
         }


     }


 }

}













#if 0
    if(argc ==1)
      {

#if 0
        printf("******************************Parameters that* readME ********************\n");
        printf("example: ./gpio 4_0       ,Returns the value of the address\n");
        printf("example: ./gpio 4_0  in   ,you can read and write  hight \n");
        printf("example: ./gpio 4_0  out  ,you can`nt read and writ  low\n");
        printf("example: ./gpio 4_0  12   ,the '12' value that you put in, You can write what you want \n");
        printf("example: ./gpio 4_0  on   ,direction controller  input\n");
        printf("example: ./gpio 4_0  off  ,direction controller  output\n");
        printf("example: ./gpio 4_0  get  ,get reg data is addr \n");
        printf("example: ./gpio 4_0  m  1 , 'm' is behalf of reuse(MUX)  '1'  Set the value of the\n");
        //printf("example: ./gpio 4_0  m  0 ");
        printf("**************************************************************************\n");



        int kind_big_or_sml = (int)str2int(thisini->GetConfig(SoftwareConfig:: OLed_a_kind_start));

       int oled_jpio_j   = (int)str2int(thisini->GetConfig(SoftwareConfig:: OLed_a_gpio_j));

//       int GPIO_sda_MUX2  = (int)str2int(thisini->GetConfig(SoftwareConfig:: OLed_sml_SDAMUX));
       printf("kind_big_or aml=%d\n",kind_big_or_sml);


       if(kind_big_or_sml  == 0 )
       {
          if(oled_jpio_j != 0)
          {
           if(who_device >= 5) //3521d
           gpiomaster->oled_gpio_ba(1);
           else
               gpiomaster->oled_gpio_ba(0);
           }
          // gpiomaster->gpio_write_file_3531d(who_device);
       }
       else if(kind_big_or_sml == 1)
       {
           if(oled_jpio_j != 0)
           {
               if(who_device  >= 5)// 3521d
               {

                   gpiomaster->oled_gpio_sml(1);
                 //  gpiomaster->gpio_write_file_3521d( who_device);

               }
               else //3531d
               {
                   gpiomaster->oled_gpio_sml(0);
                   //gpiomaster->gpio_write_file_3531d(who_device);

               }

           }

       }
       else
       {
        printf("close all\n");
       }
    }

#endif

}
#endif













    return 0;
}
