#ifndef __READSYS_H__
#define __READSYS_H__

#include <stdio.h>
#include <stdarg.h>
#include <linux/input.h>  
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>  
#include <sys/stat.h> 
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <sys/io.h>
#include <memory.h>
#include <vector>
#include <iostream>  
#include <sstream>

#define PROC_VI_PATH    "/proc/umap/vi"
#define PROC_VENC_PATH  "/proc/umap/venc"
#define PROC_NET_PATH   "/proc/net/dev"
#define PROC_VDEC_PATH  "/proc/umap/vdec"
#define PROC_VO_PATH    "/proc/umap/vo"
#define PROC_VPSS_PATH  "/proc/umap/vpss"
#define PROC_AO_PATH    "/home/monitor/monitor.ini"

using namespace std;






extern string CheckTimeFile_big();
extern string CheckEthspeedFile_big();
extern string CheckAiFile_big();

extern string CheckViFile_big();
extern string CheckVencFile_big();
extern string CheckCpuFile_big();
extern string CheckNetFile_big();
extern string CheckVdecFile_big();
extern string CheckVoFile_big();
extern string CheckMonitorFile_big();
extern string CheckViFile_sml();


#endif
