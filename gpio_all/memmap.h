//#define __MEM_MAP_H__
#ifndef __MEM_MAP_H__
#define __MEM_MAP_H__

#if 1
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "oled_type.h"

//#include <string>
#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

#define PAGE_SIZE       0x1000
#define PAGE_SIZE_MASK  0xfffff000
#define DEFAULT_MD_LEN  128
#define IN
#define OUT





//typedef     unsigned long       HI_RET;
extern HI_RET StrToNumber(IN CHAR *str , OUT U32 * pulValue);
 extern VOID*  himm(char* addr, char* bit);
void *memmap_reg(unsigned int phy_addr, unsigned int size);

 #ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif
#endif
