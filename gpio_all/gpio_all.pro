OBJECTS_DIR += ./objs
TARGET = gpio



SOURCES += \
         main.cpp \
    software_config.cpp \
    common.cpp \
    gpio.cpp \
    memmap.cpp \
    hi_reg/hi_reg.c

HEADERS += \
    GPIO3521d.h \
    software_config.h \
    boost/lexical_cast.hpp \
    common.h \
    singleton.h \
    GPIO3531d.h \
    gpio.h \
    memmap.h \
    oled_type.h
