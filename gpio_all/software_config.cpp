#include "software_config.h"
char netfilepath_[36];
char filepath_[36];

SoftwareConfig::SoftwareConfig()
{
    if( !MakeDir(DATA_FILE) ){
        sprintf(netfilepath_, "./%s", NET_CONFIG_FILE);
        sprintf(filepath_, "./%s", OLED_CONFIG_FILE);
        }else{
        sprintf(netfilepath_, "%s/%s", DATA_FILE, NET_CONFIG_FILE);
        sprintf(filepath_, "%s/%s", DATA_FILE, OLED_CONFIG_FILE);
        }

    // 初始化 向量类型softwareConfig,定义其长度为kSoftWareConfigIDMax，内容都先置为空
    configvalue_.assign(kSoftWareConfigIDMax, "");
    //network










    configvalue_[OLed_Port]                  = "16124";
    configvalue_[OLed_Port_udp]               = "16124";
    //色彩增强
    configvalue_[OLed_a_kind_start]                   = "1";
  //  configvalue_[Oled_a_big_start]                     = "0";
    configvalue_[OLed_a_small_i_start]                 = "0";
    configvalue_[OLed_a_small_n_start]                 = "0";
    configvalue_[OLed_a_small_i_buf]                     = "0.0.0.0";
    configvalue_[OLed_a_small_n_buf]                     = "WELCOME";
  // configvalue_ LOled_a_small_china_start]              ="0";
    configvalue_[OLed_a_cpu]                         = "1";
    configvalue_[OLed_a_vi]                          = "1";
    configvalue_[OLed_a_ai]                          = "1";
    configvalue_[OLed_a_net]                         = "1";
    configvalue_[OLed_a_venc]                        = "1";
    configvalue_[OLed_a_start]                       = "1";
    configvalue_[OLed_a_sleep_tim]                   = "3";
    configvalue_[OLed_a_all_ip]                      = "1";
    configvalue_[OLed_a_gpio_j]                      = "0";
    configvalue_[OLed_ba_SDINMUX]                    = "4_2";
    configvalue_[OLed_ba_SCLKMUX]                    = "4_3";
    configvalue_[OLed_ba_CS_MUX]                     = "3_5";
    configvalue_[OLed_ba_DC_MUX]                     = "4_0";
    configvalue_[OLed_sml_VCCMUX]                    = "4_2";
    configvalue_[OLed_sml_GNDMUX]                    = "4_3";
    configvalue_[OLed_ba_RES_MUX]                    = "4_1";
    //gpio






    snprintf(paramsnamelist[OLed_Port],                       PARA_NAME_LEN, "%s=", EnumtoStr(OLed_Port));
    snprintf(paramsnamelist[OLed_Port_udp],                   PARA_NAME_LEN, "%s=", EnumtoStr(OLed_Port_udp));
    snprintf(paramsnamelist[OLed_a_kind_start],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_kind_start));
  //  snprintf(paramsnamelist[Oled_a_big_start],                PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_big_start));
    snprintf(paramsnamelist[OLed_a_small_i_start],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_small_i_start));
    snprintf(paramsnamelist[OLed_a_small_n_start],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_small_n_start));
    snprintf(paramsnamelist[OLed_a_small_i_buf],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_small_i_buf));
    snprintf(paramsnamelist[OLed_a_small_n_buf],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_small_n_buf));
  // snprintf(paramsnamelist[Lled_a_small_china_start],              PARA_NAME_LEN, "%s=", EnumtoStr(Oled_a_small_china_start));
    snprintf(paramsnamelist[OLed_a_cpu],                         PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_cpu));
    snprintf(paramsnamelist[OLed_a_vi],                          PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_vi));
    snprintf(paramsnamelist[OLed_a_ai],                      PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_ai));
    snprintf(paramsnamelist[OLed_a_net],                     PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_net));
    snprintf(paramsnamelist[OLed_a_venc],                    PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_venc));
    snprintf(paramsnamelist[OLed_a_start],                   PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_start));
    snprintf(paramsnamelist[OLed_a_sleep_tim],               PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_sleep_tim));
    snprintf(paramsnamelist[OLed_a_all_ip],                  PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_all_ip));
    snprintf(paramsnamelist[OLed_a_gpio_j],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_a_gpio_j));
   //gpio

    snprintf(paramsnamelist[ OLed_ba_SDINMUX],         PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SDINMUX  ));
    snprintf(paramsnamelist[ OLed_ba_SCLKMUX] ,        PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SCLKMUX));
    snprintf(paramsnamelist[OLed_ba_CS_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_MUX));
    snprintf(paramsnamelist[OLed_ba_DC_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_MUX));
    snprintf(paramsnamelist[OLed_sml_VCCMUX],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCCMUX));
    snprintf(paramsnamelist[OLed_sml_GNDMUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GNDMUX));
     snprintf(paramsnamelist[OLed_ba_RES_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_MUX));

 #if 0
    snprintf(paramsnamelist[Oledsml_SDA_DIR],              PARA_NAME_LEN, "%s=", EnumtoStr(Oledsml_SDA_DIR));
    snprintf(paramsnamelist[OLed_sml_SCL_DIR],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_DIR));
    snprintf(paramsnamelist[OLed_sml_SCLMUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCLMUX));
    snprintf(paramsnamelist[OLed_sml_SCLMUX_G],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCLMUX_G));
    snprintf(paramsnamelist[OLed_sml_SDAMUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDAMUX));
    snprintf(paramsnamelist[OLed_sml_SDAMUX_G],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDAMUX_G));
    snprintf(paramsnamelist[OLed_sml_SDA_star],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_star));
    snprintf(paramsnamelist[OLed_sml_SCL_star],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_star));
    snprintf(paramsnamelist[OLed_sml_SDA],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA));
    snprintf(paramsnamelist[OLed_sml_SCL],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL));
    snprintf(paramsnamelist[OLed_sml_SDA_H_star],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_H_star));
    snprintf(paramsnamelist[OLed_sml_SDA_L_star],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_L_star));
    snprintf(paramsnamelist[OLed_sml_SCL_H_star],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_H_star));
    snprintf(paramsnamelist[OLed_sml_SCL_L_star],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_L_star));
    snprintf(paramsnamelist[OLed_sml_SDA_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_H));
    snprintf(paramsnamelist[OLed_sml_SDA_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_L));
    snprintf(paramsnamelist[OLed_sml_SCL_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_H));
    snprintf(paramsnamelist[OLed_sml_SCL_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_L));
    snprintf(paramsnamelist[OLed_sml_SCL_DIR_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SCL_DIR_W));
    snprintf(paramsnamelist[OLed_sml_SDA_DIR_W],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_SDA_DIR_W));

    snprintf(paramsnamelist[OLed_sml_VCC],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC));
    snprintf(paramsnamelist[OLed_sml_GND],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GND));
    snprintf(paramsnamelist[OLed_sml_VCC_H],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC_H));
     snprintf(paramsnamelist[OLed_sml_VCC_L],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC_L));
     snprintf(paramsnamelist[OLed_sml_VCC_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_VCC_L));
     snprintf(paramsnamelist[OLed_sml_GND_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GND_H));
     snprintf(paramsnamelist[OLed_sml_GND_L],            PARA_NAME_LEN, "%s=", EnumtoStr(OLed_sml_GND_L));


     //big

    snprintf(paramsnamelist[ OLed_ba_SDINMUX_G],       PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SDINMUX_G));
    snprintf(paramsnamelist[ OLed_ba_SCLKMUX_G] ,      PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SCLKMUX_G));
    snprintf(paramsnamelist[ OLed_ba_SDIN] ,           PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SDIN));
    snprintf(paramsnamelist[ OLed_ba_SCLK] ,           PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SCLK));
    snprintf(paramsnamelist[ OLed_ba_SDIN_H],          PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SDIN_H));
    snprintf(paramsnamelist[ OLed_ba_SDIN_L] ,         PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SDIN_L));
    snprintf(paramsnamelist[ OLed_ba_SCLK_H],          PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SCLK_H));
    snprintf(paramsnamelist[ OLed_ba_SCLK_L],          PARA_NAME_LEN,"%s=",EnumtoStr( OLed_ba_SCLK_L));
    snprintf(paramsnamelist[OLed_ba_CS],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS));
    snprintf(paramsnamelist[OLed_ba_DC],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC));
    snprintf(paramsnamelist[OLed_ba_RES],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES));




     snprintf(paramsnamelist[OLed_ba_RES_mux_W],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_mux_W));
     snprintf(paramsnamelist[OLed_ba_CS_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_MUX));
     snprintf(paramsnamelist[OLed_ba_DC_MUX],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_MUX));
     snprintf(paramsnamelist[OLed_ba_CS_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_H));
      snprintf(paramsnamelist[OLed_ba_DC_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_H));
      snprintf(paramsnamelist[OLed_ba_RES_H],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_H));
      snprintf(paramsnamelist[OLed_ba_CS_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_CS_L));
      snprintf(paramsnamelist[OLed_ba_DC_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_DC_L));
      snprintf(paramsnamelist[OLed_ba_RES_L],              PARA_NAME_LEN, "%s=", EnumtoStr(OLed_ba_RES_L));

#endif




}

SoftwareConfig::~SoftwareConfig()
{
    COMMON_PRT("SoftwareConfig Normal Exit!\n");
}
  int kEth0Mac = 0;
bool SoftwareConfig::ReadConfig()
{
    COMMON_PRT("read_config:%s and %s\n", netfilepath_, filepath_);

//    read_ini rnet( netfilepath_ );
//    for(uint i = 0; i <= kEth0Mac; i++){
//        rnet.find_value( paramsnamelist[i],   configvalue_[i] );
//    }

    read_ini ri( filepath_ );
    for(uint i = 1; i < kSoftWareConfigIDMax; i++){
        ri.find_value( paramsnamelist[i],   configvalue_[i] );
    }

    return true;
}
#if 1
 #endif
bool SoftwareConfig::SaveConfig()
{
    FILE *f = NULL;
    f = fopen( filepath_, "wb" );
    if(f)
    {
        for(uint i = kEth0Mac+1; i < kSoftWareConfigIDMax; i++){
            fprintf(f, "%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
        }
        fflush(f);
        fclose(f);
        f = NULL;
        COMMON_PRT("save config succeed");
        return true;

    }

    COMMON_PRT("save config failed");
    return false;
}

void SoftwareConfig::PrintConfig()
{
    for(uint i = 0; i < kSoftWareConfigIDMax; i++){
        printf("%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
    }
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, string value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = value;
    else
        return false;

    return true;
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, int value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = int2str(value);
    else
        return false;

    return true;
}

string SoftwareConfig::GetConfig(const SoftWareConfigID kId)
{
    if(kId < kSoftWareConfigIDMax)
        return configvalue_[kId];
    else
        return "error";
}

bool SoftwareConfig::LoadConfigToGlobal(bool bPrintThem)
{
    if(bPrintThem){

    }

    return true;
}


