//#include<stdio.h>

#define muxctrl_3521d_base_reg    0x120f0000

//      GPIO1
#define muxctrl_3521d_reg1      0x004//0:gpio1_0
#define muxctrl_3521d_reg2      0x008//0:gpio1_1
#define muxctrl_3521d_reg3      0x00C//0:gpio1_2
#define muxctrl_3521d_reg4      0x010//0:gpio1_3
#define muxctrl_3521d_reg5      0x014//0:gpio1_4
#define muxctrl_3521d_reg6      0x018//0:gpio1_5
#define muxctrl_3521d_reg7      0x01C//0:gpio1_6
#define muxctrl_3521d_reg8      0x020//0:gpio1_7
//      GPIO2
#define muxctrl_3521d_reg10     0x028//0:gpio2_0
#define muxctrl_3521d_reg11     0x02c//0:gpio2_1
#define muxctrl_3521d_reg12     0x030//0:gpio2_2
#define muxctrl_3521d_reg13     0x034//0:gpio2_3
#define muxctrl_3521d_reg14     0x038//0:gpio2_4
#define muxctrl_3521d_reg15     0x03C//0:gpio2_5
#define muxctrl_3521d_reg16     0x040//0:gpio2_6
#define muxctrl_3521d_reg17     0x044//0:gpio2_7
//      GPIO3
#define muxctrl_3521d_reg20     0x050//0:gpio3_0
#define muxctrl_3521d_reg21     0x054//0:gpio3_1
#define muxctrl_3521d_reg22     0x058//0:gpio3_2
#define muxctrl_3521d_reg23     0x05C//0:gpio3_3
#define muxctrl_3521d_reg24     0x060//0:gpio3_4
#define muxctrl_3521d_reg25     0x064//0:gpio3_5
#define muxctrl_3521d_reg26     0x068//0:gpio3_6
#define muxctrl_3521d_reg27     0x06C//0:gpio3_7
//      GPIO4
#define muxctrl_3521d_reg29     0x074//0:gpio4_0
#define muxctrl_3521d_reg30     0x078//0:gpio4_1
#define muxctrl_3521d_reg31     0x07C//0:gpio4_2
#define muxctrl_3521d_reg32     0x080//0:gpio4_3
#define muxctrl_3521d_reg33     0x084//0:gpio4_4
#define muxctrl_3521d_reg34     0x088//0:gpio4_5
#define muxctrl_3521d_reg35     0x08C//0:gpio4_6
#define muxctrl_3521d_reg36     0x090//0:gpio4_7
//      GPIO5
#define muxctrl_3521d_reg49     0x0C4//2:gpio5_0
#define muxctrl_3521d_reg50     0x0C8//0:gpio5_1
#define muxctrl_3521d_reg51     0x0CC//0:gpio5_2
#define muxctrl_3521d_reg52     0x0D0//0:gpio5_3
#define muxctrl_3521d_reg48     0x0C0//0:gpio5_4
#define muxctrl_3521d_reg54     0x0D8//1:gpio5_5
#define muxctrl_3521d_reg55     0x0DC//1:gpio5_6
#define muxctrl_3521d_reg0      0x000//0:gpio5_7
//      GPIO6
#define muxctrl_3521d_reg18     0x048//0:gpio6_0
#define muxctrl_3521d_reg37     0x094//0:gpio6_1
#define muxctrl_3521d_reg60     0x0F0//1:gpio6_2
#define muxctrl_3521d_reg61     0x0F4//1:gpio6_3
#define muxctrl_3521d_reg82     0x148//0:gpio6_4
#define muxctrl_3521d_reg62     0x0F8//0:gpio6_5
#define muxctrl_3521d_reg81     0x144//1:gpio6_6
#define muxctrl_3521d_reg63     0x0FC//0:gpio6_7
//      GPIO7
#define muxctrl_3521d_reg66     0x108//0:gpio7_0
#define muxctrl_3521d_reg67     0x10C//0:gpio7_1
#define muxctrl_3521d_reg68     0x110//0:gpio7_2
#define muxctrl_3521d_reg69     0x114//0:gpio7_3
#define muxctrl_3521d_reg70     0x118//0:gpio7_4
#define muxctrl_3521d_reg71     0x11C//0:gpio7_5
#define muxctrl_3521d_reg72     0x120//0:gpio7_6
#define muxctrl_3521d_reg73     0x124//0:gpio7_7
//      GPIO8
#define muxctrl_3521d_reg74     0x128//0:gpio8_0
#define muxctrl_3521d_reg75     0x12C//0:gpio8_1
#define muxctrl_3521d_reg76     0x130//0:gpio8_2
#define muxctrl_3521d_reg77     0x134//0:gpio8_3
#define muxctrl_3521d_reg78     0x138//0:gpio8_4
#define muxctrl_3521d_reg79     0x13C//0:gpio8_5
#define muxctrl_3521d_reg80     0x140//0:gpio8_6
#define muxctrl_3521d_reg53     0x0D4//0:gpio8_7
//      GPIO9
#define muxctrl_3521d_reg40     0x0A0//0:gpio9_0
#define muxctrl_3521d_reg41     0x0A4//0:gpio9_1
#define muxctrl_3521d_reg42     0x0A8//0:gpio9_2
#define muxctrl_3521d_reg43     0x0AC//0:gpio9_3
#define muxctrl_3521d_reg44     0x0B0//0:gpio9_4
#define muxctrl_3521d_reg45     0x0B4//0:gpio9_5
#define muxctrl_3521d_reg46     0x0B8//0:gpio9_6
#define muxctrl_3521d_reg47     0x0BC//0:gpio9_7
//      GPIO10
#define muxctrl_3521d_reg83     0x14C//0:gpio10_0
#define muxctrl_3521d_reg84     0x150//0:gpio10_1
#define muxctrl_3521d_reg85     0x154//0:gpio10_2
#define muxctrl_3521d_reg97     0x184//0:gpio10_3
#define muxctrl_3521d_reg98     0x188//0:gpio10_4
#define muxctrl_3521d_reg28     0x070//0:gpio10_5
#define muxctrl_3521d_reg9      0x024//0:gpio10_6
#define muxctrl_3521d_reg58     0x0E8//1:gpio10_7
//      GPIO11
#define muxctrl_3521d_reg86     0x158//1:gpio11_0
#define muxctrl_3521d_reg87     0x15C//1:gpio11_1
#define muxctrl_3521d_reg88     0x160//1:gpio11_2
#define muxctrl_3521d_reg39     0x09C//0:gpio11_3
#define muxctrl_3521d_reg64     0x100//0:gpio11_4
#define muxctrl_3521d_reg65     0x104//0:gpio11_5
#define muxctrl_3521d_reg38     0x098//0:gpio11_6
#define muxctrl_3521d_reg19     0x04C//0:gpio11_7
//      GPIO12
#define muxctrl_3521d_reg59     0x0EC//1:gpio12_5
#define muxctrl_3521d_reg56     0x0E0//0:gpio12_6
#define muxctrl_3521d_reg57     0x0E4//0:gpio12_7
//#define muxctrl_3521d_reg
//#define muxctrl_3521d_reg
//#define muxctrl_3521d_reg
//#define muxctrl_3521d_reg
//#define muxctrl_3521d_reg
//      GPIO13
#define muxctrl_3521d_reg89    0x164  //0:gpio13_0
#define muxctrl_3521d_reg90    0x168  //0:gpio13_1
#define muxctrl_3521d_reg91    0x16C  //0:gpio13_2
#define muxctrl_3521d_reg92    0x170  //0:gpio13_3
#define muxctrl_3521d_reg93    0x174  //0:gpio13_4
#define muxctrl_3521d_reg94    0x178  //0:gpio13_5
#define muxctrl_3521d_reg95    0x17C  //0:gpio13_6
#define muxctrl_3521d_reg96    0x180  //0:gpio13_7
//      GPIO14


#define gpio13_3521d_base_reg     0x12220000
#define gpio12_3521d_base_reg     0x12210000
#define gpio11_3521d_base_reg     0x12200000
#define gpio10_3521d_base_reg     0x121F0000
#define gpio9_3521d_base_reg      0x121E0000
#define gpio8_3521d_base_reg      0x121D0000
#define gpio7_3521d_base_reg      0x121C0000
#define gpio6_3521d_base_reg      0x121B0000
#define gpio5_3521d_base_reg      0x121A0000
#define gpio4_3521d_base_reg      0x12190000
#define gpio3_3521d_base_reg      0x12180000
#define gpio2_3521d_base_reg      0x12170000
#define gpio1_3521d_base_reg      0x12160000
#define gpio0_3521d_base_reg      0x12150000

#define gpio_3521d_dir_reg        0x0400
//#define muxctrl_reg_x


#define  muxctrl_3521d_reg_x     0xFFFF
#if 1
typedef enum {
    gpio_3521d_0   = 0,
    gpio_3521d_1   = 1,
    gpio_3521d_2   = 2,
    gpio_3521d_3   = 3,
    gpio_3521d_4   = 4,
    gpio_3521d_5   = 5,
    gpio_3521d_6   = 6,
    gpio_3521d_7   = 7,
    gpio_3521d_8   = 8,
    gpio_3521d_9   = 9,
    gpio_3521d_10  = 10,
    gpio_3521d_11  = 11,
    gpio_3521d_12  = 12,
    gpio_3521d_13  = 13,
    gpio_3521d_max,
}GPIO_3521d_GROUP;

unsigned int g_muxctrl_3521d_reg[(gpio_3521d_max +1) * 8][2] =
{

    {muxctrl_3521d_reg_x,   0x0}, {muxctrl_3521d_reg_x , 0x0},
    {muxctrl_3521d_reg_x,   0x0}, {muxctrl_3521d_reg_x , 0x0},
    {muxctrl_3521d_reg_x,   0x0}, {muxctrl_3521d_reg_x , 0x0},
    {muxctrl_3521d_reg_x,   0x0}, {muxctrl_3521d_reg_x , 0x0},

    //gpio1  3521d_

    {muxctrl_3521d_reg1,   0x0}, {muxctrl_3521d_reg2  , 0x0},
    {muxctrl_3521d_reg3,   0x0}, {muxctrl_3521d_reg4  , 0x0},
    {muxctrl_3521d_reg5,   0x0}, {muxctrl_3521d_reg6  , 0x0},
    {muxctrl_3521d_reg7,   0x0}, {muxctrl_3521d_reg8  , 0x0},
     //gpio2
    {muxctrl_3521d_reg10 ,  0x0}, {muxctrl_3521d_reg11,  0x0},
    {muxctrl_3521d_reg12 ,  0x0}, {muxctrl_3521d_reg13,  0x0},
    {muxctrl_3521d_reg14 ,  0x0}, {muxctrl_3521d_reg15,  0x0},
    {muxctrl_3521d_reg16 ,  0x0}, {muxctrl_3521d_reg17,  0x0},
    //gpio3
    {muxctrl_3521d_reg20,  0x0}, {muxctrl_3521d_reg21,  0x0},
    {muxctrl_3521d_reg22,  0x0}, {muxctrl_3521d_reg23,  0x0},
    {muxctrl_3521d_reg24,  0x0}, {muxctrl_3521d_reg25,  0x0},
    {muxctrl_3521d_reg26,  0x0}, {muxctrl_3521d_reg27,  0x0},
  //gpio4
    {muxctrl_3521d_reg29,  0x0}, {muxctrl_3521d_reg30,  0x0},
    {muxctrl_3521d_reg31,  0x0}, {muxctrl_3521d_reg32,  0x0},
    {muxctrl_3521d_reg33,  0x0}, {muxctrl_3521d_reg34,  0x0},
    {muxctrl_3521d_reg35,  0x0}, {muxctrl_3521d_reg36,  0x0},
    //gpio5
    {muxctrl_3521d_reg49,  0x2}, {muxctrl_3521d_reg50,  0x0},
    {muxctrl_3521d_reg51,  0x0}, {muxctrl_3521d_reg52,  0x0},
    {muxctrl_3521d_reg53,  0x0}, {muxctrl_3521d_reg54,  0x1},
    {muxctrl_3521d_reg55,  0x1}, {muxctrl_3521d_reg0,   0x0},
    //gpio6
    {muxctrl_3521d_reg18,  0x0}, {muxctrl_3521d_reg37,  0x0},
    {muxctrl_3521d_reg60,  0x1}, {muxctrl_3521d_reg61,  0x1},
    {muxctrl_3521d_reg82,  0x0}, {muxctrl_3521d_reg62,  0x0},
    {muxctrl_3521d_reg81,  0x1}, {muxctrl_3521d_reg63,  0x0},
    //gpio7
    {muxctrl_3521d_reg66,  0x0}, {muxctrl_3521d_reg67,  0x0},
    {muxctrl_3521d_reg68,  0x0}, {muxctrl_3521d_reg69,  0x0},
    {muxctrl_3521d_reg70,  0x0}, {muxctrl_3521d_reg71,  0x0},
    {muxctrl_3521d_reg72,  0x0}, {muxctrl_3521d_reg73,  0x0},
    //gpio8
    {muxctrl_3521d_reg74,  0x0}, {muxctrl_3521d_reg75,  0x0},
    {muxctrl_3521d_reg76,  0x0}, {muxctrl_3521d_reg77,  0x0},
    {muxctrl_3521d_reg78,  0x0}, {muxctrl_3521d_reg79,  0x0},
    {muxctrl_3521d_reg80,  0x0}, {muxctrl_3521d_reg53,  0x0},

    //gpio9
    {muxctrl_3521d_reg40,  0x0}, {muxctrl_3521d_reg41,  0x0},
    {muxctrl_3521d_reg42,  0x0}, {muxctrl_3521d_reg43,  0x0},
    {muxctrl_3521d_reg44,  0x0}, {muxctrl_3521d_reg45,  0x0},
    {muxctrl_3521d_reg46,  0x0}, {muxctrl_3521d_reg47,  0x0},
    //gpio10
    {muxctrl_3521d_reg83,  0x0}, {muxctrl_3521d_reg84,  0x0},
    {muxctrl_3521d_reg85,  0x0}, {muxctrl_3521d_reg97,  0x0},
    {muxctrl_3521d_reg98,  0x0}, {muxctrl_3521d_reg28,  0x0},
    {muxctrl_3521d_reg9,  0x0}, {muxctrl_3521d_reg58,  0x1},

    //gpio11
    {muxctrl_3521d_reg86,  0x1}, {muxctrl_3521d_reg87,  0x1},
    {muxctrl_3521d_reg88,  0x1}, {muxctrl_3521d_reg39,  0x0},
    {muxctrl_3521d_reg64,  0x0}, {muxctrl_3521d_reg65,  0x0},
    {muxctrl_3521d_reg38,  0x0}, {muxctrl_3521d_reg19,  0x0},
    //gpio12
    {muxctrl_3521d_reg_x,  0x0}, {muxctrl_3521d_reg_x,  0x0},
    {muxctrl_3521d_reg_x,  0x0}, {muxctrl_3521d_reg_x,  0x0},
    {muxctrl_3521d_reg_x,  0x0}, {muxctrl_3521d_reg59,  0x1},
    {muxctrl_3521d_reg56,  0x0}, {muxctrl_3521d_reg57,  0x0},
    //gpio13
    {muxctrl_3521d_reg89,  0x0}, {muxctrl_3521d_reg90,  0x0},
    {muxctrl_3521d_reg91,  0x0}, {muxctrl_3521d_reg92,  0x0},
    {muxctrl_3521d_reg93,  0x0}, {muxctrl_3521d_reg94,  0x0},
    {muxctrl_3521d_reg95,  0x0}, {muxctrl_3521d_reg96,  0x0},
    //gpio14

};

unsigned int g_gpio_3521d_base_reg[gpio_3521d_max] =
{
    gpio0_3521d_base_reg,  gpio1_3521d_base_reg,  gpio2_3521d_base_reg,  gpio3_3521d_base_reg,
    gpio4_3521d_base_reg,  gpio5_3521d_base_reg,  gpio6_3521d_base_reg,  gpio7_3521d_base_reg,
    gpio8_3521d_base_reg,  gpio9_3521d_base_reg,  gpio10_3521d_base_reg, gpio11_3521d_base_reg,
    gpio12_3521d_base_reg, gpio13_3521d_base_reg,


};

#endif


