#include <stdio.h>
#include <stdlib.h>
#ifndef DEF_GPIO_REG_H
#define DEF_GPIO_REG_H


typedef enum {
    gpio0   = 0,
    gpio1   = 1,
    gpio2   = 2,
    gpio3   = 3,
    gpio4   = 4,
    gpio5   = 5,
    gpio6   = 6,
    gpio7   = 7,
    gpio8   = 8,
    gpio9   = 9,
    gpio10  = 10,
    gpio11  = 11,
    gpio12  = 12,
    gpio13  = 13,
    gpio14  = 14,
    gpio15  = 15,
    gpio16  = 16,
    gpio17  = 17,
    gpio18  = 18,
    gpio19  = 19,
    gpio20  = 20,
    gpio21  = 21,
    gpio22  = 22,
    gpio23  = 23,
    gpio24  = 24,

    gpio_max,
}GPIO_GROUP;

//复用寄存器基地址
#define muxctrl_base_reg    0x120f0000

#define muxctrl_reg_x        0xFFFF//未知
//GPIO0
#define muxctrl_reg117       0x1D4 //0:gpio0_0
#define muxctrl_reg118       0x1D8 //0:gpio0_1
#define muxctrl_reg119       0x1DC //0:gpio0_2
#define muxctrl_reg120       0x1E0 //0:gpio0_3
#define muxctrl_reg121       0x1E4 //0:gpio0_4
#define muxctrl_reg122       0x1E8 //0:gpio0_5
#define muxctrl_reg123       0x1EC //0:gpio0_6
#define muxctrl_reg124       0x1F0 //0:gpio0_7
//GPIO1
#define muxctrl_reg1         0x004 //1:gpio1_0
#define muxctrl_reg2         0x008 //1:gpio1_1
#define muxctrl_reg3         0x00C //1:gpio1_2
#define muxctrl_reg4         0x010 //1:gpio1_3
#define muxctrl_reg5         0x014 //1:gpio1_4
#define muxctrl_reg6         0x018 //1:gpio1_5
#define muxctrl_reg7         0x01C //1:gpio1_6
#define muxctrl_reg8         0x020 //1:gpio1_7
//GPIO2
#define muxctrl_reg10        0x028 //1:gpio2_0
#define muxctrl_reg11        0x02C //1:gpio2_1
#define muxctrl_reg12        0x030 //1:gpio2_2
#define muxctrl_reg13        0x034 //1:gpio2_3
#define muxctrl_reg14        0x038 //1:gpio2_4
#define muxctrl_reg15        0x03C //1:gpio2_5
#define muxctrl_reg16        0x040 //1:gpio2_6
#define muxctrl_reg17        0x044 //1:gpio2_7
//GPIO3
#define muxctrl_reg20        0x050 //1:gpio3_0
#define muxctrl_reg21        0x054 //1:gpio3_1
#define muxctrl_reg22        0x058 //1:gpio3_2
#define muxctrl_reg23        0x05C //1:gpio3_3
#define muxctrl_reg24        0x060 //1:gpio3_4
#define muxctrl_reg25        0x064 //1:gpio3_5
#define muxctrl_reg26        0x068 //1:gpio3_6
#define muxctrl_reg27        0x06C //1:gpio3_7
//GPIO4
#define muxctrl_reg29        0x074 //1:gpio4_0
#define muxctrl_reg30        0x078 //1:gpio4_1
#define muxctrl_reg31        0x07C //1:gpio4_2
#define muxctrl_reg32        0x080 //1:gpio4_3
#define muxctrl_reg33        0x084 //1:gpio3_4
#define muxctrl_reg34        0x088 //1:gpio4_5
#define muxctrl_reg35        0x08C //1:gpio4_6
#define muxctrl_reg36        0x090 //1:gpio4_7
//GPIO5
#define muxctrl_reg39        0x09C //1:gpio5_0
#define muxctrl_reg40        0x0A0 //1:gpio5_1
#define muxctrl_reg41        0x0A4 //1:gpio5_2
#define muxctrl_reg42        0x0A8 //1:gpio5_3
#define muxctrl_reg43        0x0AC //1:gpio5_4
#define muxctrl_reg44        0x0B0 //1:gpio5_5
#define muxctrl_reg45        0x0B4 //1:gpio5_6
#define muxctrl_reg46        0x0B8 //1:gpio5_7
//GPIO6
#define muxctrl_reg48        0x0C0 //1:gpio6_0
#define muxctrl_reg49        0x0C4 //1:gpio6_1
#define muxctrl_reg50        0x0C8 //1:gpio6_2
#define muxctrl_reg51        0x0CC //1:gpio6_3
#define muxctrl_reg52        0x0D0 //1:gpio6_4
#define muxctrl_reg53        0x0D4 //1:gpio6_5
#define muxctrl_reg54        0x0D8 //1:gpio6_6
#define muxctrl_reg55        0x0DC //1:gpio6_7
//GPIO7
#define muxctrl_reg58        0x0E8 //1:gpio7_0
#define muxctrl_reg59        0x0EC //1:gpio7_1
#define muxctrl_reg60        0x0F0 //1:gpio7_2
#define muxctrl_reg61        0x0F4 //1:gpio7_3
#define muxctrl_reg62        0x0F8 //1:gpio7_4
#define muxctrl_reg63        0x0FC //1:gpio7_5
#define muxctrl_reg64        0x100 //1:gpio7_6
#define muxctrl_reg65        0x104 //1:gpio7_7
//GPIO8
#define muxctrl_reg67        0x10C //1:gpio8_0
#define muxctrl_reg68        0x110 //1:gpio8_1
#define muxctrl_reg69        0x114 //1:gpio8_2
#define muxctrl_reg70        0x118 //1:gpio8_3
#define muxctrl_reg71        0x11C //1:gpio8_4
#define muxctrl_reg72        0x120 //1:gpio8_5
#define muxctrl_reg73        0x124 //1:gpio8_6
#define muxctrl_reg74        0x128 //1:gpio8_7
//GPIO9
#define muxctrl_reg86        0x158 //0:gpio9_0, 2:uart0_rx
#define muxctrl_reg87        0x15C //0:gpio9_1, 2:uart0_tx
#define muxctrl_reg88        0x160 //0:gpio9_2, 2:uart2_rx
#define muxctrl_reg89        0x164 //0:gpio9_3, 2:uart2_tx
#define muxctrl_reg90        0x168 //0:gpio9_4,2:uaer_1_rtsn,3:uart_0rtsn
#define muxctrl_reg91        0x16C //0:gpio9_5,2:uaer_1_ctsn,3:uart_0ctsn
#define muxctrl_reg92        0x170 //0:gpio9_6,2:uart1_rx
#define muxctrl_reg93        0x174 //0:gpio9_7,2:uart1_tx
//      GPIO 10
#define muxctrl_reg94        0x178  //0:gpio10_0
#define muxctrl_reg95        0x17C  //0:gpio10_1
#define muxctrl_reg96        0x180  //0:gpio10_2
#define muxctrl_reg97        0x184  //0:gpio10_3
#define muxctrl_reg98        0x188  //0:gpio10_4
#define muxctrl_reg99        0x18C  //0:gpio10_5
#define muxctrl_reg100       0x190  //0:gpio10_6
#define muxctrl_reg101       0x194  //0:gpio10_7
//      GPIO 11
#define muxctrl_reg76        0x130 //0:gpio11_0
#define muxctrl_reg77        0x134 //0:gpio11_1
#define muxctrl_reg78        0x138 //0:gpio11_2
#define muxctrl_reg79        0x13C //0:gpio11_3
#define muxctrl_reg80        0x140 //0:gpio11_4
#define muxctrl_reg81        0x144 //0:gpio11_5
#define muxctrl_reg82        0x148 //0:gpio11_6
#define muxctrl_reg83        0x14C //3:gpio11_7
//      GPIO 12
#define muxctrl_reg109       0x1B4  //0:gpio12_0
#define muxctrl_reg56        0x0E0  //0:gpio12_1
#define muxctrl_reg57        0x0E4  //1:gpio12_2
#define muxctrl_reg110       0x1B8  //0:gpio12_3
#define muxctrl_reg111       0x1BC  //0:gpio12_4
#define muxctrl_reg102       0x198  //0:gpio12_5
#define muxctrl_reg103       0x19C  //0:gpio12_6
#define muxctrl_reg104       0x1A0  //0:gpio12_7




//GPIO13
#define muxctrl_reg82        0x148 //0:gpio13_0
#define muxctrl_reg83        0x14C //3:gpio13_1
#define muxctrl_reg84        0x150 //0:gpio13_2
#define muxctrl_reg176       0x2C0 //0:gpio13_3
#define muxctrl_reg177       0x2C4 //0:gpio13_4
#define muxctrl_reg178       0x2C8 //0:gpio13_5
#define muxctrl_reg179       0x2CC //0:gpio13_6
#define muxctrl_reg125       0x1F4 //0:gpio13_7
//GPIO14
#define muxctrl_reg181       0x2D4 //0:gpio14_0
#define muxctrl_reg182       0x2D8 //0:gpio14_1
#define muxctrl_reg183       0x2DC //0:gpio14_2
#define muxctrl_reg184       0x2E0 //0:gpio14_3
#define muxctrl_reg185       0x2E4 //0:gpio14_4
#define muxctrl_reg186       0x2E8 //0:gpio14_5
#define muxctrl_reg187       0x2EC //0:gpio14_6
#define muxctrl_reg188       0x2F0 //0:gpio14_7

//      GPIO15
#define muxctrl_reg189          0x2F4//0:gpio15_0
#define muxctrl_reg190          0x2F8//0:gpio15_1
#define muxctrl_reg191          0x2FC//0:gpio15_2
#define muxctrl_reg192          0x300//0:gpio15_3
#define muxctrl_reg122          0x1C0//1:gpio15_4
#define muxctrl_reg123          0x1C4//1:gpio15_5
#define muxctrl_reg124          0x1C8//1:gpio15_6
#define muxctrl_reg66           0x108//1:gpio15_7
//      GPIO16
#define muxctrl_reg136          0x220//0:gpio16_0
#define muxctrl_reg137          0x224//0:gpio16_1
#define muxctrl_reg138          0x228//0:gpio16_2
#define muxctrl_reg139          0x22C//0:gpio16_3
#define muxctrl_reg140          0x230//0:gpio16_4
#define muxctrl_reg141          0x234//0:gpio16_5
#define muxctrl_reg142          0x238//0:gpio16_6
#define muxctrl_reg143          0x23C//0:gpio16_7
//      GPIO17
#define muxctrl_reg144          0x240//0:gpio17_0
#define muxctrl_reg145          0x244//0:gpio17_1
#define muxctrl_reg146          0x248//0:gpio17_2
#define muxctrl_reg147          0x24C//0:gpio17_3
#define muxctrl_reg148          0x250//0:gpio17_4
#define muxctrl_reg149          0x254//0:gpio17_5
#define muxctrl_reg150          0x258//0:gpio17_6
#define muxctrl_reg151          0x25C//0:gpio17_7
//      GPIO18
#define muxctrl_reg126          0x1F8//2:gpio18_0
#define muxctrl_reg127          0x1FC//2:gpio18_1
#define muxctrl_reg128          0x200//0:gpio18_2
#define muxctrl_reg129          0x204//0:gpio18_3
#define muxctrl_reg130          0x208//0:gpio18_4
#define muxctrl_reg131          0x20C//0:gpio18_5
#define muxctrl_reg132          0x210//0:gpio18_6
#define muxctrl_reg133          0x214//0:gpio18_7
//      GPIO19
#define muxctrl_reg157          0x274//1:gpio19_0
#define muxctrl_reg158          0x278//1:gpio19_1
#define muxctrl_reg160          0x280//1:gpio19_2
#define muxctrl_reg161          0x284//1:gpio19_3
#define muxctrl_reg162          0x288//1:gpio19_4
#define muxctrl_reg163          0x28C//1:gpio19_5
#define muxctrl_reg115          0x1CC//0:gpio19_6
#define muxctrl_reg116          0x1D0//0:gpio19_7


//GPIO20


//NOTE: gpio20_0~20_4 由硬件控制复用,JTAG_EN=0 为GPIO20_0~20_4
#define muxctrl_JTAG_EN      0x0   //0:gpio20_0 ~ gpio20_4
#define muxctrl_reg156       0x270 //0:gpio20_5
#define muxctrl_reg75        0x12c //0:gpio20_6
#define muxctrl_reg85        0x154 //0:gpio20_7
//GPIO21
#define muxctrl_reg0         0x000 //1:gpio21_0
#define muxctrl_reg9         0x024 //1:gpio21_1
#define muxctrl_reg18        0x048 //0:gpio21_2
#define muxctrl_reg19        0x04C //1:gpio21_3
#define muxctrl_reg28        0x070 //1:gpio21_4
#define muxctrl_reg37        0x094 //0:gpio21_5
#define muxctrl_reg38        0x098 //1:gpio21_6
#define muxctrl_reg47        0x0BC //1:gpio21_7
//GPIO22
#define muxctrl_reg164       0x290 //0:gpio22_0
#define muxctrl_reg165       0x294 //0:gpio22_1
#define muxctrl_reg166       0x298 //0:gpio22_2
#define muxctrl_reg167       0x29C //0:gpio22_3
#define muxctrl_reg168       0x2A0 //0:gpio22_4
#define muxctrl_reg169       0x2A4 //0:gpio22_5
#define muxctrl_reg170       0x2A8 //0:gpio22_6
#define muxctrl_reg171       0x2AC //0:gpio22_7


//GPIO23
#define muxctrl_reg180       0x2D0 //0:gpio23_0
#define muxctrl_reg172       0x2B0 //0:gpio23_1
#define muxctrl_reg173       0x2B4 //0:gpio23_2
#define muxctrl_reg174       0x2B8 //0:gpio23_3
#define muxctrl_reg175       0x2BC //0:gpio23_4
#define muxctrl_reg155       0x26C //0:gpio23_5
#define muxctrl_reg134       0x218 //0:gpio23_6
#define muxctrl_reg135       0x21C //0:gpio23_7
//GPIO24
#define muxctrl_reg105       0x1A4 //0:gpio24_0;1:VGA_VS
#define muxctrl_reg106       0x1A8 //0:gpio24_1;1:VGA_VS
#define muxctrl_reg152       0x260 //0:gpio24_2
#define muxctrl_reg153       0x264 //0:gpio24_3
#define muxctrl_reg154       0x268 //0:gpio24_4
#define muxctrl_reg159       0x27C //1:gpio24_5

// gpio0～24基地址
#define gpio24_base_reg     0x122D0000
#define gpio23_base_reg     0x122C0000
#define gpio22_base_reg     0x122B0000
#define gpio21_base_reg     0x122A0000
#define gpio20_base_reg     0x12290000
#define gpio19_base_reg     0x12280000
#define gpio18_base_reg     0x12270000
#define gpio17_base_reg     0x12260000
#define gpio16_base_reg     0x12250000
#define gpio15_base_reg     0x12240000
#define gpio14_base_reg     0x12230000
#define gpio13_base_reg     0x12220000
#define gpio12_base_reg     0x12210000
#define gpio11_base_reg     0x12200000
#define gpio10_base_reg     0x121F0000
#define gpio9_base_reg      0x121E0000
#define gpio8_base_reg      0x121D0000
#define gpio7_base_reg      0x121C0000
#define gpio6_base_reg      0x121B0000
#define gpio5_base_reg      0x121A0000
#define gpio4_base_reg      0x12190000
#define gpio3_base_reg      0x12180000
#define gpio2_base_reg      0x12170000
#define gpio1_base_reg      0x12160000
#define gpio0_base_reg      0x12150000

#define gpio_dir_reg        0x0400


#define gpio__0                 0x4
#define gpio__1                 0x8
#define gpio__2                 0x10
#define gpio__3                 0x20
#define gpio__4                 0x40
#define gpio__5                 0x80
#define gpio__6                 0x100
#define gpio__7                 0x200
//gpio 0 -7  Hight
#define gpio0_base_H          0x01
#define gpio1_base_H          0x02
#define gpio2_base_H          0x04
#define gpio3_base_H          0x08
#define gpio4_base_H          0x10
#define gpio5_base_H          0x20
#define gpio6_base_H          0x40
#define gpio7_base_H          0x80
//gpio 0-7 L
#define gpio_base_L       0x00

//gpio



unsigned int g_muxctrl_reg[gpio_max * 8][2] =
{
    //gpio0
    {muxctrl_reg117, 0x0}, {muxctrl_reg118, 0x0},
    {muxctrl_reg119, 0x0}, {muxctrl_reg120, 0x0},
    {muxctrl_reg121, 0x0}, {muxctrl_reg122, 0x0},
    {muxctrl_reg123, 0x0}, {muxctrl_reg124, 0x0},
    //gpio1
    {muxctrl_reg1,   0x1}, {muxctrl_reg2,   0x1},
    {muxctrl_reg3,   0x1}, {muxctrl_reg4,   0x1},
    {muxctrl_reg5,   0x1}, {muxctrl_reg6,   0x1},
    {muxctrl_reg7,   0x1}, {muxctrl_reg8,   0x1},
    //gpio2
    {muxctrl_reg10,  0x1}, {muxctrl_reg11,  0x1},
    {muxctrl_reg12,  0x1}, {muxctrl_reg13,  0x1},
    {muxctrl_reg14,  0x1}, {muxctrl_reg15,  0x1},
    {muxctrl_reg16,  0x1}, {muxctrl_reg17,  0x1},
    //gpio3
    {muxctrl_reg20,  0x1}, {muxctrl_reg21,  0x1},
    {muxctrl_reg22,  0x1}, {muxctrl_reg23,  0x1},
    {muxctrl_reg24,  0x1}, {muxctrl_reg25,  0x1},
    {muxctrl_reg26,  0x1}, {muxctrl_reg27,  0x1},
    //gpio4
    {muxctrl_reg29,  0x1}, {muxctrl_reg30,  0x1},
    {muxctrl_reg31,  0x1}, {muxctrl_reg32,  0x1},
    {muxctrl_reg33,  0x1}, {muxctrl_reg34,  0x1},
    {muxctrl_reg35,  0x1}, {muxctrl_reg36,  0x1},
    //gpio5
    {muxctrl_reg39,  0x1}, {muxctrl_reg40,  0x1},
    {muxctrl_reg41,  0x1}, {muxctrl_reg42,  0x1},
    {muxctrl_reg43,  0x1}, {muxctrl_reg44,  0x1},
    {muxctrl_reg45,  0x1}, {muxctrl_reg46,  0x1},
    //gpio6
    {muxctrl_reg48,  0x1}, {muxctrl_reg49,  0x1},
    {muxctrl_reg50,  0x1}, {muxctrl_reg51,  0x1},
    {muxctrl_reg52,  0x1}, {muxctrl_reg53,  0x1},
    {muxctrl_reg54,  0x1}, {muxctrl_reg55,  0x1},
    //gpio7
    {muxctrl_reg58,  0x1}, {muxctrl_reg59,  0x1},
    {muxctrl_reg60,  0x1}, {muxctrl_reg61,  0x1},
    {muxctrl_reg62,  0x1}, {muxctrl_reg63,  0x1},
    {muxctrl_reg64,  0x1}, {muxctrl_reg65,  0x1},
    //gpio8
    {muxctrl_reg67,  0x1}, {muxctrl_reg68,  0x1},
    {muxctrl_reg69,  0x1}, {muxctrl_reg70,  0x1},
    {muxctrl_reg71,  0x1}, {muxctrl_reg72,  0x1},
    {muxctrl_reg73,  0x1}, {muxctrl_reg74,  0x1},
    //gpio9
    {muxctrl_reg86,  0x0}, {muxctrl_reg87,  0x0},
    {muxctrl_reg88,  0x0}, {muxctrl_reg89,  0x0},
    {muxctrl_reg90,  0x0}, {muxctrl_reg91,  0x0},
    {muxctrl_reg92,  0x0}, {muxctrl_reg93,  0x0},
    //gpio10
    {muxctrl_reg94,  0x0}, {muxctrl_reg95,  0x0},
    {muxctrl_reg96,  0x0}, {muxctrl_reg97,  0x0},
    {muxctrl_reg98,  0x0}, {muxctrl_reg99,  0x0},
    {muxctrl_reg100,  0x0}, {muxctrl_reg101,0x0},
    //gpio11
    {muxctrl_reg76,  0x0}, {muxctrl_reg77,  0x0},
    {muxctrl_reg78,  0x0}, {muxctrl_reg79,  0x0},
    {muxctrl_reg80,  0x0}, {muxctrl_reg81,  0x0},
    {muxctrl_reg82,  0x0}, {muxctrl_reg83,  0x3},
    //gpio12
    {muxctrl_reg109,  0x0}, {muxctrl_reg56,  0x0},
    {muxctrl_reg57,  0x1}, {muxctrl_reg110,  0x0},
    {muxctrl_reg111,  0x0}, {muxctrl_reg102,  0x0},
    {muxctrl_reg103,  0x0}, {muxctrl_reg104,  0x0},
    //gpio13
    {muxctrl_reg82,  0x0}, {muxctrl_reg83,  0x3},
    {muxctrl_reg84,  0x0}, {muxctrl_reg176, 0x0},
    {muxctrl_reg177, 0x0}, {muxctrl_reg178, 0x0},
    {muxctrl_reg179, 0x0}, {muxctrl_reg125, 0x0},
    //gpio14
    {muxctrl_reg181,  0x0}, {muxctrl_reg182,  0x0},
    {muxctrl_reg183,  0x0}, {muxctrl_reg184,  0x0},
    {muxctrl_reg185,  0x0}, {muxctrl_reg186, 0x0},
    {muxctrl_reg187,  0x0}, {muxctrl_reg188,  0x0},
    //gpio15
    {muxctrl_reg189,  0x0}, {muxctrl_reg190,  0x0},
    {muxctrl_reg191,  0x0}, {muxctrl_reg192,  0x0},
    {muxctrl_reg122,  0x1}, {muxctrl_reg123,  0x1},
    {muxctrl_reg124,  0x1}, {muxctrl_reg66,  0x1},
    //gpio16
    {muxctrl_reg136,  0x0}, {muxctrl_reg137,  0x0},
    {muxctrl_reg138,  0x0}, {muxctrl_reg139,  0x0},
    {muxctrl_reg140,  0x0}, {muxctrl_reg141,  0x0},
    {muxctrl_reg142,  0x0}, {muxctrl_reg143,  0x0},
    //gpio17
    {muxctrl_reg144,  0x0}, {muxctrl_reg145,  0x0},
    {muxctrl_reg146,  0x0}, {muxctrl_reg147,  0x0},
    {muxctrl_reg148,  0x0}, {muxctrl_reg149,  0x0},
    {muxctrl_reg150,  0x0}, {muxctrl_reg151,  0x0},
    //gpio18
    {muxctrl_reg126,  0x2}, {muxctrl_reg127,  0x2},
    {muxctrl_reg128,  0x0}, {muxctrl_reg129,  0x0},
    {muxctrl_reg130,  0x0}, {muxctrl_reg131,  0x0},
    {muxctrl_reg132,  0x0}, {muxctrl_reg133,  0x0},
    //gpio19
    {muxctrl_reg157,  0x1}, {muxctrl_reg158,  0x1},
    {muxctrl_reg160,  0x1}, {muxctrl_reg161,  0x1},
    {muxctrl_reg162,  0x1}, {muxctrl_reg163,  0x1},
    {muxctrl_reg115,  0x0}, {muxctrl_reg116,  0x0},
    //gpio20
    {muxctrl_JTAG_EN,0x0}, {muxctrl_JTAG_EN,0x0},
    {muxctrl_JTAG_EN,0x0}, {muxctrl_JTAG_EN,0x0},
    {muxctrl_JTAG_EN,0x0}, {muxctrl_reg156, 0x0},
    {muxctrl_reg75,  0x0}, {muxctrl_reg85,  0x0},
    //gpio21
    {muxctrl_reg0,   0x1}, {muxctrl_reg9,   0x1},
    {muxctrl_reg18,  0x0}, {muxctrl_reg19,  0x1},
    {muxctrl_reg28,  0x1}, {muxctrl_reg37,  0x0},
    {muxctrl_reg38,  0x1}, {muxctrl_reg47,  0x1},
    //gpio22
    {muxctrl_reg164,  0x0}, {muxctrl_reg165,  0x0},
    {muxctrl_reg166,  0x0}, {muxctrl_reg167,  0x0},
    {muxctrl_reg168,  0x0}, {muxctrl_reg169,  0x0},
    {muxctrl_reg170,  0x0}, {muxctrl_reg171,  0x0},
    //gpio23
    {muxctrl_reg180, 0x0}, {muxctrl_reg172, 0x0},
    {muxctrl_reg173, 0x0}, {muxctrl_reg174, 0x0},
    {muxctrl_reg175, 0x0}, {muxctrl_reg155, 0x0},
    {muxctrl_reg134, 0x0}, {muxctrl_reg135, 0x0},
    //gpio24
    {muxctrl_reg105,  0x0}, {muxctrl_reg106, 0x0},
    {muxctrl_reg152,  0x0}, {muxctrl_reg153,  0x0},
    {muxctrl_reg154,  0x0}, {muxctrl_reg159,  0x1},
    {muxctrl_reg_x,  0x0}, {muxctrl_reg_x,  0x0}
};

unsigned int g_gpio_base_reg[gpio_max] =
{
    gpio0_base_reg,  gpio1_base_reg,  gpio2_base_reg,  gpio3_base_reg,
    gpio4_base_reg,  gpio5_base_reg,  gpio6_base_reg,  gpio7_base_reg,
    gpio8_base_reg,  gpio9_base_reg,  gpio10_base_reg, gpio11_base_reg,
    gpio12_base_reg, gpio13_base_reg, gpio14_base_reg, gpio15_base_reg,
    gpio16_base_reg, gpio17_base_reg, gpio18_base_reg, gpio19_base_reg,
    gpio20_base_reg, gpio21_base_reg, gpio22_base_reg, gpio23_base_reg,
    gpio24_base_reg
};

unsigned int g_gpio_base_reg_chile[8] =
{
    gpio__0,
    gpio__1,
    gpio__2,
    gpio__3,
    gpio__4,
    gpio__5,
    gpio__6,
    gpio__7,
};

unsigned int g_gpio_base_reg_H[8] =
{
    gpio0_base_H,
    gpio1_base_H,
    gpio2_base_H,
    gpio3_base_H,
    gpio4_base_H,
    gpio5_base_H,
    gpio6_base_H,
    gpio7_base_H,
};


#endif // DEF_GPIO_REG_H
