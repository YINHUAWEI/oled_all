#ifndef Software_Config_H
#define Software_Config_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>

#include <readini.h>
//#include "global.h"

#include "common.h"
#include "version.h"

using namespace std;
#define ETH0_NAME "eth0"
#define ETH1_NAME "eth1"
#define DNS_NAME  "/etc/resolv.conf"
#define DATA_FILE "/data"
#define NET_CONFIG_FILE "net.ini"
#define OLED_CONFIG_FILE "oled.ini"
#define NODE_NAME_FILE  "in_node_name"
extern char filepath_[36];
extern char netfilepath_[36];

#define PARA_NAME_LEN 56
#define EnumtoStr(val) #val //把枚举变量的名称. 返回类型为const char[]

class SoftwareConfig
{
public:
    typedef enum {

      //  [oled],
        OLed_Port_udp,
        OLed_Port,
       // Oled_a_small_start,
        OLed_a_kind_start,
      //  Oled_a_big_start ,
        OLed_a_small_i_start,
        OLed_a_small_n_start,
        OLed_a_small_i_buf,
        OLed_a_small_n_buf,
        OLed_a_cpu ,
        OLed_a_vi ,
        OLed_a_ai ,
        OLed_a_net,
        OLed_a_venc,
        OLed_a_start,
        OLed_a_sleep_tim,
        OLed_a_all_ip,
        OLed_a_gpio_j,
        OLed_ba_SDINMUX,
        OLed_ba_SCLKMUX,
        OLed_ba_CS_MUX,
        OLed_ba_DC_MUX,
        OLed_sml_VCCMUX,
        OLed_sml_GNDMUX,
        OLed_ba_RES_MUX,
        kSoftWareConfigIDMax,



    }SoftWareConfigID;

    SoftwareConfig();
    ~SoftwareConfig();

    bool ReadConfig();
    bool SaveConfig();
    bool SaveNetConfig();
    void PrintConfig();
    bool LoadConfigToGlobal(bool bPrintThem = false);
    bool SetConfig(const SoftWareConfigID kId, string value);
    bool SetConfig(const SoftWareConfigID kId, int value);
    string GetConfig(const SoftWareConfigID kId);
    void do_set_net(const char *kIp, const char *kMask, const char *kGateway, const char *kDns, const char* kEthName );
    void do_set_mac(const char* kMac, const char* kEthName );
    void SetNetwork();


private:
    vector<string> configvalue_;
    //const char *const filepath_ = "./hi3531d_venc.ini"; //const data,const pointer
    char paramsnamelist[kSoftWareConfigIDMax][PARA_NAME_LEN]; //存放配置文件的前缀名称
};

#endif // Software_Config_H
