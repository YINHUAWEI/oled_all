//#include <stdio.h>
//#include<GPIO3531d.h>
//#include<GPIO3521d.h>


#define HI3520D_7000_v1_D             "SHDJ23014d4_7000_V1"                     //7000_v1                      9
#define HI3520D_7000_v1               "SHNJ23014d4_7000_V1"                     //3521d 7000 v1                9
#define HI3521D_LOOP_V1               "SHNJ23015d_100_V1"                       //3521d 硬环出V1（绿色）          8
#define HI3521D_LOOP_V2               "SHNJ23015d_100_V2"                       //3521d 硬环出V2（蓝色）          7
#define HI3521D_2HDMI                 "SHNJ23015d_200_V1"                          //3521D 双hdmi输入           6
#define HI3521D_SDI                   "SHNJ23015d_400_V1"                      //3521D sdi环出                  5
#define HI3531D_4HDMI                                "0"                        //3531D 4hdmi老板子             4
#define HI3531D_DOUBLEDECK_HDMI       "SHNJ2301fd_110_V1"                        //3531D 双层hdmi               3
#define HI3531D_GUANGZHOU_VDEC        "SHGZ2301fd_100_V1"                      //3531D 广州解码300R              2
#define HI3531D_4MIX_INPUT            "SHNJ2301fd_300_V1"                       //3531D 4混合输入，4hdmi新板子    0
#define HI3531D_4MIX_INPUT_MIX        "SHNJ2301fd_400_V1"                                              //      0
#define HI3531D_NJ_JIU_6PIN           "SHNJ2301fd_100_V2"                      //3531D 南京6PIN旧板子            1
#define HI3531D_GZ_JIU_6PIN           "SHGZ2301fd_300_V1"                       // guang zhou                  1
#define HI3531D_NJ_JIU_7PIN_3         "SHNJ2301fd_100_V3"                        // nanjing venc_vdec           1
#define HI3531D_NJ_JIU_6PIN_1         "SHNJ2301fd_100_V1"                      //3531D 南京6PIN旧板子            1




extern int argv_addr_ret;
extern int who_device;

class GPIOMaster
{
public:
    GPIOMaster();
    ~GPIOMaster();

    void gpio_write_file_3531d(int devic_gpio);
    void gpio_write_file_3521d(int devic_gpio);
    void oled_gpio_ba(int mod_21d_or_31d);
    void oled_gpio_sml(int mod_21d_or_31d);
    void  OLED_init_gpio(int oled_moudle);
    int   gpio_need_addr(int gpio_mastr,int gpio_chile);
    int   recv_gpio_file(char * gpioini_data,int * addr_,int  * addr_set,int mod_21d_or_31d);
    void  write_oled_ini(char * gpioini_data);
    int    argc_gpio_3531d(int gp_master,int gp_chile,int gp_moudle);
    int   argc_gpio_3521d(int gp_master,int gp_chile,int gp_moudle);
    int   gpio_can_read_3521d(  int gp_master,int gp_chile,int gp_moudle,int gp_addr);
    int   gpio_can_read_3531d(  int gp_master,int gp_chile,int gp_moudle,int gp_addr);
    int   gpio_return_3531(  int gp_master,int gp_chile,int * gp_moudle);
    int   gpio_return_3521(  int gp_master,int gp_chile,int * gp_moudle);
    int   gpio_MUX_3531d(int gp_master,int gp_chile,int gp_moudle,int gp_addr);
    int   gpio_MUX_3521d(int gp_master,int gp_chile,int gp_moudle,int gp_addr);
    int   argc_gpio_3531d_wit(  int gp_master,int gp_chile,int gp_moudle,int gp_addr);
    int   argc_gpio_3521d_wit(  int gp_master,int gp_chile,int gp_moudle,int gp_addr);
    int   recv_gpio_(char * gpioini_data,int * addr_data,int * addr_set);
    int  recv_same_gpio();
    int  gpio_return_base_3531(int gp_mastr,int gp_chile,int *gp_addr  );
    int   argc_direction_3521d(int gp_master,int gp_chile,int *gp_moudle, int gp_31_21 );
    int   argc_MUX_3531d(int gp_master,int gp_chile,int *gp_moudle);
    int   argc_MUX_3521d(int gp_master,int gp_chile,int *gp_moudle);
public:

    bool    OLED_buf_my;
private:
    int use_gpio[7];
    int use_gpio_set[7];
    int SDAMUX_mastr;
    int SDAMUX_chile;
#if 1
    int    SDA_DIR ;
    int    SCL_DIR ;
    int    SCLMUX;
    int    SCLMUX_G;
    int    SDAMUX ;
    int    SDAMUX_G ;
    int    SDA  ;
    int    SCL  ;
    int    SDA_H  ;
    int    SDA_L  = 0x0;
    int    SCL_H  ;
    int    SCL_L = 0x0 ;
    int    SCL_DIR_W ;
    int    SDA_DIR_W ;
    int    VCCMUX ;
    int    GNDMUX ;
    int    VCC  ;
    int    GND  ;
    int    VCC_H ;
    int    VCC_L = 0x0;
    int    GND_H ;
    int    GND_L = 0x0;
    //big
    int   CSMUX ; //GPIO MUX 3_5
    int   DCMUX ; //GPIO MUX 4_0
    int   RESMUX ; //GPIO MUX 4_1
    int   SDINMUX ; //GPIO MUX 4_2
    int   SCLKMUX ; //GPIO MUX 4_3
    int   CSMUX_G ; //GPIO 3_5
    int   DCMUX_G ; //GPIO 4_0
    int   RESMUX_G ; //GPIO 4_1
    int   SDINMUX_G ; //GPIO 4_2
    int   SCLKMUX_G ; //GPIO 4_3
    int    CS_DIR ; //GPIO3_DIR
    int    DC_DIR ; //GPIO4_DIR
    int   CS ; //GPIO_DATA 3_5
    int   DC ; //GPIO_DATA 4_0
    int   RES ; //GPIO_DATA 4_1
    int   SDIN ; //GPIO_DATA 4_2
    int   SCLK ; //GPIO_DATA 4_3
    int   CS_H ;       //GPIO3_5: SET 1
    int   CS_L ;       //GPIO3_5: SET 0
    int   DC_H ;       //GPIO4_0: SET 1
    int   DC_L ;       //GPIO4_0: SET 0
    int   RES_H ;       //GPIO4_1: SET 1
    int   RES_L ;       //GPIO4_1: SET 1
    int   SDIN_H ;       //GPIO4_2: SET 1
    int   SDIN_L ;       //GPIO4_2: SET 0
    int   SCLK_H ;       //GPIO4_3: SET 1
    int   SCLK_L ;       //GPIO4_3: SET 1

    int   CS_DIR_W ;//GPIO_DIR 3_5 set out
    int   DC_DIR_W ;//GPIO_DIR 4_0 4_1 4_2 4_3 set out



#endif


};


